//
//  ProcureSurPlusFoodVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/4/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreLocation


class ProcureSurPlusFoodVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strTitle = String()
    let locationManager = CLLocationManager()
    var strErrorMessage  = ""

    @IBOutlet weak var imgAdvertisement: UIImageView!
    var timer = Timer()
    var strAdvertiseTag  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        lblTitle.text = strTitle
      //  self.callGetFoodListAPI(radius: "", lat: "", long: "")
        if(DeviceType.IS_IPHONE_X){
                         heightHeader.constant = 94
                     }else{
                         heightHeader.constant = 74
                     }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        self.callGetFoodListAPI(radius: "", lat: "", long: "", day: 15)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(aryAdvertise_Data.count != 0){
            timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        }

        super.viewWillAppear(true)
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.view.endEditing(true)
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
     @IBAction func actiononFilter(_ sender: UIButton) {
       
        self.view.endEditing(true)
        
           let vc: FilterVC = self.storyboard!.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)

}
    
    @IBAction func actionOnReload(_ sender: UIButton) {
        self.view.endEditing(true)
        self.callGetFoodListAPI(radius: "", lat: "", long: "", day: 15)
    }
    
    
    //MARK:
    //MARK:------------updateTimer For Advertisement
    @objc func updateTimer() {
        strAdvertiseTag  = strAdvertiseTag + 1
        let dict = aryAdvertise_Data.object(at: strAdvertiseTag)as! NSDictionary
        let urlImage = "\(dict.value(forKey: "url")!)" + "\(dict.value(forKey: "image")!)".replacingOccurrences(of: "~/", with: "")
        imgAdvertisement.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(strAdvertiseTag == aryAdvertise_Data.count - 1){
            strAdvertiseTag = 0
        }
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callGetFoodListAPI(radius : String , lat : String , long : String , day : Int) {
        self.strErrorMessage = ""
        
        if !isInternetAvailable() {
            self.strErrorMessage = alertInternet
            self.tvlist.reloadData()
        }else{
           let aryDate = getPastDates(days: day)
            let dict = (aryDate.lastObject)as! NSDictionary
            let fromDate = "\(dict.value(forKey: "month")!)/\(dict.value(forKey: "day")!)/\(dict.value(forKey: "year")!)"
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: date)
              let toDate = result
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodDonate xmlns='http://aahar.org.in/'><FromDate>\(fromDate)</FromDate><ToDate>\(toDate)</ToDate><Lat>\(lat)</Lat><Long>\(long)</Long><Radius>\(radius)</Radius><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id></GetFoodDonate></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetFoodDonateResult", responcetype: "GetFoodDonateResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    self.aryList = NSMutableArray()

                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryList = NSMutableArray()
                        self.aryList = (dictTemp.value(forKey: "FoodDonate")as! NSArray).mutableCopy()as! NSMutableArray
                        self.arySelectedList = NSMutableArray()
                        self.arySelectedList = (dictTemp.value(forKey: "FoodDonate")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
            }
        }
    }
    func getPastDates(days: Int) -> NSMutableArray {
        
        let dates = NSMutableArray()
        let calendar = Calendar.current
        var today = calendar.startOfDay(for: Date())
        for _ in 1 ... days {
            let day = calendar.component(.day, from: today)
            let month = calendar.component(.month, from: today)
            let year = calendar.component(.year, from: today)
            
            let date = NSMutableDictionary()
            
            date.setValue(day, forKey: "day")
            date.setValue(month, forKey: "month")
            date.setValue(year, forKey: "year")
            
            dates.add(date)
            today = calendar.date(byAdding: .day, value: -1, to: today)!
        }
        
        return dates
        
    }
  
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ProcureSurPlusFoodVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ProcureSurPlusFoodCell", for: indexPath as IndexPath) as! ProcureSurPlusFoodCell
                let dict = aryList.object(at: indexPath.row)as! NSDictionary
                cell.lbl_name.text = "\(dict.value(forKey: "Name")!)"
        if("\(dict.value(forKey: "InstitutionName")!)" != ""){
            cell.lbl_name.text = "\(dict.value(forKey: "Name")!) - \(dict.value(forKey: "InstitutionName")!)"
        }
        
        
                cell.lbl_address.text = "\(dict.value(forKey: "Address")!)"
                cell.lbl_contactNo.text = "\(dict.value(forKey: "ContactPersonName")!)(\(dict.value(forKey: "ContactPersonNo")!))"
                cell.lbl_food_picked_time.text = "\(dict.value(forKey: "FoodCollectStartTime")!) - \(dict.value(forKey: "FoodCollectEndTime")!)"
        cell.lbl_date_Time.text = "Date : \(dict.value(forKey: "CreatedDate1")!)"
               cell.lbl_date_Time.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
                cell.btn_procure.layer.cornerRadius = 5.0
        
        if(("\(dict.value(forKey: "ProcureTimeOver")!)" == "Procure Time Over") && ("\(dict.value(forKey: "BookedStatus")!)" == "Unbooked")){
            cell.btn_procure.setTitle("TIME OUT", for: .normal)
            cell.btn_procure.backgroundColor = UIColor(displayP3Red: 255/255.0, green: 171/255.0, blue: 0/255.0, alpha: 1.0)
      
        }else if (("\(dict.value(forKey: "ProcureTimeOver")!)" == "Procure Time Over") && ("\(dict.value(forKey: "BookedStatus")!)" == "Booked")){
            cell.btn_procure.setTitle("BOOKED", for: .normal)
             cell.btn_procure.backgroundColor = UIColor.red
            
        }else if (("\(dict.value(forKey: "ProcureTimeOver")!)" == "Procure") && ("\(dict.value(forKey: "BookedStatus")!)" == "Booked")){
            cell.btn_procure.setTitle("BOOKED", for: .normal)
            cell.btn_procure.backgroundColor = UIColor.red
            
        }else if (("\(dict.value(forKey: "ProcureTimeOver")!)" == "Procure") && ("\(dict.value(forKey: "BookedStatus")!)" == "Unbooked")){
            
            cell.btn_procure.setTitle("PROCURE", for: .normal)
            cell.btn_procure.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        }
        cell.btn_procure.layer.cornerRadius = 4.0
        cell.btn_procure.tag = indexPath.row
        cell.btn_procure.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
      
        if("\(dict.value(forKey: "DonorVId")!)" == "\((dict_Login_Data.value(forKey: "DonorVId")!))"){
            cell.width_btnTrack.constant = 40.0
            cell.btn_Track.tintColor = hexStringToUIColor(hex: primaryGreenColor)
            cell.btn_Track.tag = indexPath.row
            cell.btn_Track.layer.borderWidth = 1.0
            cell.btn_Track.layer.borderColor = hexStringToUIColor(hex: primaryGreenColor).cgColor
            cell.btn_Track.addTarget(self, action: #selector(btnTrack(_:)), for: .touchUpInside)
        }else{
        cell.width_btnTrack.constant = 0.0
            
        }
        
      
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    
    @objc func btnTrack(_ button: UIButton) {
            timer.invalidate()
        let dict = aryList.object(at: button.tag)as! NSDictionary
        let vc: procureTrackerVC = self.storyboard!.instantiateViewController(withIdentifier: "procureTrackerVC") as! procureTrackerVC
        vc.dictData = dict.mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func pressButton(_ button: UIButton) {
            timer.invalidate()
        print("Button with tag: \(button.tag) clicked!")
        if(button.titleLabel?.text! == "TIME OUT"){
            let alert = UIAlertController(title: alertMessage, message: "Indicated convenient Time Out.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true)
        }else{
            let dict = aryList.object(at: button.tag)as! NSDictionary
            let vc: ProcureFoodDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureFoodDetailVC") as! ProcureFoodDetailVC
            vc.strComeFrom = "Procure_list"
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
// MARK: - ----------------ProcureSurPlusFoodCell
// MARK: -
class ProcureSurPlusFoodCell: UITableViewCell {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_date_Time: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_contactNo: UILabel!
    @IBOutlet weak var lbl_food_picked_time: UILabel!
    @IBOutlet weak var btn_procure: UIButton!
    @IBOutlet weak var btn_Track: UIButton!
    @IBOutlet weak var width_btnTrack: NSLayoutConstraint!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -


extension ProcureSurPlusFoodVC: UITextFieldDelegate  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchAutocomplete(Searching: textField.text! as NSString)
        self.view.endEditing(true)
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
        return true
    }
    
    func searchAutocomplete(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
        self.strErrorMessage = ""
        removeErrorView()
        let resultPredicate = NSPredicate(format: "InstitutionName contains[c] %@ OR ProductName contains[c] %@ OR Address contains[c] %@", argumentArray: [Searching, Searching,Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.arySelectedList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.arySelectedList.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(aryList.count == 0){
            self.strErrorMessage = alertDataNotFound
            self.tvlist.reloadData()
          
        }
    }
}

extension ProcureSurPlusFoodVC: FilterDelegate {
    func GetDataFromFilterDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 000){
            self.callGetFoodListAPI(radius: "", lat: "", long: "", day: 15)

        }else{
            var strRaduis = dictData.value(forKey: "miles")as! String
                    strRaduis = strRaduis.replacingOccurrences(of: " miles", with: "")
                    strRaduis = "\(Double(Int(strRaduis)! * 1000) * 1.609)"
               let strDays = dictData.value(forKey: "days")as! String
               self.callGetFoodListAPI(radius: strRaduis, lat: strLat, long: strLong, day: Int(strDays)!)
        }
        
    }
    
 
  
}
