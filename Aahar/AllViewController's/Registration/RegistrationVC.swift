//
//  RegistrationVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseAuth

//MARK:
//MARK: ---------------Protocol-----------------

protocol RegistrationScreenDelegate : class{
    func refreshRegistrationScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------RegistrationScreenDelegate Methods----------

extension RegistrationVC: RegistrationScreenDelegate {
    func refreshRegistrationScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        if tag == 1 {
            txtCountry.text = (dictData.value(forKey: "country_name")as! String)
            txtCountry.tag = Int("\(dictData.value(forKey: "country_id")!)")!
            txtCIty.text = ""
            txtCIty.tag = 0
            txtState.text = ""
            txtState.tag = 0
            txtCountryCode.text = "\(dictData.value(forKey: "country_code")!)"
            self.aryState = NSMutableArray()
            self.aryCity = NSMutableArray()

        }else if (tag == 2){
            txtState.text = (dictData.value(forKey: "state_name")as! String)
            txtState.tag = Int("\(dictData.value(forKey: "state_id")!)")!
            txtCIty.text = ""
            txtCIty.tag = 0
            self.aryCity = NSMutableArray()

        }else if (tag == 3){
            txtCIty.text = (dictData.value(forKey: "city_name")as! String)
            txtCIty.tag = Int("\(dictData.value(forKey: "city_id")!)")!
        }
    }
}
//MARK:-
//MARK:- ---------RegistrationVC----------

class RegistrationVC: UIViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblUnderLine: UILabel!
    @IBOutlet weak var txtCountry: ACFloatingTextfield!
    @IBOutlet weak var txtState: ACFloatingTextfield!
    @IBOutlet weak var txtCIty: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtCountryCode: ACFloatingTextfield!

    @IBOutlet weak var txtName: ACFloatingTextfield!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewContain: UIView!

    var aryCountry = NSMutableArray()
    var aryState = NSMutableArray()
    var aryCity = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        lblUnderLine.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
      
        tblView.tableFooterView = UIView()
      
        txtCountryCode.text  = ""
       
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
       
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

//        Auth.auth().createUser(withEmail: "navin.p@infocratsweb.com", password: "123456") { (userInfo, error) in
//            if Auth.auth().currentUser != nil && !Auth.auth().currentUser!.isEmailVerified {
//                Auth.auth().currentUser!.sendEmailVerification(completion: { (error) in
//
//                })
//            }
//        }
//
//        Auth.auth().signIn(withEmail: "navin.p@infocratsweb.com", password: "123456", completion: {(user, error) in
//                  if let firebaseError = error {
//                      print(firebaseError.localizedDescription)
//                      return
//                  }
//                  if user != nil && !user!.isEmailVerified {
//                  // User is available, but their email is not verified.
//                  // Let the user know by an alert, preferably with an option to re-send the verification mail.
//                  }
//              })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.imgLogo.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.imgLogo.transform = .identity
            },
                       completion: nil)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {

                  let text = textField.text ?? ""

                  let trimmedText = text.trimmingCharacters(in: .whitespaces)

                  textField.text = trimmedText
             if(txtMobile == textField){
               textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
           }
      }
    //MARK:*********
    //MARK: IBAction
    @IBAction func actionOnDropDown(_ sender: UIButton) {
        if(sender.tag == 1){ //Country
            if aryCountry.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryCountry, strTitle: "")
            }else{
                callCountryAPI(sender: sender)
            }
        }else if (sender.tag == 2){ //State
            if txtCountry.tag != 0{
                if aryState.count != 0 {
                    gotoPopViewWithArray(sender: sender, aryData: aryState, strTitle: "")
                }else{
                    callstateAPI(sender: sender)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Country, viewcontrol: self)
            }
        }else if (sender.tag == 3){ // City
            if txtCountry.tag != 0 && txtState.tag != 0{
                if aryCity.count != 0 {
                    gotoPopViewWithArray(sender: sender, aryData: aryCity, strTitle: "")
                }else{
                    callCityAPI(sender: sender)
                }
            }else{
                if(txtCountry.tag == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Country, viewcontrol: self)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_State, viewcontrol: self)
                }
            }
        }
    }
  
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
          }
    
    @IBAction func actionOnNext(_ sender: UIButton) {

        /* let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC")as! OTPVC
         let dictTemp =  NSMutableDictionary()

                            var strMobile = "\(self.txtMobile.text!.replacingOccurrences(of: " ", with: ""))"
                            strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"
                            dictTemp.setValue(strMobile.replacingOccurrences(of: " ", with: ""), forKey: "MobileNo")
                            dictTemp.setValue(self.txtName.text!, forKey: "Name")
                            dictTemp.setValue("\(self.txtCIty.tag)", forKey: "city_id")
                            dictTemp.setValue("\(self.txtState.tag)", forKey: "state_id")
                            testController.dictData = dictTemp
                            self.navigationController?.pushViewController(testController, animated: true)*/
        if(validation()){
            callRegistrationAPI(sender: sender)

        }
    }
    //MARK:*********
    //MARK: Validation
    func validation() -> Bool {
        
        if(txtCountry.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Country, viewcontrol: self)
            return false
        }else if (txtState.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_State, viewcontrol: self)
            return false
            
        }else if (txtCIty.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_City, viewcontrol: self)
            return false
            
        }else if (txtName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Name, viewcontrol: self)
            return false
            
        }else if (txtMobile.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
            return false
            
        }else if ((txtMobile.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
            return false
        }else if (txtEmail.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Email, viewcontrol: self)
            return false
        }else if !(validateEmail(email: txtEmail.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
            return false
        }
        return true
        
    }
    //MARK:*********
    //MARK: API CAlling
    func callCountryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCountryList xmlns='http://aahar.org.in/'></GetCountryList></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetCountryListResult", responcetype: "GetCountryListResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryCountry = NSMutableArray()
                     self.aryCountry = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryCountry, strTitle: "")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
        
        
        
    }
    func callstateAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetStateListByCountry xmlns='http://aahar.org.in/'><country_id>\(txtCountry.tag)</country_id></GetStateListByCountry></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetStateListByCountryResult", responcetype: "GetStateListByCountryResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryState = NSMutableArray()
                    self.aryState = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryState, strTitle: "")
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
    func callCityAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCityListByState xmlns='http://aahar.org.in/'><state_id>\(txtState.tag)</state_id></GetCityListByState></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetCityListByStateResult", responcetype: "GetCityListByStateResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryCity = NSMutableArray()
                    self.aryCity = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryCity, strTitle: "")
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                    
                }
            }
        }
        
        
        
    }
    func callRegistrationAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
            let strEmail = "\(txtEmail.text!.replacingOccurrences(of: " ", with: ""))"
            var strMobile = "\(txtMobile.text!.replacingOccurrences(of: " ", with: ""))"
             strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"

            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UserRegistration xmlns='http://aahar.org.in/'>     <Name>\(txtName.text!)</Name><MobileNo>\(strMobile)</MobileNo><state_id>\(txtState.tag)</state_id><city_id>\(txtCIty.tag)</city_id><EmailId>\(strEmail)</EmailId><Password></Password></UserRegistration></soap12:Body></soap12:Envelope>"
          
            dotLoader(sender: sender , controller : self, viewContain: self.viewContain, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "UserRegistrationResult", responcetype: "UserRegistrationResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC")as! OTPVC
                        var strMobile = "\(self.txtMobile.text!.replacingOccurrences(of: " ", with: ""))"
                        strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"
                        dictTemp.setValue(strMobile.replacingOccurrences(of: " ", with: ""), forKey: "MobileNo")
                        dictTemp.setValue(self.txtName.text!, forKey: "Name")
                        dictTemp.setValue("\(self.txtCIty.tag)", forKey: "city_id")
                        dictTemp.setValue("\(self.txtState.tag)", forKey: "state_id")
                        dictTemp.setValue("Registration", forKey: "LoginType")

                        testController.dictData = dictTemp
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    //MARK:*********
    //MARK: Extra Method
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleRegistrationView = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
    }
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension RegistrationVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if (textField == txtName || textField == txtEmail)  {
//            return  txtFiledValidation(textField: txtName, string: string, returnOnly: "ALL", limitValue: 90)
//        }
//        if textField == txtMobile  {
//            return (txtFiledValidation(textField: txtMobile, string: string, returnOnly: "NUMBER", limitValue: 15))
//        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
