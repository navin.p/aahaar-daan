//
//  PickUpTrackerVC.swift
//  Aahar
//
//  Created by Navin Patidar on 4/24/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PickUpTrackerVC: UIViewController {
  
    //MARK:
    //MARK: ------------IBOutlet
   
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var dictData = NSMutableDictionary()
    var strLat = 0.0
    var strLong = 0.0
    var gameTimer: Timer!
    
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?

    //MARK:
    //MARK: --------------Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                               heightHeader.constant = 94
                           }else{
                               heightHeader.constant = 74
                           }
        print(dictData)
        mapReload()
        btnStop.alpha = 0.5
        btnStop.isUserInteractionEnabled = false
    }
    
    @objc func runTimedCode (){
        callAddTrackAPI()
    }
    
    //MARK:
    //MARK: --------------IBAction
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        if(gameTimer != nil){
            gameTimer.invalidate()
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnSTART(_ sender: UIButton) {
        callStartAPI()
    }
    
    @IBAction func actionOnSTOP(_ sender: UIButton) {
        btnStart.alpha = 1.0
        btnStart.isUserInteractionEnabled = true
        sender.alpha = 0.5
        sender.isUserInteractionEnabled = false
        gameTimer.invalidate()
        locationManager.stopUpdatingLocation()
    }
    
    //MARK:
    //MARK: -------------Extra Function
    
    func mapReload()  {
        mapView.delegate = self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Check for Location Services
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        mapView.delegate = self
    }
    
    func drawLine(){
        // 2.
        if(strLat == 0.0 || strLong == 0.0  ||  "\(dictData.value(forKey: "LatD")!)" == "" || "\(dictData.value(forKey: "LongD")!)" == ""){
        }else{
            if(self.mapView.annotations.count != 0){
                self.mapView.removeAnnotations(self.mapView.annotations)
            }
            let MomentaryLatitudeDesti = Double("\(dictData.value(forKey: "LatD")!)")
            let MomentaryLongitudeDesti = Double("\(dictData.value(forKey: "LongD")!)")
            let sourceLocation = CLLocationCoordinate2D(latitude: (strLat), longitude: (strLong))
            let destinationLocation = CLLocationCoordinate2D(latitude: (MomentaryLatitudeDesti)!, longitude: (MomentaryLongitudeDesti)!)
            // 3.
            let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
            let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
            // 4.
            let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
            let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
            // 5.
            let sourceAnnotation = MKPointAnnotation()
            sourceAnnotation.title = "Start Point"
            if let location = sourcePlacemark.location {
                sourceAnnotation.coordinate = location.coordinate
            }
            let destinationAnnotation = MKPointAnnotation()
            destinationAnnotation.title = "Pickup Point"
            if let location = destinationPlacemark.location {
                destinationAnnotation.coordinate = location.coordinate
            }
            // 6.
            //   self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
            self.mapView.addAnnotations([sourceAnnotation , destinationAnnotation])
            self.mapView.showAnnotations(mapView.annotations, animated: true)
            // 7.
            let directionRequest = MKDirections.Request()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem
            directionRequest.transportType = .automobile
            // Calculate the direction
            let directions = MKDirections(request: directionRequest)
            // 8.
            directions.calculate {
                (response, error) -> Void in
                guard let response = response else {
                    if let error = error {
                        print("Error: \(error)")
                    }
                    return
                }
                let route = response.routes[0]
                if(self.mapView.overlays.count != 0){
                    self.mapView.removeOverlays(self.mapView.overlays)
                }
                self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
                let rect = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
            }
        }
    }
    
    //MARK:
    //MARK: -------------API Calling
    
    func callStartAPI() {
        if !isInternetAvailable() {
            //  showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let strFoodDonatedId = "\(dictData.value(forKey: "ProcureFoodId")!)"
            let strDonorVId = "\(dict_Login_Data.value(forKey: "DonorVId")!)"
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Attachcollectbywithprocureid xmlns='http://aahar.org.in/'><ProcureFoodId>\(strFoodDonatedId)</ProcureFoodId><CollectByDonorVId>\(strDonorVId)</CollectByDonorVId></Attachcollectbywithprocureid></soap12:Body></soap12:Envelope>"
            dotLoader(sender: btnStart , controller : self, viewContain: self.view, onView: "Button")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLTrack, resultype: "AttachcollectbywithprocureidResult", responcetype: "AttachcollectbywithprocureidResponse") { (responce, status) in
                print(responce)
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.btnStop.alpha = 1.0
                        self.btnStop.isUserInteractionEnabled = true
                        self.btnStart.alpha = 0.5
                        self.btnStart.isUserInteractionEnabled = false
                        self.gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                    }else{
                    }
                }else{
                }
            }
        }
    }
    
    func callAddTrackAPI() {
        if !isInternetAvailable() {
            //  showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let strFoodDonatedId = "\(dictData.value(forKey: "FoodDonetId")!)"
            let strDonorVId = "\(dictData.value(forKey: "DonorVId")!)"
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddTrackLocation xmlns='http://aahar.org.in/'><FoodDonetId>\(strFoodDonatedId)</FoodDonetId><DonorVId>\(strDonorVId)</DonorVId><Lat>\(strLat)</Lat><Long>\(strLong)</Long></AddTrackLocation></soap12:Body></soap12:Envelope>"
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLTrack, resultype: "AddTrackLocationResult", responcetype: "AddTrackLocationResponse") { (responce, status) in
                print(responce)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        
                    }else{
                    }
                }else{
                }
            }
        }
    }
}

// MARK: - ---------------MKMapViewDelegate
// MARK: -

extension PickUpTrackerVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        else {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            if(annotation.title == " "){
                annotationView.image = UIImage(named:"scooter (1)")
                return annotationView
            }
            //            else if (annotation.title == "Destination Point"){
            //                annotationView.image = UIImage(named:"DestinationFood")
            //            }
        }
        return nil
    }
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    // CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        self.strLat = currentLocation?.coordinate.latitude ?? 0
        self.strLong = currentLocation?.coordinate.longitude ?? 0
        if(self.mapView.overlays.count == 0){
            self.drawLine()
        }
        //        let newPin = MKPointAnnotation()
        //        let location = locations.last! as CLLocation
        //        newPin.title = "ooo"
        //        self.mapView.addAnnotation(newPin)
        
        for item in self.mapView.annotations {
            if(item as AnyObject).title == " "{
                self.mapView.removeAnnotation((item as AnyObject)as! MKAnnotation)
            }
        }
        let userLocation:CLLocation = locations[0] as CLLocation
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
        myAnnotation.title = " "
        mapView.addAnnotation(myAnnotation)
    }
}

