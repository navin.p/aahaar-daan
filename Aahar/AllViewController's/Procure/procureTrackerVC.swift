//
//  procureTrackerVC.swift
//  Aahar
//
//  Created by Navin Patidar on 4/24/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class procureTrackerVC: UIViewController {
    
    //MARK:
    //MARK: ------------IBOutlet
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnNumber: UIButton!
    @IBOutlet weak var imgForBookingPerson: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblRideStart: UILabel!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var dictData = NSMutableDictionary()
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?
    var strFoodDonetId = ""
    var strLat = 0.0
    var strLong = 0.0
    var gameTimer: Timer!
    //MARK:
    //MARK: --------------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                                    heightHeader.constant = 94
                                }else{
                                    heightHeader.constant = 74
                                }
           print(dictData)
        callGetFoodProcureByFoodDonetIdAPI()
       
     
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
          self.gameTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
    }
    @objc func runTimedCode (){
        if(strFoodDonetId.count != 0){
            callGetTrackAPI()
        }
    }
    
    //MARK:
    //MARK: -------------Extra Function
    func drawLine(trackData : NSDictionary)  {
        // 2.
        if("\(trackData.value(forKey: "Long")!)" == "" || "\(trackData.value(forKey: "Long")!)" == ""  ||  "\(dictData.value(forKey: "Long")!)" == "" || "\(dictData.value(forKey: "Lat")!)" == ""){
        }else{
            if(self.mapView.annotations.count != 0){
                self.mapView.removeAnnotations(self.mapView.annotations)
            }
            let MomentaryLatitudeDesti = Double("\(dictData.value(forKey: "Lat")!)")
            let MomentaryLongitudeDesti = Double("\(dictData.value(forKey: "Long")!)")
           
            let MomentaryLatitudeSource = Double("\(trackData.value(forKey: "Lat")!)")
            let MomentaryLongitudeSource = Double("\(trackData.value(forKey: "Long")!)")
            
            let sourceLocation = CLLocationCoordinate2D(latitude: (MomentaryLatitudeSource)!, longitude: (MomentaryLongitudeSource)!)

            
            let destinationLocation = CLLocationCoordinate2D(latitude: (MomentaryLatitudeDesti)!, longitude: (MomentaryLongitudeDesti)!)
            // 3.
            let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
            let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
            // 4.
            let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
            let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
            // 5.
            let sourceAnnotation = MKPointAnnotation()
            sourceAnnotation.title = "Start Point"
            if let location = sourcePlacemark.location {
                sourceAnnotation.coordinate = location.coordinate
            }
            let destinationAnnotation = MKPointAnnotation()
            destinationAnnotation.title = "Pickup Point"
            if let location = destinationPlacemark.location {
                destinationAnnotation.coordinate = location.coordinate
            }
            // 6.
            //   self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
            self.mapView.addAnnotations([sourceAnnotation , destinationAnnotation])
            if(self.mapView.tag == 0){
                self.mapView.showAnnotations(mapView.annotations, animated: true)
                self.mapView.tag = 1
            }
            // 7.
            let directionRequest = MKDirections.Request()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem
            directionRequest.transportType = .automobile
            // Calculate the direction
            let directions = MKDirections(request: directionRequest)
            // 8.
            directions.calculate {
                (response, error) -> Void in
                guard let response = response else {
                    if let error = error {
                        print("Error: \(error)")
                    }
                    return
                }
                let route = response.routes[0]
                if(self.mapView.overlays.count != 0){
                    self.mapView.removeOverlays(self.mapView.overlays)
                }
                self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
                let rect = route.polyline.boundingMapRect
               // self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
            }
        }
    }
    //MARK:
    //MARK: --------------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnCall(_ sender: UIButton) {
        if (callingFunction(number: ("\(String(describing: (btnNumber.titleLabel?.text!)!))".replacingOccurrences(of: "Mobile :", with: "") as NSString))){
            
        }
    }
    
    
    //MARK:
    //MARK: -------------API Calling
    
    func callGetTrackAPI() {
        if !isInternetAvailable() {
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetTrackLocation xmlns='http://aahar.org.in/'><FoodDonetId>\(self.strFoodDonetId)</FoodDonetId></GetTrackLocation></soap12:Body></soap12:Envelope>"
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLTrack, resultype: "GetTrackLocationResult", responcetype: "GetTrackLocationResponse") { (responce, status) in
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                        if(aryTemp.count != 0){
                            let dict = aryTemp.object(at: 0)as! NSDictionary
                            self.drawLine(trackData: dict)
                        }
                        self.lblRideStart.isHidden = true
                    }else{
                        self.lblRideStart.isHidden = false
                    }
                }else{
                }
            }
        }
    }
    func callGetFoodProcureByFoodDonetIdAPI() {
        if !isInternetAvailable() {
          //  showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodProcureByFoodDonetId xmlns='http://aahar.org.in/'><FoodDonetId>\(dictData.value(forKey: "FoodDonetId")!)</FoodDonetId></GetFoodProcureByFoodDonetId></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetFoodProcureByFoodDonetIdResult", responcetype: "GetFoodProcureByFoodDonetIdResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        
                        let aryTemp = (dictTemp.value(forKey: "FoodProcure")as! NSArray).mutableCopy()as! NSMutableArray
                        if(aryTemp.count != 0){
                            let dict = aryTemp.object(at: 0)as! NSDictionary
                            self.strFoodDonetId = "\(dict.value(forKey: "FoodDonetId")!)"
                            self.callGetTrackAPI()
                            self.lblName.text = "Booked By : \(dict.value(forKey: "Name")!)"
                            self.btnNumber.setTitle("Mobile : \(dict.value(forKey: "MobileNo")!)", for: .normal)
                        }
                    }else{
                        // self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    // self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
}

// MARK: - ---------------MKMapViewDelegate
// MARK: -


extension procureTrackerVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        else {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            if(annotation.title == "Start Point"){
                annotationView.image = UIImage(named:"scooter (1)")
                return annotationView
            }
            //            else if (annotation.title == "Destination Point"){
            //                annotationView.image = UIImage(named:"DestinationFood")
            //            }
        }
        return nil
    }
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
  
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        if currentLocation == nil {
            // Zoom to user location
            if let userLocation = locations.last {
                let viewRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
                mapView.setRegion(viewRegion, animated: false)
                mapView.centerCoordinate = (manager.location?.coordinate)!
                locationManager.stopUpdatingLocation()
            }
        }
    }
}

