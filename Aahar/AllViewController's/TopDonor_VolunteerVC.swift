//
//  TopDonor_VolunteerVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class TopDonor_VolunteerVC: UIViewController {
    
    // MARK: - -------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var imgAdvertisement: UIImageView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    // MARK: - -------------Variable
    // MARK: -
    var aryTopDonor = NSMutableArray()
    var aryTopVolunteer = NSMutableArray()
    var aryTBL = NSMutableArray()
    var timer = Timer()
    var strAdvertiseTag  = 0
    var strErrorMessage  = ""

    // MARK: - ---------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 130
       // segment.tintColor =  hexStringToUIColor(hex: primaryGreenColor)
        aryTBL = NSMutableArray()
        tvlist.reloadData()
        callTopDonorListAPI()
        if(aryAdvertise_Data.count != 0){
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        let font = UIFont.boldSystemFont(ofSize: 15)
        segment.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
        if(DeviceType.IS_IPHONE_X){
                                      heightHeader.constant = 94
                                  }else{
                                      heightHeader.constant = 74
                                  }
        
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func actionOnSegment(_ sender: UISegmentedControl) {
        strErrorMessage = ""
        if(sender.selectedSegmentIndex == 0){
            aryTBL = NSMutableArray()
            tvlist.tag = 1
            tvlist.reloadData()
            if aryTopDonor.count != 0{
                aryTBL = NSMutableArray()
                aryTBL = aryTopDonor
                tvlist.tag = 1
                tvlist.reloadData()
            }else{
                callTopDonorListAPI()
            }
        }else{
            aryTBL = NSMutableArray()
            tvlist.tag = 2
            tvlist.reloadData()
            if aryTopVolunteer.count != 0{
                aryTBL = NSMutableArray()
                aryTBL = aryTopVolunteer
                tvlist.tag = 2
                tvlist.reloadData()
            }else{
                callTopProcureListAPI()
            }
        }
        
        
    }

    //MARK:
    //MARK:------------updateTimer For Advertisement
    @objc func updateTimer() {
        strAdvertiseTag  = strAdvertiseTag + 1
        let dict = aryAdvertise_Data.object(at: strAdvertiseTag)as! NSDictionary
        let urlImage = "\(dict.value(forKey: "url")!)" + "\(dict.value(forKey: "image")!)".replacingOccurrences(of: "~/", with: "")
        imgAdvertisement.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(strAdvertiseTag == aryAdvertise_Data.count - 1){
            strAdvertiseTag = 0
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
    }
    //MARK:
    //MARK:------------API CAlling
    func callTopDonorListAPI() {
        if !isInternetAvailable() {
            self.strErrorMessage = alertInternet
            self.tvlist.reloadData()
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetTopFoodDonation xmlns='http://aahar.org.in/'><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id></GetTopFoodDonation></soap12:Body></soap12:Envelope>"
            
            let loading = DPBasicLoading(table: self.tvlist, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetTopFoodDonationResult", responcetype: "GetTopFoodDonationResponse") { (responce, status) in
                loading.endLoading()

                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary

                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.strErrorMessage  = ""
                        let aryTemp = dictTemp.value(forKey: "TopDonation")as! NSArray
                        self.aryTopDonor = NSMutableArray()
                        self.aryTopDonor = aryTemp.mutableCopy()as! NSMutableArray
                        self.aryTBL = NSMutableArray()
                        self.aryTBL = self.aryTopDonor
                        self.tvlist.tag = 1
                        self.tvlist.reloadData()

                    }else{
                        self.aryTopDonor = NSMutableArray()
                        self.aryTBL = NSMutableArray()
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()

                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()

                }
            }
        }
        
    }
    func callTopProcureListAPI() {
        if !isInternetAvailable() {
            self.strErrorMessage = alertInternet
            self.tvlist.reloadData()
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetTopFoodProcured xmlns='http://aahar.org.in/'><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id></GetTopFoodProcured></soap12:Body></soap12:Envelope>"
            
            let loading = DPBasicLoading(table: self.tvlist, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetTopFoodProcuredResult", responcetype: "GetTopFoodProcuredResponse") { (responce, status) in
                loading.endLoading()
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary

                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.strErrorMessage  = ""

                        let aryTemp = dictTemp.value(forKey: "TopProcure")as! NSArray
                        self.aryTopVolunteer = NSMutableArray()
                        self.aryTopVolunteer = aryTemp.mutableCopy()as! NSMutableArray
                        self.aryTBL = NSMutableArray()
                        self.aryTBL = self.aryTopVolunteer
                        self.tvlist.tag = 2
                        self.tvlist.reloadData()
                    }else{
                        self.aryTopVolunteer = NSMutableArray()
                        self.aryTBL = NSMutableArray()
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                        
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
            }
        }
        
    }

    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension TopDonor_VolunteerVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTBL.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "TopDonor_VolunteerVCell", for: indexPath as IndexPath) as! TopDonor_VolunteerVCell
        let dict = aryTBL.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "Name")!)"
        if(tvlist.tag == 1){
            cell.lblTitleDetail.text = "Donate Count - \(dict.value(forKey: "donatecount")!)"
        }else{
            cell.lblTitleDetail.text = "Procure Count - \(dict.value(forKey: "procurecount")!)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
}

// MARK: - ----------------TopDonor_VolunteerVCCell
// MARK: -
class TopDonor_VolunteerVCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleDetail: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
