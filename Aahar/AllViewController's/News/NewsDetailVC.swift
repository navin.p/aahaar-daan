//
//  NewsDetailVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController {
@IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblNewsTitle: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var txtDetail: UITextView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

 var dictNews = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                  heightHeader.constant = 94
              }else{
                  heightHeader.constant = 74
              }
       lblNewsTitle.text = "\(dictNews.value(forKey: "Title")!)"
       txtDetail.text = "\(dictNews.value(forKey: "Message")!)".html2String
        let urlImage = "\(dictNews.value(forKey: "ImagePath")!)"
        imgNews.setImageWith(URL(string: BaseURLImagePath + urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
}
