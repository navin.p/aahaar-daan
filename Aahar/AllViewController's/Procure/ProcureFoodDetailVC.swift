//
//  ProcureFoodDetailVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/5/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol ProcureFoodDetailScreenDelegate : class{
    func refreshProcureFoodDetailScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------DonateFreshfoodScreenDelegate Methods----------

extension ProcureFoodDetailVC: ProcureFoodDetailScreenDelegate {
    func refreshProcureFoodDetailScreen(dictData: NSDictionary, tag: Int) {
        
        if(tag == 15){ // For Procured Person
            lblFoodProcuredBy.text = (dictData.value(forKey: "ContactPersonName")as! String) + " (\(dictData.value(forKey: "ContactPersonNo")!))"
                   lblFoodProcuredBy.tag = Int("\(dictData.value(forKey: "DVOtherContactId")!)")!
        }else{
            lblFoodBookedBy.text = (dictData.value(forKey: "ContactPersonName")as! String) + " (\(dictData.value(forKey: "ContactPersonNo")!))"
                   lblFoodBookedBy.tag = Int("\(dictData.value(forKey: "DVOtherContactId")!)")!
        }
        
       
    }
}
class ProcureFoodDetailVC: UIViewController {
    
    var dictData = NSMutableDictionary()
    var aryList = NSMutableArray()
    var dictProcureData = NSMutableDictionary()
    var strComeFrom = String()
    var aryCollection = NSMutableArray()
    var aryCollectionDistributionImage = NSMutableArray()

    var picker = UIImagePickerController()
    var strImage1 = ""
    var strImage2 = ""
    var strImage3 = ""
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnProcure: UIButton!
    @IBOutlet weak var btnPickUp: UIButton!
    @IBOutlet weak var btnselectContact: UIButton!
    @IBOutlet weak var btnsBookedelectContact: UIButton!

    @IBOutlet weak var heightTV: NSLayoutConstraint!
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tvlist: UITableView!
    
    @IBOutlet weak var lblOrganizationName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPickUPTime: UILabel!
    
    @IBOutlet weak var lblContactPerson: UILabel!
    @IBOutlet weak var lblFoodBookedBy: UILabel!
    @IBOutlet weak var lblFoodProcuredBy: UILabel!

    @IBOutlet var viewDistribution: UIView!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var bynSubmit: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnBrowse: UIButton!

    @IBOutlet weak var lblDistributed: UILabel!
    @IBOutlet weak var lblDistributedAddress: UILabel!
    @IBOutlet weak var collectionDistributted: UICollectionView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    
    
    @IBOutlet weak var heightViewProcureByBookedBy: NSLayoutConstraint!

    weak var delegate: FilterDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                                            heightHeader.constant = 94
                                        }else{
                                            heightHeader.constant = 74
                                        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnProcure.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnPickUp.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        bynSubmit.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnBrowse.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)

        lblMessage.isHidden = false
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        
        collectionView.layer.cornerRadius = 4.0
        collectionView.layer.borderColor  =  UIColor.lightGray.cgColor
         collectionView.layer.borderWidth = 1.0
        collectionDistributted.tag = 0
        btnselectContact.layer.cornerRadius = 4.0
                   btnselectContact.layer.borderColor = UIColor.lightGray.cgColor
                   btnselectContact.layer.borderWidth = 1.0
                   btnsBookedelectContact.layer.cornerRadius = 4.0
                   btnsBookedelectContact.layer.borderColor = UIColor.lightGray.cgColor
                   btnsBookedelectContact.layer.borderWidth = 1.0
        
        if(strComeFrom == "Donated"){
            lblOrganizationName.text = "\(dictData.value(forKey: "Name")!)"
            if("\(dictData.value(forKey: "InstitutionName")!)" != ""){
                      lblOrganizationName.text = "\(dictData.value(forKey: "Name")!) - \(dictData.value(forKey: "InstitutionName")!)"
                  }
                  
            
            lblAddress.text = "\(dictData.value(forKey: "Address")!)"
            lblDate.text = "\(dictData.value(forKey: "CreatedDate")!)"
            
            lblPickUPTime.text = "\(dictData.value(forKey: "FoodCollectStartTime")!) - " +  "\(dictData.value(forKey: "FoodCollectEndTime")!)"
            lblContactPerson.text = "\(dictData.value(forKey: "ContactPersonName")!) (\(dictData.value(forKey: "ContactPersonNo")!))"
          
            if ("\(dictData.value(forKey: "FStatus")!)" == "Distributed"){
                lblDistributed.text = "Distribution Address"
                lblDistributedAddress.text = "\(dictData.value(forKey: "DistributedAddress")!)" == "<null>" ?"":"\(dictData.value(forKey: "DistributedAddress")!)"
                collectionDistributted.tag = 99
                
                var srtImages = "\(dictData.value(forKey: "Images")!)"
                srtImages = srtImages.replacingOccurrences(of: ",,", with: "")
                let aryDistributionImages = srtImages.split(separator: ",")
                self.aryCollectionDistributionImage = (aryDistributionImages as AnyObject).mutableCopy() as! NSMutableArray
                self.collectionDistributted.reloadData()
                
            }else{
                lblDistributed.text = ""
                lblDistributedAddress.text = ""
                self.aryCollectionDistributionImage =  NSMutableArray()
                self.collectionDistributted.reloadData()
            }
            
            btnPickUp.isHidden = true
            btnProcure.isHidden = true
        }else if(strComeFrom == "Procured"){
            
            
            btnPickUp.isHidden = true
            btnProcure.isHidden = true
            lblOrganizationName.text = "\(dictData.value(forKey: "DonatePersonName")!)"

            if("\(dictData.value(forKey: "DonateInstitutionName")!)" != ""){
                                lblOrganizationName.text = "\(dictData.value(forKey: "DonatePersonName")!) - \(dictData.value(forKey: "DonateInstitutionName")!)"
                            }
            lblAddress.text = "\(dictData.value(forKey: "DonorAddress")!)"
            lblDate.text = "\(dictData.value(forKey: "ProcureDate")!)"
            
            lblPickUPTime.text = "\(dictData.value(forKey: "FoodCollectStartTime")!) - " +  "\(dictData.value(forKey: "FoodCollectEndTime")!)"
            lblContactPerson.text = "\(dictData.value(forKey: "DonateContactPersonName")!) (\(dictData.value(forKey: "DonateContactPersonNo")!))"
          
            lblDistributed.text = ""
            lblDistributedAddress.text = ""
            aryCollectionDistributionImage = NSMutableArray()
            self.collectionDistributted.reloadData()

            
        }
        else if(strComeFrom == "Procure_list"){
            btnPickUp.isHidden = false
            btnProcure.isHidden = false
            lblOrganizationName.text = "\(dictData.value(forKey: "Name")!)"
            if("\(dictData.value(forKey: "InstitutionName")!)" != ""){
                                 lblOrganizationName.text = "\(dictData.value(forKey: "Name")!) - \(dictData.value(forKey: "InstitutionName")!)"
                             }
                             
            lblAddress.text = "\(dictData.value(forKey: "Address")!)"
            lblDate.text = "\(dictData.value(forKey: "CreatedDate")!)"
            
            lblPickUPTime.text = "\(dictData.value(forKey: "FoodCollectStartTime")!) - " +  "\(dictData.value(forKey: "FoodCollectEndTime")!)"
            lblContactPerson.text = "\(dictData.value(forKey: "ContactPersonName")!) (\(dictData.value(forKey: "ContactPersonNo")!))"
           
            lblDistributed.text = ""
            lblDistributedAddress.text = ""
            self.aryCollectionDistributionImage =  NSMutableArray()
            self.collectionDistributted.reloadData()
            heightViewProcureByBookedBy.constant = 180.0
            if("\(dictData.value(forKey: "BookedStatus")!)" == "Booked"){
                heightViewProcureByBookedBy.constant = 95.0
            }
        }
    
        
        else if (strComeFrom == "Distributed"){

            lblOrganizationName.text = "\(dictData.value(forKey: "DonatePersonName")!)"
            
            if("\(dictData.value(forKey: "DonateInstitutionName")!)" != ""){
                                lblOrganizationName.text = "\(dictData.value(forKey: "DonatePersonName")!) - \(dictData.value(forKey: "DonateInstitutionName")!)"
                            }
            lblAddress.text = "\(dictData.value(forKey: "DonorAddress")!)"
            lblDate.text = "\(dictData.value(forKey: "ProcureDate")!)"

            lblPickUPTime.text = "\(dictData.value(forKey: "FoodCollectStartTime")!) - " +  "\(dictData.value(forKey: "FoodCollectEndTime")!)"
            lblContactPerson.text = "\(dictData.value(forKey: "DonateContactPersonName")!) (\(dictData.value(forKey: "DonateContactPersonNo")!))"
          


            lblDistributed.text = "Distribution Address"
            lblDistributedAddress.text = "\(dictData.value(forKey: "DistributedAddress")!)" == "<null>" ?"":"\(dictData.value(forKey: "DistributedAddress")!)"
            collectionDistributted.tag = 99

            var srtImages = "\(dictData.value(forKey: "Images")!)"
            srtImages = srtImages.replacingOccurrences(of: ",,", with: "")
            let aryDistributionImages = srtImages.split(separator: ",")
            self.aryCollectionDistributionImage = (aryDistributionImages as AnyObject).mutableCopy() as! NSMutableArray
            self.collectionDistributted.reloadData()

            btnPickUp.isHidden = true
            btnProcure.isHidden = true

        }
        
        
        
        callGetFoodProcureListAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(aryList.count == 0){
            heightTV.constant = 0.0
        }else{
            heightTV.constant = CGFloat((aryList.count * 70) + 41)
        }
        var heightScroll = 550
        if(strComeFrom == "Distributed" || strComeFrom == "Donated" ){
            heightScroll = 650
        }
        
        viewContain.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: Int (heightTV.constant) + heightScroll)
        scroll.contentSize = CGSize(width: Int(self.view.frame.width), height: Int (heightTV.constant) + heightScroll)
        viewContain.removeFromSuperview()
        scroll.addSubview(viewContain)
        
        
        if(strComeFrom == "Procured"){
            if( "\(dictData.value(forKey: "FStatus")!)" != "Distributed"){
                for btn in self.view.subviews {
                    if(btn is UIButton){
                        if (btn as AnyObject).tag == 999{
                            btn.removeFromSuperview()
                        }
                    }
                }
                let btnDistributed = UIButton(frame: CGRect(x: 8, y:scroll.frame.maxY, width: self.view.frame.width - 16, height: 44))
                btnDistributed.setTitle("Distribution", for: .normal)
                btnDistributed.layer.cornerRadius = 8.0
                btnDistributed.tag = 999
                btnDistributed.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
                btnDistributed.addTarget(self, action: #selector(pressButtonDistribution), for: .touchUpInside)
                if(aryList.count != 0){
                    if !(self.view.subviews.contains(self.viewDistribution)){
                        self.view.addSubview(btnDistributed)
                    }
                }
            }
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnSubmitDistribution(_ sender: UIButton) {
       
        if(txtAddress.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Address, viewcontrol: self)
        }else if (aryCollection.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Distribution image requird!", viewcontrol: self)

        }else{
            for (index , item) in aryCollection.enumerated(){
                          if(index == 0){
                              strImage1 = (item as AnyObject).value(forKey: "name")as! String
                          }else if (index == 1){
                              strImage2 = (item as AnyObject).value(forKey: "name")as! String

                          }else if (index == 2){
                              strImage3 = (item as AnyObject).value(forKey: "name")as! String

                          }
                      }

            
            let dict = self.aryCollection.lastObject as! NSDictionary
           self.uploadDistributionImage( image: (dict.value(forKey: "image") as! UIImage), name: (dict.value(forKey: "name") as! String))
        }
    }
    
    @IBAction func actionOnButtonTransprant(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
              self.viewDistribution.frame = CGRect(x: 0, y: Int(self.view.frame.height), width: Int(self.view.frame.width), height: Int(self.view.frame.height - self.viewHeader.frame.height))
        }) { (Bool) in
            self.viewDistribution.removeFromSuperview()
        }
    }
    
    @IBAction func actiononBrowse(_ sender: UIButton) {
    if aryCollection.count < 3{
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: Alert_Camera, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera(UIImagePickerController.SourceType.camera)
        }
        let galleryAction = UIAlertAction(title: Alert_Gallery, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera(UIImagePickerController.SourceType.photoLibrary)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }else{
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReadingImageLimit, viewcontrol: self)
        }
        
    }
    func openCamera(_ sourceType: UIImagePickerController.SourceType) {
           picker.sourceType = sourceType
           self.present(picker, animated: true, completion: nil)
       }
    @IBAction func actionSelectBookedBy(_ sender: UIButton) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = ""
        vc.strTag =  sender.tag == 0 ? 12 : 15
        let aryData = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
        let resultPredicate = NSPredicate(format: "IsActive contains[c] %@", argumentArray: ["True"])
        let arrayfilter = (aryData ).filtered(using: resultPredicate)
        let nsMutableArray = NSMutableArray(array: arrayfilter)
        if nsMutableArray.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleProcureFoodDetailScreen = self
            vc.aryTBL = nsMutableArray.mutableCopy() as! NSMutableArray
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ContactList, viewcontrol: self)
        }
    }
    
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.view.endEditing(true)

        if("\(dictData.value(forKey: "DonorVId")!)" == "\(dict_Login_Data.value(forKey: "DonorVId")!)"){
            if("\(dictData.value(forKey: "BookedStatus")!)" == "Booked"){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_BookedMessage, viewcontrol: self)
                
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_BookedDonatedBySelf, viewcontrol: self)
            }
        }else{
            
            if("\(dictData.value(forKey: "BookedStatus")!)" == "Booked"){
                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_BookedMessage, viewcontrol: self)

                    }else{
                        if(lblFoodBookedBy.tag == 0){
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectBookedContact, viewcontrol: self)
                            }
                            else if(lblFoodProcuredBy.tag == 0){
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectProcureContact, viewcontrol: self)
                                }
                            else{
                                callADDFoodDonetIdAPI()
                            }
                        
            }
                }
                
        
        
        
        
    
    }
    @IBAction func actionOnPickup(_ sender: Any) {
        if(self.dictProcureData.count != 0){
            
            if("\(dictProcureData.value(forKey: "DonorVId")!)" == "\(dict_Login_Data.value(forKey: "DonorVId")!)"){
                let vc: PickUpTrackerVC = self.storyboard!.instantiateViewController(withIdentifier: "PickUpTrackerVC") as! PickUpTrackerVC
                               vc.dictData = self.dictProcureData
                               self.navigationController?.pushViewController(vc, animated: true)
            }else{
               
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_PickUpMessage, viewcontrol: self)

            }
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_BeforepickupMessage, viewcontrol: self)
        }
        
    }
    
    @objc func pressButtonDistribution(_ button: UIButton) {
          self.viewDistribution.frame = CGRect(x: 0, y: Int(self.view.frame.height), width: Int(self.view.frame.width), height: Int(self.view.frame.height - self.viewHeader.frame.height))
        UIView.animate(withDuration: 0.2, animations: {
            self.viewDistribution.frame = CGRect(x: 0, y: Int(self.viewHeader.frame.maxY), width: Int(self.view.frame.width), height: Int(self.view.frame.height - self.viewHeader.frame.height))
            self.view.addSubview( self.viewDistribution)
        }) { (Bool) in
        }
    }
    
    func GoOnback(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
           UIView.animate(withDuration: 0.2, animations: {
                     self.viewDistribution.frame = CGRect(x: 0, y: Int(self.view.frame.height), width: Int(self.view.frame.width), height: Int(self.view.frame.height - self.viewHeader.frame.height))
                 }) { (Bool) in
                     self.viewDistribution.removeFromSuperview()
                     self.view.endEditing(true)
                     self.navigationController?.popViewController(animated: true)
                 }
        }
        
        
       
        
    }
    
    
    
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callADDFoodDonetIdAPI() {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddProcureFoodAssignTo xmlns='http://aahar.org.in/'><ProcureFoodId>0</ProcureFoodId><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><FoodDonetId>\(dictData.value(forKey: "FoodDonetId")!)</FoodDonetId><DVOtherContactId>\(lblFoodBookedBy.tag)</DVOtherContactId><AssignToDVOtherContactId>\(lblFoodProcuredBy.tag)</AssignToDVOtherContactId></AddProcureFoodAssignTo></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddProcureFoodAssignToResult", responcetype: "AddProcureFoodAssignToResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let alert = UIAlertController(title: alertInfo, message: alert_ProcureFoodBook, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.delegate?.GetDataFromFilterDelegate(dictData: NSDictionary(), tag: 000)
                                 self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                    }else{
                        self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    func callGetFoodProcureByFoodDonetIdAPI() {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodProcureByFoodDonetId xmlns='http://aahar.org.in/'><FoodDonetId>\(dictData.value(forKey: "FoodDonetId")!)</FoodDonetId></GetFoodProcureByFoodDonetId></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetFoodProcureByFoodDonetIdResult", responcetype: "GetFoodProcureByFoodDonetIdResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        
                        
                        let aryTemp = (dictTemp.value(forKey: "FoodProcure")as! NSArray).mutableCopy()as! NSMutableArray
                        if(aryTemp.count != 0){
                            let dict = aryTemp.object(at: 0)as! NSDictionary
                            self.dictProcureData = NSMutableDictionary()
                            self.dictProcureData = dict.mutableCopy() as! NSMutableDictionary
                            self.lblFoodBookedBy.text = "\(dict.value(forKey: "Name")!)(\(dict.value(forKey: "MobileNo")!))"
                           
                            
                        }
                        self.tvlist.reloadData()
                        self.viewWillAppear(true)
                    }else{
                        // self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    // self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    
    
    func callGetFoodProcureListAPI() {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodDonationMapping xmlns='http://aahar.org.in/'><FoodDonetId>\(dictData.value(forKey: "FoodDonetId")!)</FoodDonetId></GetFoodDonationMapping></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetFoodDonationMappingResult", responcetype: "GetFoodDonationMappingResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryList = NSMutableArray()
                        self.aryList = (dictTemp.value(forKey: "FoodDonationMapping")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                        self.viewWillAppear(true)
                        self.callGetFoodProcureByFoodDonetIdAPI()
                    }else{
                        self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    func uploadDistributionImage(image : UIImage , name : String)  {
          if !isInternetAvailable() {
              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
          }else{
             let alert = loader_Show(controller: self, strMessage: "Loading...")

              WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLDistributedImagehandler, image: image, fileName: name, withName: "userfile") { (responce, status) in
                
                alert.dismiss(animated: false) {
                    if(status == "Suceess"){
                                   self.aryCollection.removeLastObject()
                                     if(self.aryCollection.count == 0){
                                         self.callAddDistributedAPI()
                                     }else{
                                         let dict = self.aryCollection.lastObject as! NSDictionary
                                         self.uploadDistributionImage( image: (dict.value(forKey: "image") as! UIImage), name: (dict.value(forKey: "name") as! String))
                                     }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
             
              }
          }
      }
  
    func callAddDistributedAPI() {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
        
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Add_FoodDistributionDetail xmlns='http://aahar.org.in/'><FoodDisId>0</FoodDisId><Images1>\(strImage1)</Images1><Images2>\(strImage2)</Images2><Images3>\(strImage3)</Images3><Address>\(txtAddress.text!)</Address><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><ProcureFoodId>\(dictData.value(forKey: "ProcureFoodId")!)</ProcureFoodId><FoodDonetId>\(dictData.value(forKey: "FoodDonetId")!)</FoodDonetId></Add_FoodDistributionDetail></soap12:Body></soap12:Envelope>"
         
            let alert = loader_Show(controller: self, strMessage: "Loading...")

            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Add_FoodDistributionDetailResult", responcetype: "Add_FoodDistributionDetailResponse") { (responce, status) in
              
                alert.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                         
                            FTIndicator.showToastMessage("Food distributed successfully.")
                            self.GoOnback()
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }else{
                    }
                }
            }
        }
    }
    
    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, view: viewHeader)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewDidLoad()
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ProcureFoodDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tvlist.dequeueReusableCell(withIdentifier: "ProcureFoodItemCell", for: indexPath as IndexPath) as! ProcureFoodItemCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblFoodName.text = "\(dict.value(forKey: "Food")!)"
        cell.lblShelfLife.text = "\(dict.value(forKey: "ShelfLife")!)"
        cell.lblQty.text = "\(dict.value(forKey: "NoOfPerson")!)"
        cell.lblOther.text = "\(dict.value(forKey: "OtherFoodDetail")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        if ("\(dict.value(forKey: "OtherFoodDetail")!)" == "") {
            return 50.0
        }else{
            return 69.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class ProcureFoodItemCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblShelfLife: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblOther: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
//MARK: ---------- UIImagePickerControllerDelegate
//MARK:
extension ProcureFoodDetailVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let dict = NSMutableDictionary()
        dict.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
        dict.setValue("\(getUniqueString())", forKey: "name")
        aryCollection.add(dict)
        self.collectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerController cancel")
        picker.dismiss(animated: true, completion: nil)
        
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension ProcureFoodDetailVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
// MARK: - --------------UICollectionView
// MARK:
extension ProcureFoodDetailVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 99 ? aryCollectionDistributionImage.count : aryCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        lblMessage.isHidden = true

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DistributedCell", for: indexPath as IndexPath) as! DistributedCell
        
        if(collectionView.tag == 99){

            let urlImage = "http://aahar.org.in/FoodDistributedImage/\(aryCollectionDistributionImage[indexPath.row])"
                            cell.ImageDistributed.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                                print(url ?? 0)
                            }, usingActivityIndicatorStyle: .gray)
        }else{
            let dict = aryCollection[indexPath.row]as! NSDictionary

            cell.ImageDistributed.image = (dict.value(forKey: "image") as! UIImage)
        }
        
        cell.ImageDistributed.layer.cornerRadius = 4.0
        cell.ImageDistributed.layer.borderColor = UIColor.lightGray.cgColor
        cell.ImageDistributed.layer.borderWidth = 1.0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90 , height:90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView.tag != 99){
            let alertController = UIAlertController(title: alertMessage, message: alertRemoveImage, preferredStyle: .actionSheet)
            let okAction = UIAlertAction(title: "Remove", style: .default) {
                UIAlertAction in
                self.aryCollection.removeObject(at: indexPath.row)
                if(self.aryCollection.count == 0){
                    self.lblMessage.isHidden = false
                }
                collectionView.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        
    }


}
class DistributedCell: UICollectionViewCell {
    //Welcome Screen
    @IBOutlet weak var ImageDistributed: UIImageView!
 
}
