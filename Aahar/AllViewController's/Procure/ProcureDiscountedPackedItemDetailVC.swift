//
//  ProcureDiscountedPackedItemDetailVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/11/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol ProcureDiscountedPackedItemDetailScreenDelegate : class{
    func refreshProcureDiscountedPackedItemDetailScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------DonateFreshfoodScreenDelegate Methods----------

extension ProcureDiscountedPackedItemDetailVC: ProcureDiscountedPackedItemDetailScreenDelegate {
    func refreshProcureDiscountedPackedItemDetailScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        txtContactPerson.text = (dictData.value(forKey: "ContactPersonName")as! String) + " (\(dictData.value(forKey: "ContactPersonNo")!))"
        txtContactPerson.tag = Int("\(dictData.value(forKey: "DVOtherContactId")!)")!
    }
    
 
}
class ProcureDiscountedPackedItemDetailVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblInstituteName: UILabel!

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblContactPerson: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var txtContactPerson: ACFloatingTextfield!
    @IBOutlet weak var btnProcure: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var dictData = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
      print(dictData)
        if(DeviceType.IS_IPHONE_X){
                                     heightHeader.constant = 94
                                 }else{
                                     heightHeader.constant = 74
                                 }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        let urlImage = "\(dictData.value(forKey: "ProductImage")!)"
        imgProduct.setImageWith(URL(string: "http://aahar.org.in/DiscountItemImage/" + urlImage), placeholderImage: UIImage(named: "defult_news_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        
        lblInstituteName.text = "\(dictData.value(forKey: "InstitutionName")!)"
        lbltitle.text = "\(dictData.value(forKey: "ProductName")!)"
        lblAddress.text = "Address : \(dictData.value(forKey: "Address")!)"
        lblExpDate.text = "Exp. Date - \(dictData.value(forKey: "ExpDate")!)"
        lblPrice.text = "\u{20B9} - \(dictData.value(forKey: "MRP")!)"
        lblDiscount.text = "\u{20B9} - \(dictData.value(forKey: "DiscountPrice")!)"
        lblDiscount.backgroundColor = hexStringToUIColor(hex: primaryOrangeColor)
        lblContactPerson.text = "Contact Person : \(dictData.value(forKey: "ContactPersonName")!)(\(dictData.value(forKey: "ContactPersonNo")!))"
        lblProductDescription.text = "\(dictData.value(forKey: "Detail")!)"
        // Do any additional setup after loading the view.
    }
    //MARK:
    //MARK:------------IBAction
    @IBAction func actionOnSelectContact(_ sender: UIButton) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = ""
        vc.strTag = 13
        let aryData = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
        
              let resultPredicate = NSPredicate(format: "IsActive contains[c] %@", argumentArray: ["True"])
        let arrayfilter = (aryData ).filtered(using: resultPredicate)
        let nsMutableArray = NSMutableArray(array: arrayfilter)
        if nsMutableArray.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleProcureDiscountedPackedItemDetailScreen = self
            vc.aryTBL = nsMutableArray.mutableCopy() as! NSMutableArray
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ContactList, viewcontrol: self)
        }
    }
    @IBAction func actionOnProcure(_ sender: UIButton) {
        if (txtContactPerson.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectContact, viewcontrol: self)
        }else{
            callProcureAPI()
        }
    }
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callProcureAPI() {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{

            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetDiscountItemProcure xmlns='http://aahar.org.in/'><AddDonorVID>\(dictData.value(forKey: "DonorVId")!)</AddDonorVID><AddDVOtherContactId>\(dictData.value(forKey: "DVOtherContactId")!)</AddDVOtherContactId><AddDiscountItemId>\(dictData.value(forKey: "DiscountItemId")!)</AddDiscountItemId><DonorVID>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVID><DVOtherContactId>\(txtContactPerson.tag)</DVOtherContactId></GetDiscountItemProcure></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetDiscountItemProcureResult", responcetype: "GetDiscountItemProcureResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryResult = (dictTemp.value(forKey: "MsgResult")as! NSArray)
                        var strMessage = "Dear ,Your request  has been placed. To procure the item(\(self.lbltitle.text!))get in touch with \(self.dictData.value(forKey: "ContactPersonName")!),please contact at \(self.dictData.value(forKey: "ContactPersonNo")!)"
                        if(aryResult.count != 0){
                            strMessage = (aryResult.object(at: 0)as! NSDictionary).value(forKey: "ResultMsg") as! String
                        }
                        let alert = UIAlertController(title: alertMessage, message: strMessage , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }
    
}
