//
//  DonateFreshoddBookNowVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/1/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit


//MARK:
//MARK: ---------------Protocol-----------------

protocol DonateFreshfoodScreenDelegate : class{
    func refreshDonateFreshfoodScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------DonateFreshfoodScreenDelegate Methods----------

extension DonateFreshoddBookNowVC: DonateFreshfoodScreenDelegate {
    func refreshDonateFreshfoodScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        txtSelectConatact.text = (dictData.value(forKey: "ContactPersonName")as! String) + " (\(dictData.value(forKey: "ContactPersonNo")!))"
        txtSelectConatact.tag = Int("\(dictData.value(forKey: "DVOtherContactId")!)")!
    }
}

class DonateFreshoddBookNowVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var lblSelectDate: UILabel!
    @IBOutlet weak var txtSelectConatact: ACFloatingTextfield!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryListAmount = NSMutableArray()
    var strTitle = String()
    var dictData = NSMutableDictionary()
    var dictAmount = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                          heightHeader.constant = 94
                      }else{
                          heightHeader.constant = 74
                      }
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        lblSelectDate.layer.borderWidth = 1.0
        lblSelectDate.layer.borderColor = UIColor.lightGray.cgColor
        lblSelectDate.layer.cornerRadius = 6.0
        btnBookNow.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        lblTitle.text = strTitle
        tvlist.tag = 0
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.callOrganizationAmountListAPI()
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSelectDate(_ sender: Any) {
        self.viewPicker.frame = self.view.frame
        picker.minimumDate = Date()
        self.view.addSubview(self.viewPicker)
    }
    
    @IBAction func actionONDone(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let somedateString = dateFormatter.string(from: picker.date)
        lblSelectDate.text = somedateString
        self.viewPicker.removeFromSuperview()
    }
    @IBAction func actionOnSelectContact(_ sender: Any) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = 7
        let aryData = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
        let resultPredicate = NSPredicate(format: "IsActive contains[c] %@", argumentArray: ["True"])
        let arrayfilter = (aryData ).filtered(using: resultPredicate)
        let nsMutableArray = NSMutableArray(array: arrayfilter)
        if nsMutableArray.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleDonateFreshfoodScreen = self
            vc.aryTBL = nsMutableArray.mutableCopy() as! NSMutableArray
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ContactList, viewcontrol: self)
        }
    }
    
    @IBAction func actiononBookNow(_ sender: UIButton) {
        if(validation()){
       callDonateNowAPI(sender: sender)
        }
    }
    
    //MARK:
    //MARK:-------------- Validation
    func validation() -> Bool {
        if(lblSelectDate.text == "Select Date"){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectDate, viewcontrol: self)
            return false
        }else if (txtSelectConatact.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectContact, viewcontrol: self)
            return false
        }
        return true
    }
    
    // MARK: - ---------------API CAlling
    // MARK: -
    
    func callOrganizationAmountListAPI() {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Get_PricePersonListOrganizationId xmlns='http://aahar.org.in/'><OrganizationId>\(dictData.value(forKey: "Organization_Id")!)</OrganizationId></Get_PricePersonListOrganizationId></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Get_PricePersonListOrganizationIdResult", responcetype: "Get_PricePersonListOrganizationIdResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryListAmount = NSMutableArray()
                        self.aryListAmount = (dictTemp.value(forKey: "MealList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.dictAmount = NSMutableDictionary()
                        if(self.aryListAmount.count != 0){
                            self.dictAmount = (self.aryListAmount.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            self.tvlist.reloadData()}
                    }else{
                        self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }

    func callDonateNowAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddDonateFreshFood xmlns='http://aahar.org.in/'><FreshFoodDonetId>0</FreshFoodDonetId><FoodPriceListId>\(dictAmount.value(forKey: "FoodPriceListId")!)</FoodPriceListId><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><Organization_Id>\(dictAmount.value(forKey: "Organization_Id")!)</Organization_Id><DonateDate>\(lblSelectDate.text!)</DonateDate><DVOtherContactId>\(txtSelectConatact.tag)</DVOtherContactId><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id></AddDonateFreshFood></soap12:Body></soap12:Envelope>"
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddDonateFreshFoodResult", responcetype: "AddDonateFreshFoodResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                    
                        let alert = UIAlertController(title: alertInfo, message: alert_DonationFoodBook, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                        
                    }else{
                        self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
    }
    

    
    
    
    
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, view: viewHeader)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DonateFreshoddBookNowVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryListAmount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DonatedFoodCell", for: indexPath as IndexPath) as! DonatedFoodCell
        let dict = aryListAmount.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "Price")!) Rs. for \(dict.value(forKey: "NoOfPerson")!) Persons"
        if tvlist.tag == indexPath.row {
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none

        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          tvlist.tag = indexPath.row
         dictAmount = NSMutableDictionary()
         dictAmount = (aryListAmount.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        tvlist.reloadData()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class DonatedFoodCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
