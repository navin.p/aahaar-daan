//
//  DonatedDiscountedFoodVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/1/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol DonatefoodDiscountScreenDelegate : class{
    func refreshDonatefoodDiscountScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------DonateFreshfoodScreenDelegate Methods----------

extension DonatedDiscountedFoodVC: DonatefoodDiscountScreenDelegate {
    func refreshDonatefoodDiscountScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        txtContactPerson.text = (dictData.value(forKey: "ContactPersonName")as! String) + " (\(dictData.value(forKey: "ContactPersonNo")!))"
        txtContactPerson.tag = Int("\(dictData.value(forKey: "DVOtherContactId")!)")!
    }
}

class DonatedDiscountedFoodVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblImageName: UILabel!
    @IBOutlet weak var ProductImg: UIImageView!
    @IBOutlet weak var btnBrowse: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtProductName: ACFloatingTextfield!
    @IBOutlet weak var txtProductdescription: KMPlaceholderTextView!
    @IBOutlet weak var txtExpDate: ACFloatingTextfield!
    @IBOutlet weak var txtMRP: ACFloatingTextfield!
    @IBOutlet weak var txtDiscount: ACFloatingTextfield!
    @IBOutlet weak var txtContactPerson: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: KMPlaceholderTextView!
    
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var pickerImg = UIImagePickerController()
    var strTitle = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                                 heightHeader.constant = 94
                             }else{
                                 heightHeader.constant = 74
                             }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnBrowse.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnBrowse.layer.cornerRadius = 6.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        lblTitle.text = strTitle
        ProductImg.layer.borderWidth = 1.0
        ProductImg.layer.borderColor = UIColor.darkGray.cgColor
        txtMRP.placeholder = "\u{20B9} MRP"
        txtDiscount.placeholder = "\u{20B9} Discount Price"
        txtAddress.text = "\(dict_Login_Data.value(forKey: "Address")!)"
    }
    // MARK: - ---------------OpenCamera
    // MARK: -
    func openCamera(_ sourceType: UIImagePickerController.SourceType) {
        pickerImg.sourceType = sourceType
        self.present(pickerImg, animated: true, completion: nil)
    }

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnBrowse(_ sender: UIButton) {
        self.view.endEditing(true)

        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: Alert_Camera, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera(UIImagePickerController.SourceType.camera)
        }
        let galleryAction = UIAlertAction(title: Alert_Gallery, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera(UIImagePickerController.SourceType.photoLibrary)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        pickerImg.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if(validation()){
            self.uploadProductImage()
        }
    }
    @IBAction func actionOnDrop(_ sender: UIButton) {
        self.view.endEditing(true)
        if(sender.tag == 1){
            self.viewPicker.frame = self.view.frame
            picker.minimumDate = Date()
            self.view.addSubview(self.viewPicker)
        }else{
            let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            vc.strTitle = strTitle
            vc.strTag = 8
            let aryData = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
            
            let resultPredicate = NSPredicate(format: "IsActive contains[c] %@", argumentArray: ["True"])
                    let arrayfilter = (aryData ).filtered(using: resultPredicate)
                 let nsMutableArray = NSMutableArray(array: arrayfilter)
            
            if nsMutableArray.count != 0{
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .coverVertical
                vc.handleDonatefoodDiscountScreen = self
                vc.aryTBL = nsMutableArray.mutableCopy() as! NSMutableArray
                self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ContactList, viewcontrol: self)
        }
      }
    }
    @IBAction func actionONDone(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let somedateString = dateFormatter.string(from: picker.date)
        txtExpDate.text = somedateString
        self.viewPicker.removeFromSuperview()
    }
    //MARK:
    //MARK:-------------- Validation
    func validation() -> Bool {

        if(txtProductName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductName, viewcontrol: self)
            return false
        }else if (txtProductdescription.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductdescription, viewcontrol: self)
            return false
        }else if (txtExpDate.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductExpDate, viewcontrol: self)
            return false
        }
        else if (txtMRP.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductMRP, viewcontrol: self)
            return false
        }
        else if (txtDiscount.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductDiscountPrice, viewcontrol: self)
            return false
        }
        else if (txtContactPerson.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductContactPerson, viewcontrol: self)
            return false
        }else if (lblImageName.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductImage, viewcontrol: self)
            return false
        }
        return true
    }
    
    //MARK: ---------API Calling
    //MARK:
    func callSaveDataAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddDiscountItem xmlns='http://aahar.org.in/'><DiscountItemId>0</DiscountItemId><ProductName>\(txtProductName.text!)</ProductName><Detail>\(txtProductdescription.text!)</Detail><ExpDate>\(txtExpDate.text!)</ExpDate><MRP>\(txtMRP.text!)</MRP><DiscountPrice>\(txtDiscount.text!)</DiscountPrice><ProductImage>\(lblImageName.text!)</ProductImage><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><DVOtherContactId>\(txtContactPerson.tag)</DVOtherContactId><IsActive></IsActive><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id></AddDiscountItem></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddDiscountItemResult", responcetype: "AddDiscountItemResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                     
                        let alert = UIAlertController(title: alertInfo, message: alert_DonationFoodBook, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func uploadProductImage()  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProduct, image: self.ProductImg.image!, fileName: lblImageName.text!, withName: "userfile") { (responce, status) in
                remove_dotLoader(controller: self)
                if(status == "Suceess"){
                  self.callSaveDataAPI(sender: UIButton())
                }
            }
        }
    }
}
//MARK: ---------- UIImagePickerControllerDelegate
//MARK:
extension DonatedDiscountedFoodVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        ProductImg.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        lblImageName.text = "Product\(getUniqueString())"
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerController cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: ---------- UITextFieldDelegate
//MARK:
extension DonatedDiscountedFoodVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  txtProductName {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
        if textField ==  txtDiscount ||  textField == txtMRP  {
            return (txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
