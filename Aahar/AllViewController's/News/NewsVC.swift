
//
//  NewsVC.swift
//  Aahar
//
//  Created by Navin on 2/21/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {
    // MARK: - ---------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var imgAdvertisement: UIImageView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryNewsList = NSMutableArray()
    var timer = Timer()
    var strAdvertiseTag  = 0
    var strErrorMessage  = ""

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                  heightHeader.constant = 94
              }else{
                  heightHeader.constant = 74
              }
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 130
        if(aryAdvertise_Data.count != 0){
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(aryNewsList.count == 0){
            callNewsListAPI()
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
   
    // MARK: - ---------------API CAlling
    // MARK: -

    func callNewsListAPI() {
        if !isInternetAvailable() {
            self.strErrorMessage = alertInternet
            self.tvlist.reloadData()
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetNews xmlns='http://aahar.org.in/'><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id></GetNews></soap12:Body></soap12:Envelope>"
            let loading = DPBasicLoading(table: self.tvlist, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")

            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetNewsResult", responcetype: "GetNewsResponse") { (responce, status) in
                loading.endLoading()
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.strErrorMessage = ""
                        let aryTemp = dictTemp.value(forKey: "FoodNameList")as! NSArray
                        self.aryNewsList = NSMutableArray()
                        self.aryNewsList = aryTemp.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                         self.aryNewsList = NSMutableArray()
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                     self.aryNewsList = NSMutableArray()
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
            }
        }
    }
    //MARK:
    //MARK:------------UpdateTimer For Advertisement
    
    @objc func updateTimer() {
        strAdvertiseTag  = strAdvertiseTag + 1
        let dict = aryAdvertise_Data.object(at: strAdvertiseTag)as! NSDictionary
        let urlImage = "\(dict.value(forKey: "url")!)" + "\(dict.value(forKey: "image")!)".replacingOccurrences(of: "~/", with: "")
        imgAdvertisement.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(strAdvertiseTag == aryAdvertise_Data.count - 1){
            strAdvertiseTag = 0
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NewsVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return aryNewsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath as IndexPath) as! NewsCell
        let dict = aryNewsList.object(at: indexPath.row)as! NSDictionary
        cell.lblDate.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
        cell.lblTitle.text = "\(dict.value(forKey: "Title")!)"
       cell.lblTitleDetail.text = "\(dict.value(forKey: "Message")!)".html2String
        cell.lblDate.text = "\(dict.value(forKey: "CreatedDate")!)"
        cell.imgNews.layer.borderColor = UIColor.lightGray.cgColor
        cell.imageView?.layer.borderWidth = 1.0
        let urlImage = "\(dict.value(forKey: "ImagePath")!)"
        cell.imgNews.setImageWith(URL(string: BaseURLImagePath + urlImage), placeholderImage: UIImage(named: "defult_news_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: NewsDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "NewsDetailVC") as! NewsDetailVC
        vc.dictNews =  (aryNewsList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class NewsCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleDetail: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
