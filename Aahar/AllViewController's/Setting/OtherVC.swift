//
//  OtherVC.swift
//  GreenGene
//
//  Created by Navin Patidar on 7/24/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class OtherVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryProfile = NSMutableArray()
    var aryPolicy = NSMutableArray()
    var aryOther = NSMutableArray()
    var aryPayment = NSMutableArray()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aryOther = [["title":"About App"],["title":"Team"],["title":"Developer Info"],["title":"Disclaimer"],["title":"Share App"],["title":"Version"]]
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
            heightHeader.constant = 94
        }else{
            heightHeader.constant = 74
        }

    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.view.endEditing(true)
        
                self.navigationController?.popViewController(animated: true)
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension OtherVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryOther.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath as IndexPath) as! OtherCell
      
            let strTitle = (aryOther[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.lblTitle.text = strTitle
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tvList.cellForRow(at: indexPath)as! OtherCell
        let title = "\(cell.lblTitle.text!)"
  

    if(title == "Statistics"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
    testController.strViewComeFrom = title
    testController.webURL = URL_statistics
    self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "Share App"){
        
         if let myApplink = NSURL(string: application_ID) {
              let objectsToShare = [AppName, myApplink] as [Any]
              let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
              activityVC.popoverPresentationController?.sourceView = self.view
              self.present(activityVC, animated: true, completion: nil)
          }
   }
   
   else if(title == "Team"){
   let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
     testController.strViewComeFrom = title
     testController.webURL = URL_Team
     self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "About App"){
   let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
       testController.strViewComeFrom = title
       testController.webURL = URL_about
       self.navigationController?.pushViewController(testController, animated: true)
   }
   
   else if(title == "Developer Info"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
        testController.strViewComeFrom = title
        testController.webURL = URL_DevelopersInfo
        self.navigationController?.pushViewController(testController, animated: true)
   }
  
   
   else if(title == "Disclaimer"){
    let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
        testController.strViewComeFrom = title
        testController.webURL = URL_Disclaimer
        self.navigationController?.pushViewController(testController, animated: true)
    
        }
      
   else if(title == "Version"){
       showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:"App Version : \(app_Version)\nDate : \(app_VersionDate)\n\(app_VersionSupport)" , viewcontrol: self)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if(section == 0){
//         return aryProfile.count == 0 ? 0 : 50
//        }
//        return 50
         return 0
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if(section == 0){
//            return "Profile"
//        }else if(section == 1){
//            return "Policy"
//        }else if(section == 2){
//            return "Payment"
//        }else if(section == 3){
//            return "Other"
//        }
        return ""
    }
}

// MARK: - ----------------VendorDiscount Cell
// MARK: -
class OtherCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
