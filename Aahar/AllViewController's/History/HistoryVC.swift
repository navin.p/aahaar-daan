//
//  HistoryVC.swift
//  Aahar
//
//  Created by Navin Patidar on 11/12/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {
    
    //MARK: variable
    //MARK--------------
    var aryDonatedList = NSMutableArray()
    var aryProcuredList = NSMutableArray()
    var aryDistributed = NSMutableArray()
    var aryTvlist = NSMutableArray()
    var strErrorMessage  = ""
    var refreshControl = UIRefreshControl()
    
    //MARK: IBOutlet
    //MARK--------------
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    //MARK: Life Cycle
    //MARK--------------
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                         heightHeader.constant = 94
                     }else{
                         heightHeader.constant = 74
                     }
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 130
        callGetDonatedListAPI(loader: true)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh...")
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tvlist.addSubview(refreshControl) // not required when using UITableViewController
        let font = UIFont.boldSystemFont(ofSize: 15)
        segment.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                       for: .normal)
        segment.selectedSegmentIndex = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         if(segment.selectedSegmentIndex == 0){  // For Donated
                      self.callGetDonatedListAPI(loader: false)
                  }else if (segment.selectedSegmentIndex == 1){ // For Procured
                      self.callGetProcuredListAPI(loader: false)
                  }else if (segment.selectedSegmentIndex == 2){ // Distributed
                      self.callGetDistributedListAPI(loader: false)
                  }

       // segment.tintColor =  hexStringToUIColor(hex: primaryGreenColor)

    
    }
    
    
    
    @objc func refresh(sender:AnyObject) {
        
        if(segment.selectedSegmentIndex == 0){  // For Donated
            self.callGetDonatedListAPI(loader: false)
        }else if (segment.selectedSegmentIndex == 1){ // For Procured
            self.callGetProcuredListAPI(loader: false)
        }else if (segment.selectedSegmentIndex == 2){ // Distributed
            self.callGetDistributedListAPI(loader: false)
        }
    }
    //MARK: IBAction
    //MARK--------------
    // MARK: - ---------------IBAction
      // MARK: -
      @IBAction func actiononBack(_ sender: UIButton) {
          self.view.endEditing(true)
          self.navigationController?.popViewController(animated: true)
      }
      
    @IBAction func actionOnSegment(_ sender: UISegmentedControl) {
        strErrorMessage  = ""
        self.aryTvlist = NSMutableArray()
        self.tvlist.reloadData()
        if(sender.selectedSegmentIndex == 0){  // For Donated
            
            if(aryDonatedList.count == 0){
                self.callGetDonatedListAPI(loader: true)
            }else{
                self.aryTvlist = NSMutableArray()
                self.aryTvlist = aryDonatedList
                self.tvlist.reloadData()
                
            }
        }else if (sender.selectedSegmentIndex == 1){ // For Procured
            if(aryProcuredList.count == 0){
                self.callGetProcuredListAPI(loader: true)
            }else{
                self.aryTvlist = NSMutableArray()
                self.aryTvlist = aryProcuredList
                self.tvlist.reloadData()
                
            }
        }else if (sender.selectedSegmentIndex == 2){ // Distributed
            if(aryDistributed.count == 0){
                self.callGetDistributedListAPI(loader: true)
            }else{
                self.aryTvlist = NSMutableArray()
                self.aryTvlist = aryDistributed
                self.tvlist.reloadData()
                
            }
        }
   }
}


//MARK: API Calling
//MARK--------------
extension HistoryVC {
    func callGetDonatedListAPI(loader : Bool) {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
            
        }else{
            var alert = UIAlertController()
            if(loader){
                alert =  loader_Show(controller: self, strMessage: "Loading...")
            }
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Get_DonateFoodHistory xmlns='http://aahar.org.in/'><DonorVID>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVID><Filter></Filter></Get_DonateFoodHistory></soap12:Body></soap12:Envelope>"
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Get_DonateFoodHistoryResult", responcetype: "Get_DonateFoodHistoryResponse") { (responce, status) in
                self.aryTvlist = NSMutableArray()
                alert.dismiss(animated: false) {}
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        if(dictTemp.value(forKey: "Detail") is NSArray){
                            self.aryDonatedList = NSMutableArray()
                            self.aryDonatedList = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                            self.aryTvlist = NSMutableArray()
                            self.aryTvlist = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                        }
                        if( self.aryTvlist.count == 0){
                            self.strErrorMessage = alertDataNotFound
                            self.tvlist.reloadData()
                        }else{
                            self.strErrorMessage = ""
                            self.tvlist.reloadData()
                        }
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func callGetProcuredListAPI(loader : Bool) {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            var alert = UIAlertController()
            if(loader){
                alert =  loader_Show(controller: self, strMessage: "Loading...")
            }
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Get_ProcureFoodHistory xmlns='http://aahar.org.in/'><DonorVID>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVID><Filter></Filter></Get_ProcureFoodHistory></soap12:Body></soap12:Envelope>"
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Get_ProcureFoodHistoryResult", responcetype: "Get_ProcureFoodHistoryResponse") { (responce, status) in
                
                self.aryTvlist = NSMutableArray()
                alert.dismiss(animated: false) {}
                
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        if(dictTemp.value(forKey: "Detail") is NSArray){
                            self.aryProcuredList = NSMutableArray()
                            self.aryProcuredList = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                            self.aryTvlist = NSMutableArray()
                            self.aryTvlist = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                        }
                        if( self.aryTvlist.count == 0){
                            self.strErrorMessage = alertDataNotFound
                            self.tvlist.reloadData()
                        }else{
                            self.strErrorMessage = ""
                            self.tvlist.reloadData()
                        }
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                    
                }
                self.refreshControl.endRefreshing()
                
                
            }
        }
        
    }
    func callGetDistributedListAPI(loader : Bool) {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
            
        }else{
            var alert = UIAlertController()
            if(loader){
                alert =  loader_Show(controller: self, strMessage: "Loading...")
            }
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Get_DistributedFoodHistory xmlns='http://aahar.org.in/'><DonorVID>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVID><Filter></Filter></Get_DistributedFoodHistory></soap12:Body></soap12:Envelope>"
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Get_DistributedFoodHistoryResult", responcetype: "Get_DistributedFoodHistoryResponse") { (responce, status) in
                self.aryTvlist = NSMutableArray()
                
                alert.dismiss(animated: false) {}
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        if(dictTemp.value(forKey: "Detail") is NSArray){
                            self.aryDistributed = NSMutableArray()
                            self.aryDistributed = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                            self.aryTvlist = NSMutableArray()
                            self.aryTvlist = (dictTemp.value(forKey: "Detail")as! NSArray).mutableCopy()as! NSMutableArray
                        }
                        if( self.aryTvlist.count == 0){
                            self.strErrorMessage = alertDataNotFound
                            self.tvlist.reloadData()
                        }else{
                            self.strErrorMessage = ""
                            self.tvlist.reloadData()
                        }
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
                self.refreshControl.endRefreshing()
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension HistoryVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTvlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DonatedCell", for: indexPath as IndexPath) as! ProcureSurPlusFoodCell
        let dict = aryTvlist.object(at: indexPath.row)as! NSDictionary
        //For Donated
        if(self.segment.selectedSegmentIndex == 0){
            cell.lbl_name.text = "\(dict.value(forKey: "InstitutionName")!)"
            if("\(dict.value(forKey: "InstitutionName")!)" != ""){
                       cell.lbl_name.text = "\(dict.value(forKey: "Name")!) - \(dict.value(forKey: "InstitutionName")!)"
                   }
            cell.lbl_address.text = "\(dict.value(forKey: "Address")!)"
            cell.lbl_contactNo.text = "\(dict.value(forKey: "ContactPersonName")!)(\(dict.value(forKey: "ContactPersonNo")!))"
            cell.lbl_food_picked_time.text = "\(dict.value(forKey: "FoodCollectStartTime")!) - \(dict.value(forKey: "FoodCollectEndTime")!)"
            cell.lbl_date_Time.text = "Date : \(dict.value(forKey: "CreatedDate")!)"
            cell.lbl_date_Time.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
            cell.btn_procure.layer.cornerRadius = 5.0
            
            if(("\(dict.value(forKey: "ProcureTimeOver")!)" == "Procure Time Over") && ("\(dict.value(forKey: "BookedStatus")!)" == "Unbooked")){
                cell.btn_procure.setTitle("TIME OUT", for: .normal)
                cell.btn_procure.backgroundColor = UIColor(displayP3Red: 255/255.0, green: 171/255.0, blue: 0/255.0, alpha: 1.0)
                
            }else if ("\(dict.value(forKey: "BookedStatus")!)" == "Booked"){
                
                if ("\(dict.value(forKey: "FStatus")!)" == "Distributed"){
                    cell.btn_procure.setTitle("DISTRIBUTED", for: .normal)
                    cell.btn_procure.backgroundColor = UIColor.darkGray
                    
                }else{
                    cell.btn_procure.setTitle("BOOKED", for: .normal)
                    cell.btn_procure.backgroundColor = UIColor.red
                }
                
                
            } else {
                
                cell.btn_procure.setTitle("", for: .normal)
                cell.btn_procure.backgroundColor = UIColor.clear
            }
            cell.btn_procure.layer.cornerRadius = 4.0
            cell.btn_procure.tag = indexPath.row
            cell.btn_procure.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
        }
            //   For Procure
            
            
        else if (self.segment.selectedSegmentIndex == 1){
            cell.lbl_name.text = "\(dict.value(forKey: "DonateInstitutionName")!)"
   
            if("\(dict.value(forKey: "DonateInstitutionName")!)" != ""){
                                  cell.lbl_name.text = "\(dict.value(forKey: "DonatePersonName")!) - \(dict.value(forKey: "DonateInstitutionName")!)"
                              }
            
            
            cell.lbl_address.text = "\(dict.value(forKey: "DonorAddress")!)"
            cell.lbl_contactNo.text = "\(dict.value(forKey: "DonateContactPersonName")!)(\(dict.value(forKey: "DonateContactPersonNo")!))"
            cell.lbl_food_picked_time.text = "\(dict.value(forKey: "FoodCollectStartTime")!) - \(dict.value(forKey: "FoodCollectEndTime")!)"
            cell.lbl_date_Time.text = "Date : \(dict.value(forKey: "ProcureDate")!)"
            cell.lbl_date_Time.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
            cell.btn_procure.layer.cornerRadius = 5.0
            
            cell.btn_procure.setTitle("PROCURED", for: .normal)
            cell.btn_procure.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
            cell.btn_procure.layer.cornerRadius = 4.0
            cell.btn_procure.tag = indexPath.row
            cell.btn_procure.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
        }else if (self.segment.selectedSegmentIndex == 2){
            cell.lbl_name.text = "\(dict.value(forKey: "DonateInstitutionName")!)"
            if("\(dict.value(forKey: "DonateInstitutionName")!)" != ""){
                                          cell.lbl_name.text = "\(dict.value(forKey: "DonatePersonName")!) - \(dict.value(forKey: "DonateInstitutionName")!)"
                                      }
            cell.lbl_address.text = "\(dict.value(forKey: "DonorAddress")!)"
            cell.lbl_contactNo.text = "\(dict.value(forKey: "DonateContactPersonName")!)(\(dict.value(forKey: "DonateContactPersonNo")!))"
            cell.lbl_food_picked_time.text = "\(dict.value(forKey: "FoodCollectStartTime")!) - \(dict.value(forKey: "FoodCollectEndTime")!)"
            cell.lbl_date_Time.text = "Date : \(dict.value(forKey: "ProcureDate")!)"
            cell.lbl_date_Time.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
            cell.btn_procure.layer.cornerRadius = 5.0
            
            cell.btn_procure.setTitle("DISTRIBUTED", for: .normal)
            cell.btn_procure.setTitleColor(UIColor.white, for: .normal)
            cell.btn_procure.backgroundColor = UIColor.darkGray
            cell.btn_procure.layer.cornerRadius = 4.0
            cell.btn_procure.tag = indexPath.row
            cell.btn_procure.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
        }
        
        
        
        
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //For Donated
        if(self.segment.selectedSegmentIndex == 0){
            
            
            let dict = aryTvlist.object(at: indexPath.row)as! NSDictionary
            if(("\(dict.value(forKey: "ProcureTimeOver")!)" == "Procure Time Over") && ("\(dict.value(forKey: "BookedStatus")!)" == "Unbooked")){
                let alert = UIAlertController(title: alertMessage, message: "Indicated convenient Time Out.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true)
                
            }else{
                let vc: ProcureFoodDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureFoodDetailVC") as! ProcureFoodDetailVC
                vc.dictData = dict.mutableCopy()as! NSMutableDictionary
                vc.strComeFrom = "Donated"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
            
        }else if(self.segment.selectedSegmentIndex == 1){
            let dict = aryTvlist.object(at: indexPath.row)as! NSDictionary
            let vc: ProcureFoodDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureFoodDetailVC") as! ProcureFoodDetailVC
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            vc.strComeFrom = "Procured"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(self.segment.selectedSegmentIndex == 2){
            let dict = aryTvlist.object(at: indexPath.row)as! NSDictionary
            let vc: ProcureFoodDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureFoodDetailVC") as! ProcureFoodDetailVC
            vc.dictData = dict.mutableCopy()as! NSMutableDictionary
            vc.strComeFrom = "Distributed"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
        
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 120))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 120))
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return  strErrorMessage.count == 0 ? 0 : 120
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    
    
    @objc func pressButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
    }
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class HistoryCell: UITableViewCell {
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
