//
//TAG = 1,2,3 Country , State,City        Registration Screen
//TAG = 4,5,6  state , city Type                Edit Profile
// TAG = 7 DonatedFreshFood
// TAG = 8 DonatedDiscountFood
// TAG = 9 , 10    DonatedSurPluse , Select Contact
//TAg = 11 For Fliter ProcureSurpluse
//TAG = 12 ProcureFoodDetailScreen
//TAG = 13 ProcureDiscountedPackedItemDetailScreenDelegate
//TAG = 14 Login
//TAG = 15 ProcureFoodDetail Person
//TAG = 16 Zone


import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol PopUpViewDelegate : class{
    func GetDictFromPopUPScreen(dictData : NSDictionary ,tag : Int)
}


class PopUpView: UIViewController {
    
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var viewFortv: CardView!

    
    var strTitle = String()
    var aryTBL = NSMutableArray()
    var strTag = Int()
    
    
    weak var handleRegistrationView: RegistrationScreenDelegate?
    weak var handleEditProfileScreen: EditProfileScreenDelegate?
    weak var handleDonateFreshfoodScreen: DonateFreshfoodScreenDelegate?
    weak var handleDonatefoodDiscountScreen: DonatefoodDiscountScreenDelegate?
    weak var handleDonateSurPluseScreen: DonateSurPluseScreenDelegate?
    weak var handleProcureFoodDetailScreen: ProcureFoodDetailScreenDelegate?
    weak var handleProcureDiscountedPackedItemDetailScreen: ProcureDiscountedPackedItemDetailScreenDelegate?
    weak var delegate: PopUpViewDelegate?





    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.estimatedRowHeight = 50.0
        tvForList.tableFooterView = UIView()
        tvForList.dataSource = self
        tvForList.delegate = self
        self.view.backgroundColor = UIColor.clear
        print(strTag)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
     
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
      self.dismiss(animated: false) { }
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PopUpView : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryTBL.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvForList.dequeueReusableCell(withIdentifier: "popupCell", for: indexPath as IndexPath) as! popupCell
        let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
        //Ragistration Screen
        if(strTag == 1  || strTag == 14){
            cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "country_code")as! String)" + ",\(dict?.value(forKey: "country_name")as! String)"
        }else if(strTag == 2 || strTag == 4){
            cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "state_name")as! String)"

        }else if(strTag == 3 || strTag == 5){
            cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "city_name")as! String)"
        }
        else if(strTag == 7 || strTag == 8 || strTag == 10 || strTag == 12  || strTag == 13 || strTag == 15){
            cell.popupCell_lbl_Title.text =  (dict?.value(forKey: "ContactPersonName")as! String) + " (\(dict?.value(forKey: "ContactPersonNo")! ?? 0))"
        }
        else if(strTag == 9){
            cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "FoodName")as! String)"
        }
            else if(strTag == 16){
                       cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "Zone")as! String)"
                   }
        //----
        else{
            cell.popupCell_lbl_Title.text = "\(dict?.value(forKey: "name")as! String)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryTBL.object(at: indexPath.row)as? NSDictionary
        if(strTag == 1 || strTag == 2 || strTag == 3){
            handleRegistrationView?.refreshRegistrationScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 4 || strTag == 5 || strTag == 6 || strTag == 16){
            handleEditProfileScreen?.refreshEditProfileScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 7){
            handleDonateFreshfoodScreen?.refreshDonateFreshfoodScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 8){
            handleDonatefoodDiscountScreen?.refreshDonatefoodDiscountScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 9) || (strTag == 10) {
            handleDonateSurPluseScreen?.refreshDonateSurPluseScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 11){
          //  handleProcureSurPlusScreen?.refreshProcureSurPlusScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 12 || strTag == 15){
            handleProcureFoodDetailScreen?.refreshProcureFoodDetailScreen(dictData: dict!, tag: strTag)
        }else if (strTag == 13){
            handleProcureDiscountedPackedItemDetailScreen?.refreshProcureDiscountedPackedItemDetailScreen(dictData: dict!, tag: strTag)
        }
        else if (strTag == 14){
            delegate?.GetDictFromPopUPScreen(dictData: dict!, tag: strTag) 
        }
        self.dismiss(animated: false) {}
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

class popupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var popupCell_lbl_Title: UILabel!
}


