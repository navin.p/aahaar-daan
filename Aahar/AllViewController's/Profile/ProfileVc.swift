//
//  ProfileVc.swift
//  Aahar
//
//  Created by Navin Patidar on 2/26/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class ProfileVc: UIViewController {
    
    var aryProfileList = NSMutableArray()
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtType: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtState: ACFloatingTextfield!
    @IBOutlet weak var txtCity: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtZone: ACFloatingTextfield!

    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!


    // MARK: - ---------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                  heightHeader.constant = 94
              }else{
                  heightHeader.constant = 74
              }

        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnEdit.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setDataOnProfileScreen()
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononEdit(_ sender: UIButton) {
       
        let vc: EditProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    func setDataOnProfileScreen() {
        if(dict_Login_Data.count != 0){
            print(dict_Login_Data)
            txtName.text =  "\(dict_Login_Data.value(forKey: "Name")!)"
            let strType = "\(dict_Login_Data.value(forKey: "UserType")!) " + "(\(dict_Login_Data.value(forKey: "InstitutionName")!))"
          txtType.text =  (strType.replacingOccurrences(of: "()", with: ""))
            txtAddress.text = "\(dict_Login_Data.value(forKey: "Address")!)"
            txtCity.text = "\(dict_Login_Data.value(forKey: "city_name")!)"
            txtState.text = "\(dict_Login_Data.value(forKey: "state_name")!)"
            txtMobileNumber.text = "\(dict_Login_Data.value(forKey: "MobileNo")!)"
            txtEmail.text = "\(dict_Login_Data.value(forKey: "EmailId")!)"
            txtZone.text = "\(dict_Login_Data.value(forKey: "Zone")!)"

            
            
            let urlImage = "http://aahar.org.in/ProfileImage/\(dict_Login_Data.value(forKey: "ProfileImage")!)"
            imgProfile.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "profile"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            imgProfile.layer.borderWidth = 1.0
            imgProfile.layer.masksToBounds = false
            imgProfile.layer.cornerRadius = imgProfile.frame.height/2
            imgProfile.clipsToBounds = true
            imgProfile.layer.borderColor = UIColor.white.cgColor
           imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration:1.5,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.imgProfile.transform = .identity
                },
                           completion: nil)
          //  var strGender = "\(dict_Login_Data.value(forKey: "Gender")!)"
      
             aryProfileList = NSMutableArray()
            aryProfileList = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
            var index = 0
            for item in aryProfileList {
                let strName = (item as AnyObject).value(forKey: "ContactPersonName")as! String
                let strNumber = (item as AnyObject).value(forKey: "ContactPersonNo")as! String
                 let strName1 = "\(dict_Login_Data.value(forKey: "Name")!)"
                let strNumber1 = "\(dict_Login_Data.value(forKey: "MobileNo")!)"
                if((strName.lowercased() == strName1.lowercased()) && (strNumber == strNumber1)){
                  aryProfileList.removeObject(at: index)
                }
                index = index + 1
            }
            tvlist.reloadData()
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ProfileVc : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryProfileList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath as IndexPath) as! ProfileCell
        let dict = aryProfileList.object(at: indexPath.row)as! NSDictionary
        cell.txtName.text = "\(dict.value(forKey: "ContactPersonName")!)"
        cell.txtMobile.text = "\(dict.value(forKey: "ContactPersonNo")!)"
        cell.txtEmail.text = "\(dict.value(forKey: "ContactPersonEmail")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
    }
}
// MARK: - ----------------ProfileCell
// MARK: -
class ProfileCell: UITableViewCell {
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
