//
//  GalleryVC.swift
//  Aahar
//
//  Created by Navin on 2/21/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class GalleryVC: UIViewController {
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    var aryGalleryList = NSMutableArray()
    @IBOutlet weak var imgAdvertisement: UIImageView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var timer = Timer()
   var strAdvertiseTag  = 0
    var strErrorMessage  = ""

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        if(aryAdvertise_Data.count != 0){
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)

        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)

        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 185
        if(DeviceType.IS_IPHONE_X){
                   heightHeader.constant = 94
               }else{
                   heightHeader.constant = 74
               }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(aryGalleryList.count == 0){
            callGalleryListAPI()
        }
    
    }
    //MARK:
    //MARK:------------UpdateTimer For Advertisement
    @objc func updateTimer() {
        strAdvertiseTag  = strAdvertiseTag + 1
        let dict = aryAdvertise_Data.object(at: strAdvertiseTag)as! NSDictionary
        let urlImage = "\(dict.value(forKey: "url")!)" + "\(dict.value(forKey: "image")!)".replacingOccurrences(of: "~/", with: "")
        imgAdvertisement.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(strAdvertiseTag == aryAdvertise_Data.count - 1){
            strAdvertiseTag = 0
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK:*********
    //MARK: API CAlling
    func callGalleryListAPI() {
        self.strErrorMessage = ""
        if !isInternetAvailable() {
            self.strErrorMessage = alertInternet
            self.tvlist.reloadData()
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGalleryCityWise xmlns='http://aahar.org.in/'><MediaType>image</MediaType><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id></GetGalleryCityWise></soap12:Body></soap12:Envelope>"
            
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetGalleryCityWiseResult", responcetype: "GetGalleryCityWiseResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "Detail")as! NSArray
                        self.aryGalleryList = NSMutableArray()
                        self.aryGalleryList = aryTemp.mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
            }
        }
        
    }
 
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension GalleryVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return aryGalleryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ViewGalleryCell", for: indexPath as IndexPath) as! ViewGalleryCell
        let dict = aryGalleryList.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dict.value(forKey: "Title")!)"
    //    cell.lblTitle.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
        cell.lblTitle.textColor = UIColor.white
        let urlImage = "http://aahar.org.in/GalleryImages/\(dict.value(forKey: "MediaPath")!)"
        cell.imgGallery.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "GalleryDetailVC")as! GalleryDetailVC
        testController.aryGallery = aryGalleryList
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    
}


// MARK: - ----------------UserDashBoardCell
// MARK: -
class ViewGalleryCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgGallery: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
