//
//  EditProfileVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/26/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol EditProfileScreenDelegate : class{
    func refreshEditProfileScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------RegistrationScreenDelegate Methods----------

extension EditProfileVC: EditProfileScreenDelegate {
    func refreshEditProfileScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        if (tag == 4){
            txtState.text = (dictData.value(forKey: "state_name")as! String)
            txtState.tag = Int("\(dictData.value(forKey: "state_id")!)")!
            txtCity.text = ""
            txtCity.tag = 0
            txtZone.text = ""
            txtZone.tag = 0
            aryCity = NSMutableArray()
        }else if (tag == 5){
            txtCity.text = (dictData.value(forKey: "city_name")as! String)
            txtCity.tag = Int("\(dictData.value(forKey: "city_id")!)")!
            txtZone.text = ""
            txtZone.tag = 0
        }
        else if (tag == 6){
            txtType.text = (dictData.value(forKey: "name")as! String)
            txtType.tag = Int("\(dictData.value(forKey: "id")!)")!
            if(txtType.text!  == "Organization"){
                heightOrganizationName.constant = 55
                lblAddNewUser.isHidden = false
                viewAddContact.isHidden = false
            }else{
                heightOrganizationName.constant = -8
                lblAddNewUser.isHidden = true
                viewAddContact.isHidden = true
                self.aryProfileList = NSMutableArray()
                self.tvlist.reloadData()
            }
        }
        else if (tag == 16){
            txtZone.text = (dictData.value(forKey: "Zone")as! String)
            txtZone.tag = Int("\(dictData.value(forKey: "ZoneId")!)")!
        }
    }
}
class EditProfileVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewHeader1: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    
    @IBOutlet weak var txtType: ACFloatingTextfield!
    @IBOutlet weak var txtOrganizationName: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var txtState: ACFloatingTextfield!
    @IBOutlet weak var txtCity: ACFloatingTextfield!
    @IBOutlet weak var txtZone: ACFloatingTextfield!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnBrowse: UIButton!
    @IBOutlet weak var viewAddContact: CardView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnSelectZone: UIButton!
    
    
    @IBOutlet weak var heightOrganizationName: NSLayoutConstraint!
    @IBOutlet weak var heightCollectionAddress: NSLayoutConstraint!
    
    @IBOutlet weak var txtEditName: ACFloatingTextfield!
    @IBOutlet weak var txtEditMobileNumber: ACFloatingTextfield!
    @IBOutlet weak var txtEditEmail: ACFloatingTextfield!
    @IBOutlet weak var btnEditMale: UIButton!
    @IBOutlet weak var btnEditFemale: UIButton!
    @IBOutlet weak var btnEditRemove: UIButton!
    @IBOutlet weak var btnEditSave: UIButton!
    @IBOutlet weak var viewAddNewContact: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addressProofMessage: UILabel!
    
    @IBOutlet weak var lblAddNewUser: UILabel!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    
    
    var dictDefaultUser = NSMutableDictionary()
    var picker = UIImagePickerController()
    var aryCountry = NSMutableArray()
    var aryState = NSMutableArray()
    var aryCity = NSMutableArray()
    var aryZone = NSMutableArray()
    
    var strProfileName = ""
    var strAddressProofName1 = ""
    var strAddressProofName2 = ""
    var updateTag = 0
    var dictNewContact = NSMutableDictionary()
    var aryAddressProff = NSMutableArray()
    var aryProfileList = NSMutableArray()
    var aryDefaultProfile = NSMutableArray()
    var aryCollection = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
            heightHeader.constant = 94
        }else{
            heightHeader.constant = 74
        }
        
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        viewHeader1.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        viewAddContact.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnBrowse.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnBrowse.layer.cornerRadius = 4.0
        btnUpdate.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        
        btnEditRemove.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnEditRemove.layer.cornerRadius = 4.0
        btnEditSave.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnEditSave.layer.cornerRadius = 4.0
        
        collectionView.layer.cornerRadius = 4.0
        collectionView.layer.borderColor  =  UIColor.lightGray.cgColor
        collectionView.layer.borderWidth = 1.0
        setDataOnProfileScreen()
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        var tagBack = 0
        for item in self.view.subviews {
            if(item == self.viewAddNewContact){
                tagBack = 1
            }else{
            }
        }
        if (tagBack == 1){
            self.viewAddNewContact.removeFromSuperview()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func actiononEdit(_ sender: UIButton) {
        picker.view.tag = 1
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: Alert_Camera, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera(UIImagePickerController.SourceType.camera)
        }
        let galleryAction = UIAlertAction(title: Alert_Gallery, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera(UIImagePickerController.SourceType.photoLibrary)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func actiononUpdate(_ sender: UIButton) {
        if( validation()){
            if(strProfileName.count != 0 && strProfileName != "\(dict_Login_Data.value(forKey: "ProfileImage")!)"){
                self.uploadProfileImage()
                
            }else if (aryAddressProff.count != 0){
                let dict = aryAddressProff.lastObject as! NSDictionary
                self.uploadAddressImage(image: (dict.value(forKey: "image") as! UIImage))
                
            }else{
                self.callUpdateDataAPI(sender: sender)
            }
            
        }
    }
    @IBAction func actiononBrowse(_ sender: UIButton) {
        if aryCollection.count == 0  || aryCollection.count == 1{
            picker.view.tag = 2
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            let cameraAction = UIAlertAction(title: Alert_Camera, style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.openCamera(UIImagePickerController.SourceType.camera)
            }
            let galleryAction = UIAlertAction(title: Alert_Gallery, style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.openCamera(UIImagePickerController.SourceType.photoLibrary)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
            }
            
            // Add the actions
            picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            alert.addAction(cameraAction)
            alert.addAction(galleryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReadingImageLimit, viewcontrol: self)
        }
        
    }
    func openCamera(_ sourceType: UIImagePickerController.SourceType) {
        picker.sourceType = sourceType
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func actiononState(_ sender: UIButton) {
        sender.tag = 4
        if aryState.count != 0 {
            gotoPopViewWithArray(sender: sender, aryData: aryState, strTitle: "")
        }else{
            callstateAPI(sender: sender)
        }
    }
    @IBAction func actiononCity(_ sender: UIButton) {
        sender.tag = 5
        
        if txtState.tag != 0{
            if aryCity.count != 0 {
                gotoPopViewWithArray(sender: sender, aryData: aryCity, strTitle: "")
            }else{
                callCityAPI(sender: sender)
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Country, viewcontrol: self)
        }
    }
    @IBAction func actiononSelectZone(_ sender: UIButton) {
        sender.tag = 16
        
        if aryZone.count != 0 {
            gotoPopViewWithArray(sender: sender, aryData: aryZone, strTitle: "")
        }else{
            callZoneAPI(sender: sender)
        }
        
    }
    @IBAction func actiononOrganizationType(_ sender: UIButton) {
        sender.tag = 6
        let aryOrganizationType = NSMutableArray()
        let dict1 = NSMutableDictionary()
        dict1.setValue("Organization", forKey: "name")
        dict1.setValue("1", forKey: "id")
        let dict2 = NSMutableDictionary()
        dict2.setValue("Individual", forKey: "name")
        dict2.setValue("2", forKey: "id")
        aryOrganizationType.add(dict1)
        aryOrganizationType.add(dict2)
        gotoPopViewWithArray(sender: sender, aryData: aryOrganizationType, strTitle: "")
    }
    
    @IBAction func actiononAddUser(_ sender: UIButton) {
        if (validation()){
            
            if(aryDefaultProfile.count != 0){
                ((aryDefaultProfile.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary).setValue(txtEmail.text!, forKey: "ContactPersonEmail")
                ((aryDefaultProfile.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary).setValue(btnFemale.currentImage == UIImage(named: "radio_1") ? "FeMale" : "Male", forKey: "ContactPersonGender")
                ((aryDefaultProfile.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary).setValue(txtName.text!, forKey: "ContactPersonName")
                ((aryDefaultProfile.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary).setValue(txtMobileNumber.text!, forKey: "ContactPersonNo")
                ((aryDefaultProfile.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary).setValue(txtName.text!, forKey: "Name")
                
            }
            
            
            viewAddNewContact.frame = CGRect(x: 0, y: self.viewHeader.frame.maxY, width: self.view.frame.width, height: self.view.frame.height -  self.viewHeader.frame.maxY)
            dictNewContact = NSMutableDictionary()
            dictNewContact.setValue("", forKey: "ContactPersonEmail")
            dictNewContact.setValue("", forKey: "ContactPersonGender")
            dictNewContact.setValue("", forKey: "ContactPersonName")
            dictNewContact.setValue("", forKey: "ContactPersonNo")
            dictNewContact.setValue("0", forKey: "DVOtherContactId")
            dictNewContact.setValue("\(dict_Login_Data.value(forKey: "DonorVId")!)", forKey: "DonorVId")
            dictNewContact.setValue("", forKey: "IsActive")
            dictNewContact.setValue("\(dict_Login_Data.value(forKey: "Name")!)", forKey: "Name")
            
            txtEditName.text = "\(dictNewContact.value(forKey: "ContactPersonName")!)"
            txtEditMobileNumber.text = "\(dictNewContact.value(forKey: "ContactPersonNo")!)"
            txtEditEmail.text = "\(dictNewContact.value(forKey: "ContactPersonEmail")!)"
            let strGender = "\(dictNewContact.value(forKey: "ContactPersonGender")!)"
            if(strGender == "Male"){
                btnEditMale.setImage(UIImage(named: "radio_1"), for: .normal)
                btnEditFemale.setImage(UIImage(named: "radio_2"), for: .normal)
            }else{
                btnEditMale.setImage(UIImage(named: "radio_2"), for: .normal)
                btnEditFemale.setImage(UIImage(named: "radio_1"), for: .normal)
            }
            if (strGender == ""){
                btnEditMale.setImage(UIImage(named: "radio_1"), for: .normal)
                btnEditFemale.setImage(UIImage(named: "radio_2"), for: .normal)
            }
            updateTag = 99999
            self.view.addSubview(viewAddNewContact)
        }
    }
    
    @IBAction func actiononRadioButton(_ sender: UIButton) {
        if sender.tag == 1 { //Male
            btnMale.setImage(UIImage(named: "radio_1"), for: .normal)
            btnFemale.setImage(UIImage(named: "radio_2"), for: .normal)
        }else{
            btnMale.setImage(UIImage(named: "radio_2"), for: .normal)
            btnFemale.setImage(UIImage(named: "radio_1"), for: .normal)
        }
    }
    @IBAction func actionOnSaveNewUser(_ sender: UIButton) {
        if(validationEdit()){
            dictNewContact.setValue(txtEditEmail.text!, forKey: "ContactPersonEmail")
            if(btnEditFemale.currentImage == UIImage(named: "radio_1")){
                dictNewContact.setValue(btnEditFemale.titleLabel?.text!, forKey: "ContactPersonGender")
            }else{
                dictNewContact.setValue(btnEditMale.titleLabel?.text!, forKey: "ContactPersonGender")
            }
            dictNewContact.setValue(txtEditName.text!, forKey: "ContactPersonName")
            dictNewContact.setValue(txtEditMobileNumber.text!, forKey: "ContactPersonNo")
            if(updateTag == 99999){
                aryProfileList.add(dictNewContact)
                self.viewAddNewContact.removeFromSuperview()
            }else{
                aryProfileList.replaceObject(at: updateTag, with: dictNewContact)
            }
            self.tvlist.reloadData()
            if(aryProfileList.count >= 5 ){
                viewAddContact.isHidden  = true
            }else{
                viewAddContact.isHidden  = false
            }
        }
        self.viewAddNewContact.removeFromSuperview()
    }
    @IBAction func actionOnRemove(_ sender: UIButton) {
        
        let alert = UIAlertController(title: alertMessage, message: alertRemove, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { action in
            print("Yay! You brought your towel!")
        }))
        alert.addAction(UIAlertAction(title: "REMOVE", style: .destructive, handler: { action in
            if(self.updateTag != 99999){
                self.aryProfileList.removeObject(at: self.updateTag)
            }
            self.viewAddNewContact.removeFromSuperview()
            self.tvlist.reloadData()
            if(self.aryProfileList.count >= 5 ){
                self.viewAddContact.isHidden  = true
            }else{
                self.viewAddContact.isHidden  = false
            }
        }))
        self.present(alert, animated: true)
    }
    @IBAction func actiononEditRadioButton(_ sender: UIButton) {
        if sender.tag == 1 { //Male
            btnEditMale.setImage(UIImage(named: "radio_1"), for: .normal)
            btnEditFemale.setImage(UIImage(named: "radio_2"), for: .normal)
        }else{
            btnEditMale.setImage(UIImage(named: "radio_2"), for: .normal)
            btnEditFemale.setImage(UIImage(named: "radio_1"), for: .normal)
        }
    }
    
    //MARK:
    //MARK:-------------SetDataOnProfileScreen
    
    func setDataOnProfileScreen() {
        if(dict_Login_Data.count != 0){
            self.strAddressProofName1 = "\(dict_Login_Data.value(forKey: "AddressProof1")!)"
            self.strAddressProofName2 = "\(dict_Login_Data.value(forKey: "AddressProof2")!)"
            self.strProfileName = "\(dict_Login_Data.value(forKey: "ProfileImage")!)"
            txtName.text =  "\(dict_Login_Data.value(forKey: "Name")!)"
            txtType.text = "\(dict_Login_Data.value(forKey: "UserType")!)"
            txtAddress.text = "\(dict_Login_Data.value(forKey: "Address")!)"
            txtCity.text = "\(dict_Login_Data.value(forKey: "city_name")!)"
            txtState.text = "\(dict_Login_Data.value(forKey: "state_name")!)"
            txtZone.text = "\(dict_Login_Data.value(forKey: "Zone")!)"
            
            let cityID = "\(dict_Login_Data.value(forKey: "city_id")!)"
            let state_ID = "\(dict_Login_Data.value(forKey: "state_id")!)"
            let zone_ID = "\(dict_Login_Data.value(forKey: "ZoneId")!)"
            
            txtState.tag = Int(state_ID == "" ?  "0" : state_ID)!
            txtCity.tag = Int(cityID == "" ?  "0" : cityID)!
            txtZone.tag = Int(zone_ID == "" ?  "0" : zone_ID)!
            
            txtMobileNumber.text = "\(dict_Login_Data.value(forKey: "MobileNo")!)"
            txtEmail.text = "\(dict_Login_Data.value(forKey: "EmailId")!)"
            txtOrganizationName.text = "\(dict_Login_Data.value(forKey: "InstitutionName")!)"
            let strGender = "\(dict_Login_Data.value(forKey: "Gender")!)"
            if(strGender == "Male"){
                btnMale.setImage(UIImage(named: "radio_1"), for: .normal)
                btnFemale.setImage(UIImage(named: "radio_2"), for: .normal)
            }else{
                btnMale.setImage(UIImage(named: "radio_2"), for: .normal)
                btnFemale.setImage(UIImage(named: "radio_1"), for: .normal)
            }
            if (strGender == ""){
                btnMale.setImage(UIImage(named: "radio_1"), for: .normal)
                btnFemale.setImage(UIImage(named: "radio_2"), for: .normal)
            }
            
            let urlImage = "http://aahar.org.in/ProfileImage/\(dict_Login_Data.value(forKey: "ProfileImage")!)"
            imgProfile.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "profile"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            imgProfile.layer.borderWidth = 1.0
            imgProfile.layer.masksToBounds = false
            imgProfile.layer.cornerRadius = imgProfile.frame.height/2
            imgProfile.clipsToBounds = true
            imgProfile.layer.borderColor = UIColor.white.cgColor
            imgProfile.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration:1.5,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 6.0,
                           options: .allowUserInteraction,
                           animations: { [weak self] in
                            self?.imgProfile.transform = .identity
                }, completion: nil)
            //  var strGender = "\(dict_Login_Data.value(forKey: "Gender")!)"
            
            self.strAddressProofName1 = "\(dict_Login_Data.value(forKey: "AddressProof1")!)"
            self.strAddressProofName2 = "\(dict_Login_Data.value(forKey: "AddressProof2")!)"
            
            
            if(self.strAddressProofName1.count != 0){
                let dict = NSMutableDictionary()
                dict.setValue("\(self.strAddressProofName1)", forKey: "image")
                dict.setValue("\(self.strAddressProofName1)", forKey: "name")
                aryCollection.add(dict)
            }
            
            if(self.strAddressProofName2.count != 0){
                let dict = NSMutableDictionary()
                dict.setValue("\(self.strAddressProofName2)", forKey: "image")
                dict.setValue("\(self.strAddressProofName2)", forKey: "name")
                aryCollection.add(dict)
            }
            
            if aryCollection.count != 0 {
                heightCollectionAddress.constant = 110.0
                self.addressProofMessage.isHidden = true
            }else{
                heightCollectionAddress.constant = 110.0
                self.addressProofMessage.isHidden = false
            }
            aryProfileList = NSMutableArray()
            aryProfileList = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
            if(aryProfileList.count == 0){
                let dict = NSMutableDictionary()
                dict.setValue(txtEmail.text!, forKey: "ContactPersonEmail")
                dict.setValue(strGender, forKey: "ContactPersonGender")
                dict.setValue(txtName.text!, forKey: "ContactPersonName")
                dict.setValue(txtMobileNumber.text!, forKey: "ContactPersonNo")
                dict.setValue("0", forKey: "DVOtherContactId")
                dict.setValue("\(dict_Login_Data.value(forKey: "DonorVId")!)", forKey: "DonorVId")
                dict.setValue("", forKey: "IsActive")
                dict.setValue(txtName.text!, forKey: "Name")
                self.aryDefaultProfile = NSMutableArray()
                self.aryDefaultProfile.add(dict)
            }else{
                var index = 0
                for item in aryProfileList {
                    let strName = (item as AnyObject).value(forKey: "ContactPersonName")as! String
                    let strNumber = (item as AnyObject).value(forKey: "ContactPersonNo")as! String
                    let strName1 = "\(dict_Login_Data.value(forKey: "Name")!)"
                    let strNumber1 = "\(dict_Login_Data.value(forKey: "MobileNo")!)"
                    if((strName.lowercased() == strName1.lowercased()) && (strNumber == strNumber1)){
                        aryProfileList.removeObject(at: index)
                        self.aryDefaultProfile = NSMutableArray()
                        self.aryDefaultProfile.add(item)
                    }
                    index = index + 1
                }
            }
            
            if(txtType.text!  == "Organization"){
                heightOrganizationName.constant = 55
                lblAddNewUser.isHidden = false
                viewAddContact.isHidden = false
            }else{
                heightOrganizationName.constant = -8
                lblAddNewUser.isHidden = true
                viewAddContact.isHidden = true
                self.aryProfileList = NSMutableArray()
                self.tvlist.reloadData()
            }
            tvlist.reloadData()
        }
        if(aryProfileList.count >= 5 ){
            viewAddContact.isHidden  = true
        }else{
            if(txtType.text!  != "Organization"){
                viewAddContact.isHidden  = true
            }
        }
    }
    //MARK:
    //MARK:-------------- Validation
    func validation() -> Bool {
        if(txtName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Name, viewcontrol: self)
            return false
        }else if (txtMobileNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
            return false
        }else if ((txtMobileNumber.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
            return false
        }else if (txtEmail.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Email, viewcontrol: self)
            return false
        }else if !(validateEmail(email: txtEmail.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
            return false
        }
        else if (txtType.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_OrganizationType, viewcontrol: self)
            return false
        }
        if(txtType.text! == "Organization"){
            if (txtOrganizationName.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_OrganizationName, viewcontrol: self)
                return false
            }
        }
        if(txtAddress.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Address, viewcontrol: self)
            return false
        }else if (txtState.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_State, viewcontrol: self)
            return false
        }else if (txtCity.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_City, viewcontrol: self)
            return false
        }
        return true
    }
    
    func validationEdit() -> Bool {
        if(txtEditName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Name, viewcontrol: self)
            return false
        }else if (txtEditMobileNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
            return false
        }else if ((txtEditMobileNumber.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
            return false
        }else if (txtEditEmail.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Email, viewcontrol: self)
            return false
        }else if !(validateEmail(email: txtEditEmail.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
            return false
        }
        return true
    }
    
    //MARK:---------------API CAlling
    //MARK:
    func callCountryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCountryList xmlns='http://aahar.org.in/'></GetCountryList></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetCountryListResult", responcetype: "GetCountryListResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryCountry = NSMutableArray()
                    self.aryCountry = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryCountry, strTitle: "")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func callstateAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetStateListByCountry xmlns='http://aahar.org.in/'><country_id>1</country_id></GetStateListByCountry></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetStateListByCountryResult", responcetype: "GetStateListByCountryResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    self.aryState = NSMutableArray()
                    self.aryState = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryState, strTitle: "")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func callCityAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCityListByState xmlns='http://aahar.org.in/'><state_id>\(txtState.tag)</state_id></GetCityListByState></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetCityListByStateResult", responcetype: "GetCityListByStateResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryCity = NSMutableArray()
                    self.aryCity = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryCity, strTitle: "")
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                    
                }
            }
        }
        
    }
    func callZoneAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetZoneMaster xmlns='http://aahar.org.in/'><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id></GetZoneMaster></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLUS, resultype: "GetZoneMasterResult", responcetype: "GetZoneMasterResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryZone = NSMutableArray()
                    self.aryZone = ((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Zone")as! NSArray).mutableCopy()as! NSMutableArray
                    if(self.aryZone.count != 0){
                        self.gotoPopViewWithArray(sender: sender, aryData: self.aryZone, strTitle: "")
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertZone, viewcontrol: self)
                    }
                    
                    
                    
                    
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
        
    }
    
    func callUpdateDataAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            var strGender = ""
            if(btnEditFemale.currentImage == UIImage(named: "radio_1")){
                strGender = (btnEditFemale.titleLabel?.text!)!
            }else{
                strGender = (btnEditMale.titleLabel?.text!)!
            }
            var json = Data()
            var jsonString = NSString()
            var aryTemp = NSMutableArray()
            aryTemp = aryProfileList
            
            if(txtType.text!  != "Organization"){
                let dict = NSMutableDictionary()
                dict.setValue(txtEmail.text!, forKey: "ContactPersonEmail")
                dict.setValue(strGender, forKey: "ContactPersonGender")
                dict.setValue(txtName.text!, forKey: "ContactPersonName")
                dict.setValue(txtMobileNumber.text!, forKey: "ContactPersonNo")
                dict.setValue("0", forKey: "DVOtherContactId")
                dict.setValue("\(dict_Login_Data.value(forKey: "DonorVId")!)", forKey: "DonorVId")
                dict.setValue("", forKey: "IsActive")
                dict.setValue(txtName.text!, forKey: "Name")
                self.aryDefaultProfile = NSMutableArray()
                self.aryDefaultProfile.add(dict)
                txtOrganizationName.text = ""
            }
            
            if(aryDefaultProfile.count != 0){
                aryTemp.add(aryDefaultProfile.object(at: 0))
            }
            
            
            let dictsend = NSMutableDictionary()
            dictsend.setValue("\(dict_Login_Data.value(forKey: "DonorVId")!)", forKey: "DonorVId")
            dictsend.setValue("\(txtName.text!)", forKey: "Name")
            dictsend.setValue("\(txtAddress.text!)", forKey: "Address")
            dictsend.setValue("\(txtMobileNumber.text!)", forKey: "MobileNo")
            dictsend.setValue("\(txtEmail.text!)", forKey: "EmailId")
            dictsend.setValue("\(dict_Login_Data.value(forKey: "Lat")!)", forKey: "Lat")
            dictsend.setValue("\(dict_Login_Data.value(forKey: "Long")!)", forKey: "Long")
            dictsend.setValue("\(txtState.tag)", forKey: "state_id")
            dictsend.setValue("\(txtCity.tag)", forKey: "city_id")
            dictsend.setValue("\(strProfileName)", forKey: "ProfileImage")
            
            dictsend.setValue("", forKey: "Type")
            
            dictsend.setValue("\(dict_Login_Data.value(forKey: "DonorVId")!)", forKey: "DPId")
            dictsend.setValue("", forKey: "DOB")
            dictsend.setValue("", forKey: "MobileNo1")
            dictsend.setValue("", forKey: "MobileNo2")
            dictsend.setValue("\(strGender)", forKey: "Gender")
            dictsend.setValue("\(dict_Login_Data.value(forKey: "CurruntStatus")!)", forKey: "CurruntStatus")
            dictsend.setValue("\(txtType.text!)", forKey: "UserType")
            dictsend.setValue("\(txtOrganizationName.text!)", forKey: "InstitutionName")
            dictsend.setValue("\(strAddressProofName1)", forKey: "AddressProof1")
            dictsend.setValue("\(strAddressProofName2)", forKey: "AddressProof2")
            dictsend.setValue("\(txtZone.tag)", forKey: "ZoneId")
            
            dictsend.setValue(aryTemp, forKey: "OtherContactArray")
            
            if JSONSerialization.isValidJSONObject(dictsend) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: dictsend, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            if(aryTemp.count == 0){
                jsonString = ""
            }
            
            
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Update_Profile xmlns='http://aahar.org.in/'><RegistrationMdl>\(jsonString)</RegistrationMdl></Update_Profile></soap12:Body></soap12:Envelope>"
            
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLUS, resultype: "Update_ProfileResult", responcetype: "Update_ProfileResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let aryTemp = dictTemp.value(forKey: "ProfileDetail")as! NSArray
                        let dictLoginData = aryTemp.object(at: 0)as! NSDictionary
                        nsud.setValue(dictLoginData, forKey: "Aahar_LoginData")
                        
                        nsud.synchronize()
                        if(nsud.value(forKey: "Aahar_LoginData") != nil){
                            dict_Login_Data = NSMutableDictionary()
                            dict_Login_Data = (nsud.value(forKey: "Aahar_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        }
                        FTIndicator.showNotification(withTitle: alertInfo, message:alert_ProfileUpdate )
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
        
    }
    func uploadProfileImage()  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: self.imgProfile.image!, fileName: strProfileName, withName: "userfile") { (responce, status) in
                remove_dotLoader(controller: self)
                if(status == "Suceess"){
                    if(self.aryAddressProff.count == 0){
                        self.callUpdateDataAPI(sender: UIButton())
                    }else{
                        let dict = self.aryAddressProff.lastObject as! NSDictionary
                        self.uploadAddressImage( image: (dict.value(forKey: "image") as! UIImage))
                    }
                }
            }
        }
    }
    
    func uploadAddressImage(image : UIImage)  {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            var strAddress = ""
            if aryAddressProff.count != 0{
                let dict = aryAddressProff.lastObject as! NSDictionary
                strAddress = "\(dict.value(forKey: "name")!)"
                dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
                
                WebService.callAPIWithImage(parameter: NSDictionary(), url: BaseURLUploadProfile, image: image, fileName: strAddress, withName: "userfile") { (responce, status) in
                    remove_dotLoader(controller: self)
                    if(status == "Suceess"){
                        self.aryAddressProff.removeLastObject()
                        if(self.aryAddressProff.count != 0){
                            let dict = self.aryAddressProff.lastObject as! NSDictionary
                            self.uploadAddressImage(image: (dict.value(forKey: "image") as! UIImage))
                        }else{
                            self.callUpdateDataAPI(sender: UIButton())
                        }
                    }
                }
            }
        }
        
    }
    //MARK:------------ Extra Method
    //MARK:
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = sender.tag
        
        if aryData.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleEditProfileScreen = self
            vc.aryTBL = aryData
            self.present(vc, animated: false, completion: {})
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension EditProfileVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryProfileList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "EditProfileCell", for: indexPath as IndexPath) as! EditProfileCell
        let dict = aryProfileList.object(at: indexPath.row)as! NSDictionary
        cell.txtName.text = "\(dict.value(forKey: "ContactPersonName")!)"
        cell.txtMobile.text = "\(dict.value(forKey: "ContactPersonNo")!)"
        cell.txtEmail.text = "\(dict.value(forKey: "ContactPersonEmail")!)"
        let strGender = "\(dict.value(forKey: "ContactPersonGender")!)"
        if(strGender == "Male"){
            cell.btnMale.setImage(UIImage(named: "radio_1"), for: .normal)
            cell.btnFemale.setImage(UIImage(named: "radio_2"), for: .normal)
        }else{
            cell.btnMale.setImage(UIImage(named: "radio_2"), for: .normal)
            cell.btnFemale.setImage(UIImage(named: "radio_1"), for: .normal)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 222.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dictNewContact = NSMutableDictionary()
        dictNewContact = (aryProfileList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        viewAddNewContact.frame = CGRect(x: 0, y: self.viewHeader.frame.maxY, width: self.view.frame.width, height: self.view.frame.height -  self.viewHeader.frame.maxY)
        txtEditName.text = "\(dictNewContact.value(forKey: "ContactPersonName")!)"
        txtEditMobileNumber.text = "\(dictNewContact.value(forKey: "ContactPersonNo")!)"
        txtEditEmail.text = "\(dictNewContact.value(forKey: "ContactPersonEmail")!)"
        let strGender = "\(dictNewContact.value(forKey: "ContactPersonGender")!)"
        if(strGender == "Male"){
            btnEditMale.setImage(UIImage(named: "radio_1"), for: .normal)
            btnEditFemale.setImage(UIImage(named: "radio_2"), for: .normal)
        }else{
            btnEditMale.setImage(UIImage(named: "radio_2"), for: .normal)
            btnEditFemale.setImage(UIImage(named: "radio_1"), for: .normal)
        }
        if (strGender == ""){
            btnEditMale.setImage(UIImage(named: "radio_1"), for: .normal)
            btnEditFemale.setImage(UIImage(named: "radio_2"), for: .normal)
        }
        updateTag = indexPath.row
        self.view.addSubview(viewAddNewContact)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        cell.alpha = 0.4
        //        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        //        UIView.animate(withDuration: 1.0) {
        //            cell.alpha = 1
        //            cell.transform = .identity
        //        }
    }
}
// MARK: - ----------------EditProfileCell
// MARK: -
class EditProfileCell: UITableViewCell {
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

//MARK: ---------- UIImagePickerControllerDelegate
//MARK:
extension EditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if(picker.view.tag == 1){
            imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            strProfileName = "\(getUniqueString())"
        }else  if(picker.view.tag == 2){
            let dict = NSMutableDictionary()
            dict.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
            dict.setValue("\(getUniqueString())", forKey: "name")
            aryAddressProff.add(dict)
            aryCollection.add(dict)
            
            if aryAddressProff.count != 0 {
                heightCollectionAddress.constant = 110.0
                self.addressProofMessage.isHidden = true
                
                
            }else{
                heightCollectionAddress.constant = 110.0
                self.addressProofMessage.isHidden = false
                
            }
            self.collectionView.reloadData()
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerController cancel")
        picker.dismiss(animated: true, completion: nil)
        if aryAddressProff.count != 0 {
            heightCollectionAddress.constant = 110.0
            self.addressProofMessage.isHidden = true
            
        }else{
            heightCollectionAddress.constant = 110.0
            self.addressProofMessage.isHidden = false
            
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension EditProfileVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  txtName ||  textField == txtEditName || textField ==  txtEmail ||  textField == txtEditEmail ||  textField == txtAddress ||  textField == txtOrganizationName {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
        if textField ==  txtMobileNumber ||  textField == txtEditMobileNumber  {
            return (txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
// MARK: - --------------UICollectionView
// MARK:
extension EditProfileVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdrressProofCell", for: indexPath as IndexPath) as! AdrressProofCell
        let dict = aryCollection[indexPath.row]as! NSDictionary
        
        if((dict.value(forKey: "image") is UIImage)){
            cell.AdrressProofImage.image = (dict.value(forKey: "image") as! UIImage)
            
        }else{
            let urlImage = "http://aahar.org.in/ProfileImage/\(dict.value(forKey: "image")!)"
            cell.AdrressProofImage.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
        }
        
        if(indexPath.row == 0){
            self.strAddressProofName1 = (dict.value(forKey: "name") as! String)
            
        }else if (indexPath.row == 1){
            self.strAddressProofName2 = (dict.value(forKey: "name") as! String)
        }
        
        cell.AdrressProofImage.layer.cornerRadius = 4.0
        cell.AdrressProofImage.layer.borderColor = UIColor.lightGray.cgColor
        cell.AdrressProofImage.layer.borderWidth = 1.0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90 , height:90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alertController = UIAlertController(title: alertMessage, message: alertRemoveImage, preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "Remove", style: .default) {
            UIAlertAction in
            let dict = self.aryCollection[indexPath.row]as! NSDictionary

            self.aryCollection.removeObject(at: indexPath.row)
            
            if(self.aryAddressProff.contains(dict)){
                self.aryAddressProff.remove(dict)
            }
            
            
            
            if(indexPath.row == 0){
                self.strAddressProofName1 = ""
                
            }else if (indexPath.row == 1){
                self.strAddressProofName2 = ""
            }
            
            if self.aryCollection.count != 0 {
                self.heightCollectionAddress.constant = 110.0
                
                self.addressProofMessage.isHidden = true
                
            }else{
                self.strAddressProofName1 = ""
                self.strAddressProofName2 = ""
                self.heightCollectionAddress.constant = 110.0
                self.addressProofMessage.isHidden = false
            }
            collectionView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
          
class AdrressProofCell: UICollectionViewCell {
    //Welcome Screen
    @IBOutlet weak var AdrressProofImage: UIImageView!
    
}
