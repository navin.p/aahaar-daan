//
//  DonateSurplusFoodVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/1/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol DonateSurPluseScreenDelegate : class{
    func refreshDonateSurPluseScreen(dictData : NSDictionary ,tag : Int)
}
//MARK:-
//MARK:- ---------DonateFreshfoodScreenDelegate Methods----------

extension DonateSurplusFoodVC: DonateSurPluseScreenDelegate {
    func refreshDonateSurPluseScreen(dictData: NSDictionary, tag: Int) {
        self.view.endEditing(true)
        
        if(tag == 10){
            txtContactPerson.text = (dictData.value(forKey: "ContactPersonName")as! String) + " (\(dictData.value(forKey: "ContactPersonNo")!))"
            txtContactPerson.tag = Int("\(dictData.value(forKey: "DVOtherContactId")!)")!
        }else if(tag == 0){ // For Adddress
            self.txtAddress.text =  (dictData.value(forKey: "address")as! String)
            lat =  (dictData.value(forKey: "lat")as! String)
            long =  (dictData.value(forKey: "long")as! String)
          //  lat = "29.636417915579706"
          //  long = "-98.51503525582032"
//            lat = "37.132840000000016"
//              long = "-95.78557999999998"
            //      29.636417915579706 -98.51503525582032  nitesh
            //   37.132840000000016 -95.78557999999998  rishabh
        }else{
            txtItemName.text = (dictData.value(forKey: "FoodName")as! String)
            txtItemName.tag = Int("\(dictData.value(forKey: "FoodId")!)")!
            if (txtItemName.text == "Others"){
                heightOther.constant = 35.0
            }else{
                heightOther.constant = 0.0
            }
        }

    }
}
class DonateSurplusFoodVC: UIViewController {
    
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDonate: UIButton!
    @IBOutlet weak var txtShelflife: UITextField!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var txtItemName: UITextField!
    @IBOutlet weak var txtOther: UITextField!
    @IBOutlet weak var heightOther: NSLayoutConstraint!
    @IBOutlet weak var heightTV: NSLayoutConstraint!
    @IBOutlet weak var viewContain: UIView!

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var txtStartTime: ACFloatingTextfield!
    @IBOutlet weak var txtContactPerson: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: KMPlaceholderTextView!
    @IBOutlet weak var txtEndetime: ACFloatingTextfield!
    
    @IBOutlet var viewPicker: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnADDFood: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryFoodList = NSMutableArray()
    var arySelectedFoodList = NSMutableArray()
    var strTitle = String()
    var lat = ""
    var long = ""
    var editIndex = 9999

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnDonate.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        lblTitle.text = strTitle
    if(DeviceType.IS_IPHONE_X){
                    heightHeader.constant = 94
                }else{
                    heightHeader.constant = 74
                }
        callFoodListAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(arySelectedFoodList.count == 0){
            heightTV.constant = 0.0
        }else{
            var heightCount = 0
            
            for item  in arySelectedFoodList {
                if ("\((item as AnyObject).value(forKey: "OtherFoodDetail")!)" == "") {
                    heightCount = heightCount + 50
                }else{
                    heightCount = heightCount + 75
                }
            }
            heightTV.constant = CGFloat(heightCount + 50)
        }
        viewContain.frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.width), height: Int (heightTV.constant) + 500)

        scroll.contentSize = CGSize(width: Int(self.view.frame.width), height: Int (heightTV.constant) + 500)
        viewContain.removeFromSuperview()
        scroll.addSubview(viewContain)
      
       
        
    }
    
    override func viewWillLayoutSubviews() {
   
    }
    
    
    //Mark: - --------GUID
        func showGuides() {
            // Reset to show everytime.
            KSGuideDataManager.reset(for: "MainGuide")
            var items = [KSGuideItem]()

            let item = KSGuideItem(sourceView: btnADDFood, arrowImage: #imageLiteral(resourceName: "one-finger-click"), text: String("ADD-\n\n\(alertGUIDE_ADDFood)"))
           
            items.append(item)
        
            

            let vc = KSGuideController(items: items, key: "MainGuide")
            vc.setIndexWillChangeBlock { (index, item) in
                print("Index will change to \(index)")
            }
            vc.setIndexDidChangeBlock { (index, item) in
                print("Index did change to \(index)")
            }
            vc.show(from: self) {
                print("Guide controller has been dismissed")
                nsud.set(true, forKey: "Aahar_Guide_DonateSurpluseFood")
                
            }
        }
        
        
    
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononSelectFood(_ sender: UIButton) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = 9
        if aryFoodList.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleDonateSurPluseScreen = self
            vc.aryTBL = aryFoodList
            self.present(vc, animated: false, completion: {})
        }else{
            FTIndicator.showToastMessage("Food item list not found!")
        }
    }
    @IBAction func actionOnAddFood(_ sender: UIButton) {
        if(validation()){
            let dict = NSMutableDictionary()
            dict.setValue(txtItemName.text!, forKey: "Food")
            dict.setValue(txtShelflife.text!, forKey: "ShelfLife")
            dict.setValue(txtQty.text!, forKey: "NoOfPerson")
            dict.setValue("\(txtItemName.tag)", forKey: "FoodId")
            dict.setValue(txtOther.text!, forKey: "OtherFoodDetail")
            
            if(self.editIndex == 9999){
                arySelectedFoodList.add(dict)

            }else{
                arySelectedFoodList.replaceObject(at: self.editIndex, with: dict)
            }
            self.editIndex = 9999
            self.tvlist.reloadData()
            txtOther.text = ""
            txtQty.text = ""
            txtShelflife.text = ""
            txtItemName.text = ""
            txtItemName.tag = 0
             viewWillAppear(true)
        }
        self.view.endEditing(true)
    }
    @IBAction func actionOnDonateFood(_ sender: UIButton) {
        if(validationOnSubmit()){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let strDueDate = dateFormatter.date(from:txtEndetime.text!)!
            let strStartDate = dateFormatter.date(from:txtStartTime.text!)!
            if(strStartDate >= strDueDate){
                txtEndetime.text = ""
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "End time should be greater then start time!", viewcontrol: self)
            }else{
                let alert = UIAlertController(title: "Confirmation", message: "Are You Sure to Proceed with the Entered details ? ", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.callFoodListDonateAPI(sender: sender)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { action in
                    print("Yay! You brought your towel!")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func actionONDone(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        let somedateString = dateFormatter.string(from: picker.date)
        if(picker.tag == 1){
            txtStartTime.text = somedateString
        }else{
               txtEndetime.text = somedateString
        }
        if(txtStartTime.text != "" && txtEndetime.text != ""){
            let strDueDate = dateFormatter.date(from:txtEndetime.text!)!
            let strStartDate = dateFormatter.date(from:txtStartTime.text!)!
            if(strStartDate >= strDueDate){
                txtEndetime.text = ""
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "End time should be greater then start time!", viewcontrol: self)
            }
        }
        
        self.viewPicker.removeFromSuperview()
    }
    @IBAction func actionOnSelectContact(_ sender: UIButton) {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.strTitle = strTitle
        vc.strTag = 10
        let aryData = (dict_Login_Data.value(forKey: "OtherContactArray")as! NSArray).mutableCopy()as! NSMutableArray
        
        let resultPredicate = NSPredicate(format: "IsActive contains[c] %@", argumentArray: ["True"])
           let arrayfilter = (aryData ).filtered(using: resultPredicate)
        let nsMutableArray = NSMutableArray(array: arrayfilter)
        if nsMutableArray.count != 0{
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleDonateSurPluseScreen = self

            vc.aryTBL = nsMutableArray.mutableCopy() as! NSMutableArray
            self.present(vc, animated: false, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ContactList, viewcontrol: self)
        }
    }
    @IBAction func actionOnStartTime(_ sender: UIButton) {
        self.viewPicker.frame = self.view.frame
        picker.minimumDate = Date()
       // picker.minuteInterval = 10
        picker.tag = 1
        picker.datePickerMode = .time
        self.view.addSubview(self.viewPicker)
    }
    @IBAction func actionOnEndTime(_ sender: UIButton) {
        self.viewPicker.frame = self.view.frame
        picker.minimumDate = Date()
       // picker.minuteInterval = 10
        picker.tag = 2
        picker.datePickerMode = .time
        self.view.addSubview(self.viewPicker)
    }
    @IBAction func actionOnAddress(_ sender: UIButton) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
//           vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            vc.modalTransitionStyle = .coverVertical
           vc.handleDonateSurPluseScreen_1 = self
        
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
    }
    //MARK:
    //MARK:-------------- Validation
    func validation() -> Bool {
        
        if(txtItemName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertFood, viewcontrol: self)
            return false
        }else if (txtShelflife.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertshelflife, viewcontrol: self)
            return false
        }else if (txtQty.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertNumberOfPerson, viewcontrol: self)
            return false
        }else if (Int(txtQty.text!)! < 20){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertLimitPerson, viewcontrol: self)
            return false
        }
        if (txtItemName.text == "Others"){
            if (txtOther.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertOther, viewcontrol: self)
                return false
            }
        }
        return true
    }
    
    func validationOnSubmit() -> Bool {

        if(arySelectedFoodList.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertFoodItem, viewcontrol: self)
            return false
        }else if (txtStartTime.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStartTime, viewcontrol: self)
            return false
        }else if (txtEndetime.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertEndTime, viewcontrol: self)
            return false
        }else if (txtContactPerson.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertProductContactPerson, viewcontrol: self)
            return false
        }
        else if (txtAddress.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertAddressPickup, viewcontrol: self)
            return false
        }
        return true
    }
    // MARK: - ---------------API CAlling
    // MARK: -

    
    func callFoodListDonateAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(arySelectedFoodList) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: arySelectedFoodList, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            if(arySelectedFoodList.count == 0){
                jsonString = ""
            }
            var description =  ""
            for item in arySelectedFoodList  {
                let dict = (item as AnyObject)as! NSDictionary
                let name = "\(dict.value(forKey: "Food")!)(\(dict.value(forKey: "NoOfPerson")!))"
                description = "\(description)\(name),"
            }
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddDonateFood xmlns='http://aahar.org.in/'><FoodDonetId>0</FoodDonetId><FoodDescription>\(description.dropLast())</FoodDescription><FoodArray>\(jsonString)</FoodArray><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><FoodImage></FoodImage><Lat>\(lat)</Lat><Long>\(long)</Long><Address>\(txtAddress.text!)</Address><FoodCollectStartTime>\(txtStartTime.text!)</FoodCollectStartTime><FoodCollectEndTime>\(txtEndetime.text!)</FoodCollectEndTime><DVOtherContactId>\(txtContactPerson.tag)</DVOtherContactId><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id></AddDonateFood></soap12:Body></soap12:Envelope>"
                print(soapMessage)
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddDonateFoodResult", responcetype: "AddDonateFoodResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let alert = UIAlertController(title: alertInfo, message: alert_DonationFoodBook, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
 
    
    func callFoodListAPI() {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            
            
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodItemListCountryWise xmlns='http://aahar.org.in/'><country_id>\(dict_Login_Data.value(forKey: "country_id")!)</country_id></GetFoodItemListCountryWise></soap12:Body></soap12:Envelope>"
            
          //  let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFoodItemListCountryWise xmlns='http://aahar.org.in/'><country_id>2</country_id></GetFoodItemListCountryWise></soap12:Body></soap12:Envelope>"
            
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetFoodItemListCountryWiseResult", responcetype: "GetFoodItemListCountryWiseResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    if(nsud.value(forKey: "Aahar_Guide_DonateSurpluseFood") !=  nil){
                               if !(nsud.value(forKey: "Aahar_Guide_DonateSurpluseFood") as! Bool){
                                self.showGuides()
                               }else{
                               // self.showGuides()
                               }
                           }else{
                                 self.showGuides()
                           }
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "FoodNameList")as! NSArray
                        self.aryFoodList = NSMutableArray()
                        self.aryFoodList = aryTemp.mutableCopy()as! NSMutableArray
                    }else{
                    }
                }else{
                }
            }
        }
    }
   
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DonateSurplusFoodVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arySelectedFoodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "FoodItemCell", for: indexPath as IndexPath) as! FoodItemCell
        let dict = arySelectedFoodList.object(at: indexPath.row)as! NSDictionary
         cell.lblFoodName.text = "\(dict.value(forKey: "Food")!)"
        cell.lblFoodName.tag =  Int("\(dict.value(forKey: "FoodId")!)")!
         cell.lblShelfLife.text = "\(dict.value(forKey: "ShelfLife")!)"
         cell.lblQty.text = "\(dict.value(forKey: "NoOfPerson")!)"
         cell.lblOther.text = "\(dict.value(forKey: "OtherFoodDetail")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dict = arySelectedFoodList.object(at: indexPath.row)as! NSDictionary
        if ("\(dict.value(forKey: "OtherFoodDetail")!)" == "") {
            return 50.0
        }else{
            return 75.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
  
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // action one
        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
            print("Edit tapped")
            let dict = self.arySelectedFoodList.object(at: indexPath.row)as! NSDictionary
            self.txtItemName.text = "\(dict.value(forKey: "Food")!)"
            self.txtShelflife.text = "\(dict.value(forKey: "ShelfLife")!)"
            self.txtQty.text = "\(dict.value(forKey: "NoOfPerson")!)"
            self.txtOther.text = "\(dict.value(forKey: "OtherFoodDetail")!)"
            if (self.txtItemName.text == "Others"){
                self.heightOther.constant = 35.0
            }else{
                self.heightOther.constant = 0.0
            }
            self.editIndex = indexPath.row
        })
        editAction.backgroundColor = UIColor.gray
          // action two
          let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
               let alert = UIAlertController(title: alertMessage, message: alertDelete, preferredStyle: .alert)
                         alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                             print("Yay! You brought your towel!")
                         }))
                         alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { action in
                             self.arySelectedFoodList.removeObject(at: indexPath.row)
                             self.tvlist.reloadData()
                             self.viewWillAppear(true)
                         }))
                         self.present(alert, animated: true)
          })
          return [editAction, deleteAction]
    }
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class FoodItemCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblShelfLife: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblOther: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension DonateSurplusFoodVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField ==  txtShelflife {
            return (txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 2))
        }
        
        if textField ==  txtQty {
            return (txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 2))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
