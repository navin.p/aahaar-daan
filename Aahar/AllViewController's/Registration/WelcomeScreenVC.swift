//
//  WelcomeScreenVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class WelcomeScreenVC: UIViewController {
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet weak var collectionForWelcome: UICollectionView!
    
    //MARK:
    //MARK: CustomeVariable
    let  ary_CollectionData = [["title":"Welcome ","subtitle" :"Aahar Application uses technology in over 45 cities, to reduce wastage of food and work towards the issue of hunger.","image":"info_img_1"],["title":"What We Do","subtitle" :"We empower donors of surplus food with hassle free donations, through volunteers, to reach the needy.","image":"info_img_2"],["title":"How It Work","subtitle" :"Aahar brings together volunteers and donors interested in donating food. With a network of individuals and community organizations, it connects individuals interested in procuring and donating food at different establishments.","image":"info_img_3"]]
    
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnNext.layer.borderWidth = 1.0
        self.btnNext.layer.borderColor = UIColor.lightGray.cgColor
        self.btnNext.layer.cornerRadius = 18.0
        self.btnSkip.layer.borderWidth = 1.0
        self.btnSkip.layer.borderColor = UIColor.lightGray.cgColor
        self.btnSkip.layer.cornerRadius = 18.0
        self.btnDone.layer.borderWidth = 1.0
        self.btnDone.layer.borderColor = UIColor.lightGray.cgColor
        self.btnDone.layer.cornerRadius = 18.0
        self.btnDone.isHidden = true
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnSkip(_ sender: UIButton) {
        sender.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        sender.setTitleColor(UIColor.white, for: .normal)
        
        sender.alpha = 0.4
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.black, for: .normal)
            
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    @IBAction func actionOnDone(_ sender: UIButton) {
        sender.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.alpha = 0.4
        btnNext.isHidden = true
        btnSkip.isHidden = true
        sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.3, animations: {
            sender.alpha = 1
            sender.transform = .identity
        }) { (result) in
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.black, for: .normal)
            
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    @IBAction func actionOnNext(_ sender: UIButton) {
   
        sender.backgroundColor = UIColor.white
        sender.setTitleColor(UIColor.black, for: .normal)
        
        if self.pageIndicator.currentPage == 0 {
            self.btnDone.isHidden = true
            collectionForWelcome.scrollToNextItem()

            self.pageIndicator.currentPage = (1)
            
        }else if (self.pageIndicator.currentPage == 1 ){
            collectionForWelcome.scrollToNextItem()
            self.pageIndicator.currentPage = (2)
            self.btnDone.isHidden = false
            
            
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
}
// MARK: - --------------UICollectionView
// MARK:
extension WelcomeScreenVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCell", for: indexPath as IndexPath) as! welcomeCell
        let dict = ary_CollectionData[indexPath.row]as NSDictionary
        cell.welcome_lbl_Title.text = dict["title"]as? String
        cell.welcome_lbl_SubTitle.text = dict["subtitle"]as? String
        cell.welcome_Image.image = UIImage(named: dict["image"]as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width) , height:(self.view.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collectionForWelcome.contentOffset.x
        let w = self.collectionForWelcome.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pageIndicator.currentPage = (currentPage)
        if currentPage == 2 {
            self.btnDone.isHidden = false
        }else{
            self.btnDone.isHidden = true
        }
    }
}
class welcomeCell: UICollectionViewCell {
    //Welcome Screen
    @IBOutlet weak var welcome_Image: UIImageView!
    @IBOutlet weak var welcome_lbl_Title: UILabel!
    @IBOutlet weak var welcome_lbl_SubTitle: UILabel!
}
extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    func moveToFrame(contentOffset : CGFloat) {
            self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
        }
}
