//
//  OTPVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class OTPVC: UIViewController {
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var tf4: UITextField!

    
    @IBOutlet weak var lblShowCounter: UILabel!
    @IBOutlet weak var lblClckHere: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var btnresend: UIButton!
    
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var viewBtnLogin: CardView!

    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var dictData = NSMutableDictionary()
    var count = 30
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
   print(dictData)
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                   heightHeader.constant = 94
               }else{
                   heightHeader.constant = 74
               }

        tf1.layer.borderColor = UIColor.gray.cgColor
        tf1.layer.borderWidth = 1.0
        tf1.layer.cornerRadius = 20.0
        tf2.layer.borderColor = UIColor.gray.cgColor
        tf2.layer.borderWidth = 1.0
        tf2.layer.cornerRadius = 20.0

        tf3.layer.borderColor = UIColor.gray.cgColor
        tf3.layer.borderWidth = 1.0
        tf3.layer.cornerRadius = 20.0

        tf4.layer.borderColor = UIColor.gray.cgColor
        tf4.layer.borderWidth = 1.0
        tf4.layer.cornerRadius = 20.0

    
        
        lblShowCounter.text = "00" + ":" + "00"
        lblClckHere.isHidden = true
        btnresend.isHidden = true
        lblShowCounter.font = lblShowCounter.font.withSize(30)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
      
        
        if("\(dictData.value(forKey: "LoginType")!)" == "Registration"){
            
            lbltitle.text = "Enter the 4 digit OTP sent over your registerd mobile number & email."
        } else if("\(dictData.value(forKey: "LoginType")!)" == "LoginViaMobile"){
                   lbltitle.text = "Enter the 4 digit OTP sent over your registerd mobile number."
               }
        else if("\(dictData.value(forKey: "LoginType")!)" == "LoginViaEmail"){
            lbltitle.text = "Enter the 4 digit OTP sent over your registerd email."
        }

        
        
        var myMutableString1 = NSMutableAttributedString()
        let attrs1 = [NSAttributedString.Key.font : lbltitle.font, NSAttributedString.Key.foregroundColor : UIColor.gray]
        
        myMutableString1 = NSMutableAttributedString(string: lbltitle.text!, attributes:attrs1 as [NSAttributedString.Key : Any])
        myMutableString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 10, length: 11))
        lbltitle.attributedText = myMutableString1
        
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0 {
          callOTPVerifyAPI(sender: sender)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OTP, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnResendSMS(_ sender: UIButton) {
        
        if("\(dictData.value(forKey: "LoginType")!)" == "Registration"){
            self.callResendOTPAPI(sender: sender)
        } else if("\(dictData.value(forKey: "LoginType")!)" == "LoginViaMobile"){
          callReSendOtPWithMobileAPI(sender: sender)
        }
        else if("\(dictData.value(forKey: "LoginType")!)" == "LoginViaEmail"){
             callReSendOtPWithEmailAPI(sender: sender)
        }
    }
    
    @objc func updateCounterValue() {
        if(count > 0){
            count = count - 1
            var minutes = String(count / 60)
            var seconds = String(count % 60)
            if count % 60 < 10{
                seconds = "0" + seconds
            }
            if count / 60 < 10{
                minutes = "0" + minutes
            }
            lblShowCounter.text = minutes + ":" + seconds
            lblClckHere.isHidden = true
            btnresend.isHidden = true
            lblShowCounter.font = lblShowCounter.font.withSize(30)
            
        }else{
            lblShowCounter.text = "If you did not received OTP then"
            lblClckHere.isHidden = false
            btnresend.isHidden = false
            lblShowCounter.font = lblShowCounter.font.withSize(15)
            timer.invalidate()
            var myMutableString2 = NSMutableAttributedString()
            let attrs2 = [NSAttributedString.Key.font : lblShowCounter.font, NSAttributedString.Key.foregroundColor : UIColor.gray]
            myMutableString2 = NSMutableAttributedString(string: lblShowCounter.text!, attributes:attrs2 as [NSAttributedString.Key : Any])
            myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 24, length: 3))
            lblShowCounter.attributedText = myMutableString2
            
        }
        
    }
    //MARK:*********
    //MARK: API Calling
    func callOTPVerifyAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            //\(dictData.value(forKey: "MobileNo")!)
            let strOTP = tf1.text! + tf2.text! +  tf3.text! +  tf4.text!
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Check_OTP xmlns='http://aahar.org.in/'><MobileNo>\(dictData.value(forKey: "MobileNo")!)</MobileNo><OTP>\(strOTP)</OTP></Check_OTP></soap12:Body></soap12:Envelope>"
            dotLoader(sender: sender , controller : self, viewContain: self.viewBtnLogin, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Check_OTPResult", responcetype: "Check_OTPResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let aryTemp = dictTemp.value(forKey: "ProfileDetail")as! NSArray
                        let dictLoginData = aryTemp.object(at: 0)as! NSDictionary
                        nsud.setValue(dictLoginData, forKey: "Aahar_LoginData")
                        nsud.setValue("True", forKey: "Aahar_LoginStatus")
                        self.timer.invalidate()
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC")as! DynemicSplashVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "OTP invalid!", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func callResendOTPAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Generate_OTPWithCity xmlns='http://aahar.org.in/'><Name>\(dictData.value(forKey: "Name")!)</Name><MobileNo>\(dictData.value(forKey: "MobileNo")!)</MobileNo><state_id>\(dictData.value(forKey: "state_id")!)</state_id><city_id>\(dictData.value(forKey: "city_id")!)</city_id></Generate_OTPWithCity></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Generate_OTPWithCityResult", responcetype: "Generate_OTPWithCityResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.count = 30
                        self.lblShowCounter.text = "00" + ":" + "00"
                             self.lblClckHere.isHidden = true
                            self.btnresend.isHidden = true
                            self.lblShowCounter.font =   self.lblShowCounter.font.withSize(30)
                             self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
        
    }
    
    func callReSendOtPWithEmailAPI(sender : UIButton) {
               if !isInternetAvailable() {
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                   
               }else{
                let strEmail = "\(dictData.value(forKey: "Email")!)"
                   let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UserLoginOTP xmlns='http://aahar.org.in/'>     <MobileNo></MobileNo><country_id></country_id><EmailId>\(strEmail)</EmailId><Password></Password></UserLoginOTP></soap12:Body></soap12:Envelope>"
                
                
                   dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
                   WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "UserLoginOTPResult", responcetype: "UserLoginOTPResponse") { (responce, status) in
                       remove_dotLoader(controller: self)
                       if status == "Suceess"{
                           let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                           if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                               self.count = 30
                               self.lblShowCounter.text = "00" + ":" + "00"
                                    self.lblClckHere.isHidden = true
                                   self.btnresend.isHidden = true
                                   self.lblShowCounter.font =   self.lblShowCounter.font.withSize(30)
                                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
                           }else{
                               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                           }
                       }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                       }
                   }
               }
           }
    
       func callReSendOtPWithMobileAPI(sender : UIButton) {
           if !isInternetAvailable() {
               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
               
           }else{
               
            
               let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UserLoginOTP xmlns='http://aahar.org.in/'>     <MobileNo>\(dictData.value(forKey: "MobileNo")!)</MobileNo><country_id>\(dictData.value(forKey: "CountryCode")!)</country_id><EmailId></EmailId><Password></Password></UserLoginOTP></soap12:Body></soap12:Envelope>"
             
            
            
               dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")

               WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "UserLoginOTPResult", responcetype: "UserLoginOTPResponse") { (responce, status) in
                   remove_dotLoader(controller: self)
                   if status == "Suceess"{
                       let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                           self.count = 30
                           self.lblShowCounter.text = "00" + ":" + "00"
                                self.lblClckHere.isHidden = true
                               self.btnresend.isHidden = true
                               self.lblShowCounter.font =   self.lblShowCounter.font.withSize(30)
                                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounterValue), userInfo: nil, repeats: true)
                       }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                       }
                   }else{
                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                   }
               }
           }
       }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -
extension OTPVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
        }
        if textField == tf1 {
            tf1.text = string
            if tf1.text?.count == 1 {
                tf2.becomeFirstResponder()
            }
        }
        if textField == tf2 {
            tf2.text = string
            if tf2.text?.count == 1 {
                tf3.becomeFirstResponder()
            }
        }
        
        if textField == tf3 {
            tf3.text = string
            if tf3.text?.count == 1 {
                tf4.becomeFirstResponder()
            }
        }
        if textField == tf4 {
            tf4.text = string
            if tf4.text?.count == 1 {
                self.view.endEditing(true)
                if tf1.text?.count != 0 && tf2.text?.count != 0 && tf3.text?.count != 0 && tf4.text?.count != 0  {
                    // self.call_OTPVerify_API(sender: btnSubmit)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: Alert_OTP, viewcontrol: self)
                }
            }
        }
     
    
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 0)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
}
