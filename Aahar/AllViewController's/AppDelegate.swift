//
//  AppDelegate.swift
//  Aahar
//
//  Created by Navin Patidar on 2/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var dictNotificationData = NSMutableDictionary()
    var statusActive = Bool()
    //internal let kMapsAPIKey = "AIzaSyDa4zTrypcXutzc1CXtf15wgIKoPy40sJY"
    let center = UNUserNotificationCenter.current()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FTIndicator.setIndicatorStyle(UIBlurEffect.Style.dark)
    
        if #available(iOS 13.0, *) {
           window?.overrideUserInterfaceStyle = .light
       }

        if DeviceType.IS_IPAD {
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }else{
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }
        
        //1 FireBase
        FirebaseApp.configure()
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        center.delegate = self
        //UINavigationBar.appearance().barStyle = .black
        gotoViewController()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func gotoViewController()  {
        
        if (nsud.value(forKey: "Aahar_LoginStatus") != nil) {
            if (nsud.value(forKey: "Aahar_LoginStatus")as! String == "True") {
                let obj_ISVC : DynemicSplashVC = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC") as! DynemicSplashVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)

            }else{
                let obj_ISVC : LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
            }
        }else{
            let obj_ISVC : WelcomeScreenVC = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            rootViewController(nav: navigationController)
        }
    }

    func rootViewController(nav : UINavigationController)  {
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Aahar")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}


// MARK: --------UNNotification/Messaging Delegate Method----------

extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate{
    //MARK: RegisterPushNotification
    func registerForRemoteNotifications()  {
        if #available(iOS 10, *) {
            
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .authorized, .provisional:
                    print("authorized")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        Messaging.messaging().delegate = self
                    }
                    
                case .denied:break
                case .notDetermined:break
                @unknown default:
                    break
                }
            })
            
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let state: UIApplication.State = UIApplication.shared.applicationState
        //let strPushType = "\((userInfo as AnyObject).value(forKey: "PushType")!)"
        if state == .active {
            let dict = (userInfo as AnyObject)as! NSDictionary
            let aps = (dict.value(forKey: "aps")as! NSDictionary)
            let alert = (aps.value(forKey: "alert")as! NSDictionary)
            let strTitle = (alert.value(forKey: "title")!)
            let strbody = (alert.value(forKey: "body")!)
            FTIndicator.showNotification(with:  UIImage(named: "icon152"), title: strTitle as? String, message: strbody as? String) {
                self.hendelNotidata(userInfo: userInfo as NSDictionary, status: true)
            }
        }
        else  {
            hendelNotidata(userInfo: userInfo as NSDictionary, status: false)
        }
    }
    
    
    func hendelNotidata(userInfo :NSDictionary , status : Bool){
        let obj_ISVC = mainStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
        dictNotificationData = NSMutableDictionary()
        statusActive = status
        dictNotificationData = userInfo.mutableCopy()as! NSMutableDictionary
        let navigationController = UINavigationController(rootViewController: obj_ISVC)
        rootViewController(nav: navigationController)
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "Aahar_FCM_Token")
            nsud.synchronize()
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge,.alert,.sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
}
