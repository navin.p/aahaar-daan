//
//  ChangePasswordVC.swift
//  Aahar
//
//  Created by Navin Patidar on 1/13/20.
//  Copyright © 2020 Saavan_patidar. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    //MARK:-
       //MARK:- ---------IBOutlet
       
       
       @IBOutlet weak var txtOldPassword: ACFloatingTextfield!
       @IBOutlet weak var txtNewPassword: ACFloatingTextfield!
       @IBOutlet weak var txtConfirmPassword: ACFloatingTextfield!
       @IBOutlet weak var viewHeader: CardView!
       @IBOutlet weak var btnChangePassword: UIButton!
       @IBOutlet weak var heightHeader: NSLayoutConstraint!
    //MARK:-
      //MARK:- ---------LifeCycle
      
    override func viewDidLoad() {
        super.viewDidLoad()
           btnChangePassword.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
             viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
             btnChangePassword.layer.cornerRadius = 8.0
        if(DeviceType.IS_IPHONE_X){
                        heightHeader.constant = 94
                    }else{
                        heightHeader.constant = 74
                    }
    }
    
   // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnChangePassword(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            callChangePasswordAPI(sender: sender)
        }
    }
    //MARK:-
       //MARK:- ---------API Calling
    func callChangePasswordAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{

            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ChangePassword xmlns='http://aahar.org.in/'><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><OldPassword>\(txtOldPassword.text!)</OldPassword><NewPassword>\(txtNewPassword.text!)</NewPassword></ChangePassword></soap12:Body></soap12:Envelope>"
            

            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLUS, resultype: "ChangePasswordResult", responcetype: "ChangePasswordResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                           
                           alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            
                            self.navigationController?.popViewController(animated: true)
                           }))
                           self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    // MARK: - --------------validation
           // MARK: -
           func validation() -> Bool {
            if (txtOldPassword.text?.count == 0){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_OldPassword, viewcontrol: self)
                   return false
            }else  if (txtNewPassword.text?.count == 0){
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_NewPassword, viewcontrol: self)
                              return false
                       }
            else  if (txtConfirmPassword.text?.count == 0){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CNewPassword, viewcontrol: self)
                   return false
            }   else  if (txtNewPassword.text! != txtConfirmPassword.text!){
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CheckNewPassword, viewcontrol: self)
                            return false
                     }
               return true
           }
}
