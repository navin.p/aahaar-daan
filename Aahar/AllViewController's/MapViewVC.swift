//
//  MapViewVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/28/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var strTitle = String()
    var dictData = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                                 heightHeader.constant = 94
                             }else{
                                 heightHeader.constant = 74
                             }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        lblTitle.text = strTitle
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
     
        myAnnotation.coordinate = CLLocationCoordinate2DMake(Double("\(dictData.value(forKey: "Organization_Latitude")!)")!, Double("\(dictData.value(forKey: "Organization_Longitude")!)")!)
        myAnnotation.title = "\(dictData.value(forKey: "Organization_Name")!)"
        mapView.showsUserLocation = true
        mapView.addAnnotation(myAnnotation)
        let center = CLLocationCoordinate2DMake(0.0, 0.0)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 0))
        mapView.setRegion(region, animated: true)
        UIView.animate(withDuration: 2.0, animations: {
            let center = CLLocationCoordinate2DMake(Double("\(self.dictData.value(forKey: "Organization_Latitude")!)")!, Double("\(self.dictData.value(forKey: "Organization_Longitude")!)")!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.mapView.setRegion(region, animated: true)
        })
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -


extension MapViewVC: MKMapViewDelegate  {
    
}
