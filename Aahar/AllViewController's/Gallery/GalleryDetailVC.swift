//
//  GalleryDetailVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class GalleryDetailVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var collectionForGallery: UICollectionView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

     var aryGallery = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        lblNumber.text = "\(0 + 1)/\(aryGallery.count)"
        if(DeviceType.IS_IPHONE_X){
                   heightHeader.constant = 94
               }else{
                   heightHeader.constant = 74
               }
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - --------------UICollectionView
// MARK:
extension GalleryDetailVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryGallery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath as IndexPath) as! galleryCell
        let dict = aryGallery.object(at: indexPath.row)as! NSDictionary
        let urlImage = "http://aahar.org.in/GalleryImages/\(dict.value(forKey: "MediaPath")!)"
        cell.gallery_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width) , height:(self.view.frame.size.height))
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = self.collectionForGallery.contentOffset.x
        let w = self.collectionForGallery.bounds.size.width
        let currentPage = Int(ceil(x/w))
        lblNumber.text = "\(currentPage + 1)/\(aryGallery.count)"
      
    }
}
class galleryCell: UICollectionViewCell {
    //Welcome Screen
    @IBOutlet weak var gallery_Image: UIImageView!
 
}
