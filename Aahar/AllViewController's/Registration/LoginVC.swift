//
//  LoginVC.swift
//  Aahar
//
//  Created by Navin Patidar on 11/26/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
  
    //MARK:-
      //MARK:- ---------IBOutlet
      
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtCountryCode: ACFloatingTextfield!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegistration: UIButton!
    @IBOutlet weak var viewContain: UIView!

    var aryCountry = NSMutableArray()
    
    //MARK:-
      //MARK:- ---------LifeCycle
      
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.tableFooterView = UIView()
        btnRegistration.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnLogin.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        txtCountryCode.isUserInteractionEnabled = false
        txtCountryCode.text = "+91"
        txtCountryCode.tag = 1
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    @objc func textFieldDidChange(_ textField: UITextField) {

                let text = textField.text ?? ""

                let trimmedText = text.trimmingCharacters(in: .whitespaces)

                textField.text = trimmedText
           if(txtMobile == textField){
             textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
         }
    }
    // MARK: - --------------IBAction
       // MARK: -
       @IBAction func actionOnRegistration(_ sender: Any) {
        self.view.endEditing(true)

        let vc: RegistrationNewVC = self.storyboard!.instantiateViewController(withIdentifier: "RegistrationNewVC") as! RegistrationNewVC
               self.navigationController?.pushViewController(vc, animated: true)
       }
  
    @IBAction func actionOnLoginViaEmail(_ sender: Any) {
        self.view.endEditing(true)

     let vc: LogInViaEmailVC = self.storyboard!.instantiateViewController(withIdentifier: "LogInViaEmailVC") as! LogInViaEmailVC
            self.navigationController?.pushViewController(vc, animated: false)
    }
  
    
    
    
    @IBAction func actionOnLogin(_ sender: UIButton) {
        self.view.endEditing(true)

        if(validation()){
            callOnLoginAPI(sender: sender)
        //    let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC")as! OTPVC
//            var strMobile = "\(self.txtMobile.text!.replacingOccurrences(of: " ", with: ""))"
//            strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"
//            dictTemp.setValue(strMobile.replacingOccurrences(of: " ", with: ""), forKey: "MobileNo")
//            dictTemp.setValue(self.txtName.text!, forKey: "Name")
//            dictTemp.setValue("\(self.txtCIty.tag)", forKey: "city_id")
//            dictTemp.setValue("\(self.txtState.tag)", forKey: "state_id")
//            testController.dictData = dictTemp
     //       self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    @IBAction func actionOnSelectCountry(_ sender: UIButton) {
        if(aryCountry.count == 0){
            callCountryAPI(sender: sender)
        }else{
            self.gotoPopViewWithArray(sender: sender, aryData: self.aryCountry, strTitle: "")
        }
        
    }
    
    // MARK: - --------------API Calling
    // MARK: -
    func callOnLoginAPI(sender : UIButton) {
           if !isInternetAvailable() {
               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
               
           }else{
               
               let strEmail = "\(txtEmail.text!.replacingOccurrences(of: " ", with: ""))"
               var strMobile = "\(txtMobile.text!.replacingOccurrences(of: " ", with: ""))"
                strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"

          
            
               let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UserLoginOTP xmlns='http://aahar.org.in/'>     <MobileNo>\(strMobile)</MobileNo><country_id>\(txtCountryCode.tag)</country_id><EmailId>\(strEmail)</EmailId><Password></Password></UserLoginOTP></soap12:Body></soap12:Envelope>"
             
            
            
               dotLoader(sender: sender , controller : self, viewContain: self.viewContain, onView: "Button")
               
               WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "UserLoginOTPResult", responcetype: "UserLoginOTPResponse") { (responce, status) in
                   remove_dotLoader(controller: self)
                   if status == "Suceess"{
                       let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                           let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC")as! OTPVC
                           var strMobile = "\(dictTemp.value(forKey: "Msg")!)"
                           strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"
                           dictTemp.setValue(strMobile.replacingOccurrences(of: " ", with: ""), forKey: "MobileNo")
                        dictTemp.setValue("", forKey: "Email")
                        dictTemp.setValue("\(self.txtCountryCode.tag)", forKey: "CountryCode")
                        dictTemp.setValue("LoginViaMobile", forKey: "LoginType")
                           testController.dictData = dictTemp
                           self.navigationController?.pushViewController(testController, animated: true)
                       }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                       }
                   }else{
                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                   }
               }
           }
       }
       
    
    func callCountryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCountryList xmlns='http://aahar.org.in/'></GetCountryList></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "FullView")
            
            WebService.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetCountryListResult", responcetype: "GetCountryListResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    self.aryCountry = NSMutableArray()
                     self.aryCountry = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.gotoPopViewWithArray(sender: sender, aryData: self.aryCountry, strTitle: "")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
        
        
        
    }
  
        // MARK: - --------------Extra Method
           // MARK: -
        func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
            let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
            vc.strTitle = strTitle
            vc.strTag = 14
            
            if aryData.count != 0{
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                vc.modalTransitionStyle = .coverVertical
                vc.delegate = self
                vc.aryTBL = aryData
                self.present(vc, animated: false, completion: {})
            }
        }
        
    // MARK: - --------------validation
    // MARK: -
    func validation() -> Bool {
        
        if (txtMobile.text?.count == 0){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
                   return false
                   
               }else if ((txtMobile.text?.count)! < 10){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
                   return false
               }else if (txtCountryCode.tag == 0){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CountryCode, viewcontrol: self)
                   return false
               }
        return true
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension LoginVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if (textField == txtName || textField == txtEmail)  {
//            return  txtFiledValidation(textField: txtName, string: string, returnOnly: "ALL", limitValue: 90)
//        }
//        if textField == txtMobile  {
//            return (txtFiledValidation(textField: txtMobile, string: string, returnOnly: "NUMBER", limitValue: 15))
//        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
// MARK: - ---------------PopUpViewDelegate
// MARK: -

extension LoginVC : PopUpViewDelegate{
    func GetDictFromPopUPScreen(dictData: NSDictionary, tag: Int) {
        txtCountryCode.tag = Int("\(dictData.value(forKey: "country_id")!)")!
        txtCountryCode.text = "\(dictData.value(forKey: "country_code")!)"

    }
    

}
