//
//  AddressGetFromMapVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/14/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddressGetFromMapVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnAddress: UIButton!
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    @IBOutlet weak var navingation_Item: UINavigationItem!
    @IBOutlet weak var viewTbl: UIView!
    @IBOutlet weak var tblList: UITableView!
    
    @IBOutlet weak var searchControl: UISearchBar!
    @IBOutlet weak var widthButton: NSLayoutConstraint!

    var matchingItems:[MKMapItem] = []

     private var locationManager: CLLocationManager!
     private var currentLocation: CLLocation?
     var handleDonateSurPluseScreen_1: DonateSurPluseScreenDelegate?
     var strLat = "0.0"
     var strLong = "0.0"
     var aryAddressList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        searchControl.tintColor = hexStringToUIColor(hex: primaryGreenColor)

       self.mapReload()
        btnAddress.layer.cornerRadius = 10.0
        viewCenter.backgroundColor  = UIColor.clear
        if(DeviceType.IS_IPHONE_X){
                        heightHeader.constant = 94
                    }else{
                        heightHeader.constant = 74
                    }

        
    }
    //MARK:
    //MARK: --------------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        let dict = NSMutableDictionary()
        dict.setValue(lblAddress.text!, forKey: "address")
        dict.setValue(self.strLat, forKey: "lat")
        dict.setValue(self.strLong, forKey: "long")
         handleDonateSurPluseScreen_1?.refreshDonateSurPluseScreen(dictData: dict, tag: 0)
        self.navigationController?.popViewController(animated: true)
       // self.dismiss(animated: true, completion: nil)
    }

    
    func mapReload()  {
        mapView.delegate = self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Check for Location Services
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        let buttonItem = MKUserTrackingBarButtonItem(mapView: mapView)
        self.navingation_Item.rightBarButtonItem = buttonItem
        mapView.showsUserLocation = true
    }
    
    func parseAddress(selectedItem:MKPlacemark) -> String {
          // put a space between "4" and "Melrose Place"
          let firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? " " : ""
          // put a comma between street and city/state
          let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
          // put a space between "Washington" and "DC"
          let secondSpace = (selectedItem.subAdministrativeArea != nil && selectedItem.administrativeArea != nil) ? " " : ""
          let addressLine = String(
              format:"%@%@%@%@%@%@%@",
              // street number
              selectedItem.subThoroughfare ?? "",
              firstSpace,
              // street name
              selectedItem.thoroughfare ?? "",
              comma,
              // city
              selectedItem.locality ?? "",
              secondSpace,
              // state
              selectedItem.administrativeArea ?? ""
          )
          return addressLine
      }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
    

        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
           
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    self.lblAddress.text = addressString
                    self.strLat = "\(pdblLatitude)"
                    self.strLong = "\(pdblLongitude)"
                    print(addressString)
                }
        })
    }
}
// MARK: - ---------------MKMapViewDelegate
// MARK: -


extension AddressGetFromMapVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        print( mapView.centerCoordinate.latitude)
        getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")

    }
  
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
         getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")
    }

    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation == nil {
            // Zoom to user location
            if let userLocation = locations.last {
                let viewRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
                   mapView.setRegion(viewRegion, animated: false)
                    mapView.centerCoordinate = (manager.location?.coordinate)!
                locationManager.stopUpdatingLocation()
                }
            }
        }
    }

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddressGetFromMapVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return matchingItems.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblList.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath as IndexPath) as! AddressCell
        let selectedItem = matchingItems[indexPath.row].placemark
        cell.lblTitle.text = selectedItem.name
        cell.lblTitleDetail.text = parseAddress(selectedItem: selectedItem)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.widthButton.constant = 44.0
        self.viewTbl.removeFromSuperview()
        let selectedItem = matchingItems[indexPath.row].placemark
        self.lblAddress.text = "\(selectedItem.name!)\n\(parseAddress(selectedItem: selectedItem))"
        
        self.view.endEditing(true)
        self.searchControl.text = ""
        let dict = NSMutableDictionary()
        dict.setValue(lblAddress.text!, forKey: "address")
        dict.setValue("\(selectedItem.coordinate.latitude)", forKey: "lat")
        dict.setValue("\(selectedItem.coordinate.longitude)", forKey: "long")
        handleDonateSurPluseScreen_1?.refreshDonateSurPluseScreen(dictData: dict, tag: 0)
        self.navigationController?.popViewController(animated: true)
    }
    
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class AddressCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleDetail: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
extension AddressGetFromMapVC :  UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.viewTbl.frame = CGRect(x: 0, y: self.viewHeader.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - self.viewHeader.frame.maxY)
        self.view.addSubview(self.viewTbl)
        self.matchingItems = []
        self.tblList.reloadData()
        self.widthButton.constant = 0.0
        self.searchControl.text = ""

    }
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         var searchBarText:NSString = searchBar.text! as NSString
              searchBarText = searchBarText.replacingCharacters(in: range, with: text) as NSString
        
        if(searchBarText == ""){
           self.matchingItems = []
            self.tblList.reloadData()
            return true

        }
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText as String
        request.region = self.mapView.region
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            guard let response = response else {
                return
            }
            self.matchingItems = response.mapItems
            self.tblList.reloadData()
        }
        
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.viewTbl.removeFromSuperview()
        self.widthButton.constant = 44.0
        self.searchControl.text = ""

    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
    
    }
    

}
