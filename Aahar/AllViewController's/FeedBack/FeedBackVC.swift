//
//  FeedBackVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class FeedBackVC: UIViewController {
  
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewNewFeedback: CardView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryFeedBackList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                        heightHeader.constant = 94
                    }else{
                        heightHeader.constant = 74
                    }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        viewNewFeedback.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 130
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callFeedbackListAPI() 
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononCreat(_ sender: UIButton) {
        let vc: CreatFeedBackVC = self.storyboard!.instantiateViewController(withIdentifier: "CreatFeedBackVC") as! CreatFeedBackVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:*********
    //MARK: API CAlling
    func callFeedbackListAPI() {
        if !isInternetAvailable() {
            showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetFeedbackSuggesion xmlns='http://aahar.org.in/'></GetFeedbackSuggesion></soap12:Body></soap12:Envelope>"
            
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetFeedbackSuggesionResult", responcetype: "GetFeedbackSuggesionResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    let aryTemp = dictTemp.value(forKey: "FeedbackSuggesion")as! NSArray
                    self.aryFeedBackList = NSMutableArray()
                     self.aryFeedBackList = aryTemp.mutableCopy()as! NSMutableArray
                    self.tvlist.reloadData()
                }else{
                    self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
        
    }
    // MARK: - --------------Show Error / Network
    // MARK: -
    
    func showErrorWithImage( strTitle : String , imgError : UIImage)   {
        removeErrorView()
        addErrorImageOnView(strMessage: strTitle, img: imgError, controller: self, view: viewHeader)
        btnRetry.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
    }
    @objc private func sayAction(_ sender: UIButton!) {
        removeErrorView()
        self.viewWillAppear(true)
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension FeedBackVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return aryFeedBackList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "FeedbackCell", for: indexPath as IndexPath) as! FeedbackCell
        let dict = aryFeedBackList.object(at: indexPath.row)as! NSDictionary
        cell.lblDate.textColor = hexStringToUIColor(hex: primaryGreenTextColor)
        cell.lblname.text = "\(dict.value(forKey: "Name")!)"
        cell.lblDetail.text = "\(dict.value(forKey: "Feedback")!)"
        cell.lblDate.text = "\(dict.value(forKey: "CreatedDate")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }

}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class FeedbackCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
