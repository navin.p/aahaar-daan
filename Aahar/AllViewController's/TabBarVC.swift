//
//  TabBarVC.swift
//  Aahar
//
//  Created by Navin Patidar on 10/10/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class TabBarVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)], for: .selected)
        
    }
    

   

}
