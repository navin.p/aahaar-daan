
//
//  AaharCardVC.swift
//  Aahar
//
//  Created by Navin Patidar on 10/10/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import SafariServices
class AaharCardVC: UIViewController {
    
    // MARK: - -------------IBOutlet
    // MARK: -
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnNumberOfCallAttend: UIButton!
    @IBOutlet weak var btnFoodPickupPerson: UIButton!
    @IBOutlet weak var btnFoddPickupLocation: UIButton!
    @IBOutlet weak var btnSpendHrs: UIButton!
    
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    
    @IBOutlet weak var viewHeader: UILabel!
    @IBOutlet weak var lblNumberOfCallAttend: UILabel!
    @IBOutlet weak var lblFoodPickupPerson: UILabel!
    @IBOutlet weak var lblFoddPickupLocation: UILabel!
    @IBOutlet weak var lblSpendHrs: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var tv: UITableView!

    // MARK: - -------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tv.tableFooterView = UIView()
        if(nsud.value(forKey: "Aahar_LoginData") != nil){
            let dictLoginData = nsud.value(forKey: "Aahar_LoginData")as! NSDictionary
           lblUserName.text = "\(dictLoginData.value(forKey: "Name")!)"
           imgProfile.layer.borderWidth = 1.0
            imgProfile.layer.masksToBounds = false
            imgProfile.layer.cornerRadius = imgProfile.frame.height/2
            imgProfile.clipsToBounds = true
            imgProfile.layer.borderColor = UIColor.white.cgColor
            let urlImage = "http://aahar.org.in/ProfileImage/\(dictLoginData.value(forKey: "ProfileImage")!)"
            imgProfile.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "profile"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
        }
        
      
        
        if(DeviceType.IS_IPHONE_X){
                                heightHeader.constant = 94
                            }else{
                                heightHeader.constant = 74
                            }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(true)
         callGetVolunteerDashboardAPI()
    }
    override func viewWillLayoutSubviews() {
        // scroll.contentSize = CGSize(width: self.scroll.frame.width, height: 650)
    }
    // MARK: - -------------IBAction
    // MARK: -
    
    @IBAction func actionLink(_ sender: Any) {
       // let pth = "http://greengene.citizencop.org"
        let url = NSURL(string: Website)
        let svc = SFSafariViewController(url: url! as URL)
        present(svc, animated: true, completion: nil)
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    
    func callGetVolunteerDashboardAPI() {
        if !isInternetAvailable() {
           FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
            
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetVolunteerDashboard xmlns='http://aahar.org.in/'><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><city_id></city_id><Filter></Filter></GetVolunteerDashboard></soap12:Body></soap12:Envelope>"
            
            activity.startAnimating()
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetVolunteerDashboardResult", responcetype: "GetVolunteerDashboardResponse") { (responce, status) in
                self.activity.stopAnimating()
                if status == "Suceess"{
                    
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                      
                        
                        let arytemp = dictTemp.value(forKey: "Dashboard")as! NSArray
                        
                        let dict1 = arytemp.object(at: 0)as! NSDictionary
                        let dict2 = arytemp.object(at: 1)as! NSDictionary
                        let dict3 = arytemp.object(at: 2)as! NSDictionary
                        let dict4 = arytemp.object(at: 3)as! NSDictionary

                        let strNumberOfCallAttendTitle = "\(dict1.value(forKey: "SummaryType")!)"
                        let strNumberOfCallAttendCount = "\(dict1.value(forKey: "TotalCount")!)"

                        let strFoddPickupLocationTitle = "\(dict2.value(forKey: "SummaryType")!)"
                        let strFoddPickupLocationCount = "\(dict2.value(forKey: "TotalCount")!)"
                        
                        let strFoodPickupPersonTitle = "\(dict3.value(forKey: "SummaryType")!)"
                        let strFoodPickupPersonCount = "\(dict3.value(forKey: "TotalCount")!)"
                        
                        let strSpendHrsTitle = "\(dict4.value(forKey: "SummaryType")!)"
                        let strSpendHrsCount = "\(dict4.value(forKey: "TotalCount")!)"
                        
                        self.btnSpendHrs.setTitle(strSpendHrsCount, for: .normal)
                        self.btnFoodPickupPerson.setTitle(strFoodPickupPersonCount, for: .normal)
                        self.btnNumberOfCallAttend.setTitle(strNumberOfCallAttendCount, for: .normal)
                        self.btnFoddPickupLocation.setTitle(strFoddPickupLocationCount, for: .normal)

                        
                        self.lblSpendHrs.text = strSpendHrsTitle
                        self.lblFoodPickupPerson.text = strFoodPickupPersonTitle
                        self.lblFoddPickupLocation.text = strFoddPickupLocationTitle
                        self.lblNumberOfCallAttend.text = strNumberOfCallAttendTitle

                        
                        
                    }else{
                        FTIndicator.showNotification(withTitle: alertMessage ,message: alertSomeError)
                    }
                }else{
                    FTIndicator.showNotification(withTitle: alertMessage ,message: alertSomeError)
                }
            }
        }
        
    }
}
