//
//  ProcureDiscountedPackedItemVC.swift
//  Aahar
//
//  Created by Navin Patidar on 3/4/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class ProcureDiscountedPackedItemVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strTitle = String()
    
    @IBOutlet weak var imgAdvertisement: UIImageView!
    var timer = Timer()
    var strAdvertiseTag  = 0
    var strErrorMessage  = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                              heightHeader.constant = 94
                          }else{
                              heightHeader.constant = 74
                          }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        lblTitle.text = strTitle
        if(aryAdvertise_Data.count != 0){
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(aryList.count == 0){
           callGetDiscountItemAllListAPI()
        }
    }

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:
    //MARK:------------updateTimer For Advertisement
    @objc func updateTimer() {
        strAdvertiseTag  = strAdvertiseTag + 1
        let dict = aryAdvertise_Data.object(at: strAdvertiseTag)as! NSDictionary
        let urlImage = "\(dict.value(forKey: "url")!)" + "\(dict.value(forKey: "image")!)".replacingOccurrences(of: "~/", with: "")
        imgAdvertisement.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(strAdvertiseTag == aryAdvertise_Data.count - 1){
            strAdvertiseTag = 0
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
    }
    // MARK: - ---------------API Calling
    // MARK: -
    func callGetDiscountItemAllListAPI() {
         self.strErrorMessage = ""
        if !isInternetAvailable() {
            self.strErrorMessage = alertInternet
            self.tvlist.reloadData()
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetDiscountItemAll xmlns='http://aahar.org.in/'><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id></GetDiscountItemAll></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetDiscountItemAllResult", responcetype: "GetDiscountItemAllResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryList = NSMutableArray()
                        self.filterData(aryData: (dictTemp.value(forKey: "FoodDonationMapping")as! NSArray).mutableCopy()as! NSMutableArray)
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                   
                }
            }
        }
    }
    
    func filterData(aryData : NSMutableArray)  {
        let aryTemp = NSMutableArray()
       let doner_ID =  "\(dict_Login_Data.value(forKey: "DonorVId")!)"
        for item in aryData {
            let donerID =  "\((item as AnyObject).value(forKey: "DonorVId")!)"
            if(donerID != doner_ID){
                aryTemp.add(item)
            }else{
              //  aryTemp.add(item)
            }
        }
        self.aryList = aryTemp
        self.arySelectedList = NSMutableArray()
        self.arySelectedList = aryTemp
        self.tvlist.reloadData()
        if(aryList.count == 0){
            self.strErrorMessage = alertDataNotFound
            self.tvlist.reloadData()
            
         
        }else{
            self.strErrorMessage = ""
            self.tvlist.reloadData()
        }
    }
   
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ProcureDiscountedPackedItemVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ProcureDiscountedPackedItemCell", for: indexPath as IndexPath) as! ProcureDiscountedPackedItemCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary

        let urlImage = "\(dict.value(forKey: "ProductImage")!)"
        cell.Image_Item.setImageWith(URL(string: "http://aahar.org.in/DiscountItemImage/" + urlImage), placeholderImage: UIImage(named: "defult_news_img"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)

        cell.lbl_Name.text = "\(dict.value(forKey: "InstitutionName")!)"
        cell.lbl_Name_Of_Item.text = "\(dict.value(forKey: "ProductName")!)"
        cell.lbl_Address.text = "\(dict.value(forKey: "Address")!)"
        cell.lbl_Product_Exp_Date.text = "Product Exp. Date - \(dict.value(forKey: "ExpDate")!)"
        cell.lbl_MRP_Origional.text = "MRP \u{20B9} - \(dict.value(forKey: "MRP")!)"
         cell.lbl_MRP_Discounted.text = "Discount \u{20B9} - \(dict.value(forKey: "DiscountPrice")!)"
        cell.lbl_Contact_person.text = "\(dict.value(forKey: "ContactPersonName")!)(\(dict.value(forKey: "ContactPersonNo")!))"
      //  cell.lbl_MRP_Origional.text="MRP \u{20B9} - 20.0"
     //   cell.lbl_MRP_Origional.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        cell.lbl_MRP_Discounted.backgroundColor = hexStringToUIColor(hex: primaryOrangeColor)
     //   cell.lbl_MRP_Discounted.text="Discount \u{20B9} - 0.0"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ProcureDiscountedPackedItemDetailVC")as! ProcureDiscountedPackedItemDetailVC
        testController.dictData = (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(testController, animated: true)
    }
   
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
}
// MARK: - ----------------ProfileCell
// MARK: -
class ProcureDiscountedPackedItemCell: UITableViewCell {
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_Contact_person: UILabel!
    @IBOutlet weak var lbl_Product_Exp_Date: UILabel!
    @IBOutlet weak var lbl_MRP_Origional: UILabel!
    @IBOutlet weak var lbl_MRP_Discounted: UILabel!
    @IBOutlet weak var lbl_Name_Of_Item: UILabel!
    @IBOutlet weak var Image_Item: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -


extension ProcureDiscountedPackedItemVC: UITextFieldDelegate  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchAutocomplete(Searching: textField.text! as NSString)
        self.view.endEditing(true)
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
        return true
    }
    
    func searchAutocomplete(Searching: NSString) -> Void {
        // let resultPredicate = NSPredicate(format: "name like %@", Searching)
         self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "InstitutionName contains[c] %@ OR ProductName contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.arySelectedList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.arySelectedList.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(aryList.count == 0){
            self.strErrorMessage = alertDataNotFound
            self.tvlist.reloadData()
            
        }
    }
}
