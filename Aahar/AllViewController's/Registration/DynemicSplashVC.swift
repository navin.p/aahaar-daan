//
//  DynemicSplashVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class DynemicSplashVC: UIViewController {
    @IBOutlet weak var imgView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if(nsud.value(forKey: "Aahar_LoginData") != nil){
            let dictLoginData = nsud.value(forKey: "Aahar_LoginData")as! NSDictionary
            let urlImage = "http://aahar.org.in/images/\(dictLoginData.value(forKey: "CitySplashScreen")!)"
            if(urlImage.count != 0){
                imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    print(url ?? 0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { // Change `2.0` to the desired number of seconds.
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                    
                }, usingActivityIndicatorStyle: .gray)
            }else{
                   let testController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
                                 self.navigationController?.pushViewController(testController, animated: true)
            }
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
      
        // Do any additional setup after loading the view.
    }
    

 
}
