//
//  DonateFreshFoodVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/28/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class DonateFreshFoodVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgAdvertisement: UIImageView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var timer = Timer()
    var strAdvertiseTag  = 0
    var aryList = NSMutableArray()
    var aryOrganizationList = NSMutableArray()
     var strTitle = String()
    var strErrorMessage  = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                          heightHeader.constant = 94
                      }else{
                          heightHeader.constant = 74
                      }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 200
        lblTitle.text = strTitle
        if(aryAdvertise_Data.count != 0){
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        txtSearch.text = ""
        if(aryList.count == 0){
            strErrorMessage = ""
            callOrganizationListAPI()
        }
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        timer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:
    //MARK:------------UpdateTimer For Advertisement
    @objc func updateTimer() {
        strAdvertiseTag  = strAdvertiseTag + 1
        let dict = aryAdvertise_Data.object(at: strAdvertiseTag)as! NSDictionary
        let urlImage = "\(dict.value(forKey: "url")!)" + "\(dict.value(forKey: "image")!)".replacingOccurrences(of: "~/", with: "")
        imgAdvertisement.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(strAdvertiseTag == aryAdvertise_Data.count - 1){
            strAdvertiseTag = 0
        }
        imgAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callOrganizationListAPI() {
        if !isInternetAvailable() {
             strErrorMessage = alertInternet
            self.tvlist.reloadData()
            //showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Get_Organization_List_Details_By_CityId xmlns='http://aahar.org.in/'><CityId>\(dict_Login_Data.value(forKey: "city_id")!)</CityId></Get_Organization_List_Details_By_CityId></soap12:Body></soap12:Envelope>"
            dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Get_Organization_List_Details_By_CityIdResult", responcetype: "Get_Organization_List_Details_By_CityIdResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                self.strErrorMessage = ""
               
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        self.aryList = NSMutableArray()
                        self.aryList = (dictTemp.value(forKey: "OrganizationList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.aryOrganizationList = NSMutableArray()
                        self.aryOrganizationList = (dictTemp.value(forKey: "OrganizationList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.tvlist.reloadData()
                    }else{
                        self.strErrorMessage = alertDataNotFound
                        self.tvlist.reloadData()
                    }
                }else{
                    self.strErrorMessage = alertSomeError
                    self.tvlist.reloadData()
                }
            }
        }
    }
  
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DonateFreshFoodVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DonatedFreshFoodCell", for: indexPath as IndexPath) as! DonatedFreshFoodCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.txtName.text = "\(dict.value(forKey: "Organization_Name")!)"
        cell.txtaddress.text = "\(dict.value(forKey: "Organization_Address")!)"
        cell.txtContact.text = "Contact No. - \(dict.value(forKey: "Poc_Mobile")!)"
        cell.btnMap.tag = indexPath.row
        cell.btnMap.addTarget(self, action: #selector(pressButtonOnMap(_:)), for: .touchUpInside)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: DonateFreshFoodDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "DonateFreshFoodDetailVC") as! DonateFreshFoodDetailVC
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        vc.strTitle = "\(dict.value(forKey: "Organization_Name")!)"
        vc.dictData = (aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        customView.backgroundColor = UIColor.clear
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        button.setTitle("\(strErrorMessage)", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        customView.addSubview(button)
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    @objc func pressButtonOnMap(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        let dict = aryList.object(at: button.tag)as! NSDictionary
        if("\(dict.value(forKey: "Organization_Latitude")!)" == ""  && "\(dict.value(forKey: "Organization_Latitude")!)" == "0"){
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Organization Address not found!", viewcontrol: self)
        }else{
         openMap(strTitle: "\(dict.value(forKey: "Organization_Name")!)", strlat: "\(dict.value(forKey: "Organization_Latitude")!)", strLong: "\(dict.value(forKey: "Organization_Longitude")!)")
        }
      }
}
// MARK: - ----------------ProfileCell
// MARK: -
class DonatedFreshFoodCell: UITableViewCell {
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtaddress: UILabel!
    @IBOutlet weak var txtContact: UILabel!
    
    @IBOutlet weak var btnMap: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -


extension DonateFreshFoodVC: UITextFieldDelegate  {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchAutocomplete(Searching: textField.text! as NSString)
        self.view.endEditing(true)
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        txtSearch.text = ""
        return true
    }
    
    func searchAutocomplete(Searching: NSString) -> Void {
         self.strErrorMessage = ""
        let resultPredicate = NSPredicate(format: "Organization_Name contains[c] %@ OR Organization_Name contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryOrganizationList ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryOrganizationList.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
        if(aryList.count == 0){
            
            self.strErrorMessage = alertDataNotFound
            self.tvlist.reloadData()
        }
    }
}
