//
//  DonateFreshFoodDetailVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/28/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DonateFreshFoodDetailVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var btnNavigation: UIButton!

    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    var strTitle = String()
    var dictData = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnBookNow.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_6){
            heightImage.constant = 165.0
        }else if(DeviceType.IS_IPHONE_6P){
            heightImage.constant = 175.0
        }else if(DeviceType.IS_IPHONE_X){
            heightImage.constant = 190.0
        }else if(DeviceType.IS_IPHONE_XR_XS_MAX){
            heightImage.constant = 200.0
        }else{
            heightImage.constant = 150.0
        }
        
        if(DeviceType.IS_IPHONE_X){
                              heightHeader.constant = 94
                          }else{
                              heightHeader.constant = 74
                          }
        btnNavigation.layer.cornerRadius = 25
        btnNavigation.layer.borderColor = UIColor.white.cgColor
        btnNavigation.layer.borderWidth = 1.0
        btnNavigation.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        lblTitle.text = strTitle
 
        lblAddress.text = "\(self.dictData.value(forKey: "Organization_Address")!)"
        lblContact.text = "\(self.dictData.value(forKey: "Poc_Name")!) \n\(self.dictData.value(forKey: "Poc_Mobile")!)"
        
        let urlImage = "\(dictData.value(forKey: "Organization_Image")!)"
       imgView.setImageWith(URL(string: BaseURLImagePathOrganization + urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let center = CLLocationCoordinate2DMake(Double("\(self.dictData.value(forKey: "Organization_Latitude")!)")!, Double("\(self.dictData.value(forKey: "Organization_Longitude")!)")!)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView.setRegion(region, animated: true)
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(Double("\(self.dictData.value(forKey: "Organization_Latitude")!)")!, Double("\(self.dictData.value(forKey: "Organization_Longitude")!)")!)
        myAnnotation.title = "\(self.dictData.value(forKey: "Organization_Name")!)"
        self.mapView.showsUserLocation = true
        self.mapView.addAnnotation(myAnnotation)
    }

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionBookNow(_ sender: UIButton) {
        let vc: DonateFreshoddBookNowVC = self.storyboard!.instantiateViewController(withIdentifier: "DonateFreshoddBookNowVC") as! DonateFreshoddBookNowVC
        vc.strTitle = self.lblTitle.text!
        vc.dictData = self.dictData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func actionNavigation(_ sender: UIButton) {
        if("\(dictData.value(forKey: "Organization_Latitude")!)" == ""  && "\(dictData.value(forKey: "Organization_Latitude")!)" == "0"){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Organization Address not found!", viewcontrol: self)
        }else{
            openMap(strTitle: "\(dictData.value(forKey: "Organization_Name")!)", strlat: "\(dictData.value(forKey: "Organization_Latitude")!)", strLong: "\(dictData.value(forKey: "Organization_Longitude")!)")
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -


extension DonateFreshFoodDetailVC: MKMapViewDelegate  {
    
}
