//
//  DashBoardVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/18/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.

import UIKit
import MapKit
import UserNotifications
import Firebase
import FirebaseInstanceID
//MARK:
//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}

extension DashBoardVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        
        if(strType == "Donate Now"){
            self.strViewComeFrom = "Donate"
            self.viewWillAppear(true)
        }else if(strType == "Procure Food"){
            self.strViewComeFrom = "Procure"
            self.viewWillAppear(true)
        }else if(strType == "Top Donor/Volunteer"){
            let vc: TopDonor_VolunteerVC = self.storyboard!.instantiateViewController(withIdentifier: "TopDonor_VolunteerVC") as! TopDonor_VolunteerVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(strType == "Gallery"){
            let vc: GalleryVC = self.storyboard!.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(strType == "News"){
            let vc: NewsVC = self.storyboard!.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(strType == "Feedback"){
            let vc: FeedBackVC = self.storyboard!.instantiateViewController(withIdentifier: "FeedBackVC") as! FeedBackVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(strType == "Profile"){
            let vc: ProfileVc = self.storyboard!.instantiateViewController(withIdentifier: "ProfileVc") as! ProfileVc
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(strType == "History"){
            let vc: HistoryVC = self.storyboard!.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        else if(strType == "Share App"){
            if let myApplink = NSURL(string: application_ID) {
                let objectsToShare = [AppName, myApplink] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        else if(strType == "LogOut") {
            let alert = UIAlertController(title: alertMessage, message: alertLogout, preferredStyle: .alert)
  
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { action in
                print("Yay! You brought your towel!")
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                self.navigationController?.pushViewController(testController, animated: true)
            }))
            self.present(alert, animated: true)
        }
       
            
            
            
        else if (strType == "Settings"){
          let vc: OtherVC = self.storyboard!.instantiateViewController(withIdentifier: "OtherVC") as! OtherVC
                self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if (strType == "Change Password"){
                let vc: ChangePasswordVC = self.storyboard!.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                      self.navigationController?.pushViewController(vc, animated: true)
              }
    }
}

//MARK:
//MARK: ---------------class DashBoardVC-----------------
class DashBoardVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var view1: CardView!
    @IBOutlet weak var view2: CardView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet var viewDonate: UIView!
    @IBOutlet weak var btnDonateSurplus: UIButton!
    @IBOutlet weak var btnDiscountedpackedItem: UIButton!
    @IBOutlet weak var btnDonateFreshHotMeal: UIButton!
    @IBOutlet var viewProcure: UIView!
    @IBOutlet weak var btnProcureDiscountedpackedItem: UIButton!
    @IBOutlet weak var btnProcureSurplus: UIButton!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    
    var locationManager: CLLocationManager!

    var strViewComeFrom = ""

    //MARK:
    //MARK: --------------Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
            heightHeader.constant = 94
        }else{
            heightHeader.constant = 74
        }
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
//        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnMenu.setImage(UIImage(named: "menu_icon"), for: .normal)
        btnDonateSurplus.layer.cornerRadius = 5.0
        btnDonateFreshHotMeal.layer.cornerRadius = 5.0
        btnDiscountedpackedItem.layer.cornerRadius = 5.0
        
        btnProcureSurplus.layer.cornerRadius = 5.0
        btnProcureDiscountedpackedItem.layer.cornerRadius = 5.0
        view1.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.8) {
            self.view1.transform = CGAffineTransform.identity
        }
        view2.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.6) {
            self.view2.transform = CGAffineTransform.identity
        }
        getCurrentAddress()
        callGetAdvertiseAPI()

        let center = UNUserNotificationCenter.current()
        // Request permission to display alerts and play sounds.
        center.requestAuthorization(options: [.alert, .sound])
        { (granted, error) in
            if(granted){
                  self.getFCM_Token()
            }
        }
        print("LoginData-----------\(dict_Login_Data)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        if(nsud.value(forKey: "Aahar_LoginData") != nil){
            dict_Login_Data = NSMutableDictionary()
            dict_Login_Data = (nsud.value(forKey: "Aahar_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            callUserDataByDonerIDAPI()

        }
        if(strViewComeFrom == "Donate"){
            self.viewDonate.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.2) {
                self.viewDonate.transform = .identity
                self.viewDonate.frame = self.view.frame
                self.view.addSubview(self.viewDonate)
            }
        }
        else if (strViewComeFrom == "Procure"){
            self.viewProcure.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
            UIView.animate(withDuration: 0.2) {
                self.viewProcure.transform = .identity
                self.viewProcure.frame = self.view.frame
                self.view.addSubview(self.viewProcure)
            }
        }
        print(dict_Login_Data)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.PushNotificationCheck()
        }
       
    let date = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd"
    let day  = dateFormatter.string(from: date)
 
        
        if((nsud.value(forKey: "AaharVersionUpdateDate")) != nil){
            if(Int(day) != Int((nsud.value(forKey: "AaharVersionUpdateDate")!)as! String)){
                checkUpdate()
            }
        }else{
               checkUpdate()
        }
        
    }
    
    //MARK:
    //MARK: checkUpdate
    func checkUpdate() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        nsud.set(day, forKey: "AaharVersionUpdateDate")

        VersionCheck.shared.checkAppStore() { isNew, version , message in
               if(isNew!){
                                                                                            let alert = UIAlertController(title: "New Version Available", message: message, preferredStyle: .alert)
                   alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { action in
                       guard let url = URL(string: application_ID) else { return }
                       UIApplication.shared.open(url)

                   }))
                   alert.addAction(UIAlertAction(title: "Skip", style: .default, handler: { action in
                       nsud.set(true, forKey: "AaharVersionUpdate")

                     }))
                   self.present(alert, animated: true, completion: nil)
               }
           }
    }
    
    //MARK:
    //MARK: PUSH Notification
    func PushNotificationCheck() {
        if(dict_Login_Data.count != 0){
            if(appDelegate.dictNotificationData.count != 0){
                let strPushType = "\(appDelegate.dictNotificationData.value(forKey: "PushType")!)"
                let aps = appDelegate.dictNotificationData.value(forKey: "aps")as! NSDictionary
                let alert = aps.value(forKey: "alert")as! NSDictionary
                let strbody = "\(alert.value(forKey: "body")!)"
                let strtitle = "\(alert.value(forKey: "title")!)"
                print(appDelegate.dictNotificationData)
                if(appDelegate.statusActive){
                    showAlertWithoutAnyAction(strtitle: "\(strtitle)", strMessage: "\(strbody)", viewcontrol: self)
                }else{
                    if(strPushType == "donatePushNotification"){
                        let vc: ProcureSurPlusFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureSurPlusFoodVC") as! ProcureSurPlusFoodVC
                        vc.strTitle = "Donate Surplus Food"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        showAlertWithoutAnyAction(strtitle: "\(strtitle)", strMessage: "\(strbody)", viewcontrol: self)

                    }
                }
                appDelegate.dictNotificationData = NSMutableDictionary()
                appDelegate.statusActive = false
            }
        }
    }
    
    
    
    //MARK:
    //MARK: CurrentAddress
    func getCurrentAddress() {
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    //MARK:
    //MARK: FCM_Token
    func getFCM_Token(){
        //For Get Token
        let center1 = UNUserNotificationCenter.current()
        center1.delegate = self
        center1.getNotificationSettings(completionHandler: { settings in
            switch settings.authorizationStatus {
            case .authorized, .provisional:
                print("authorized")
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    if let token = InstanceID.instanceID().token() {
                        nsud.setValue("\(token)", forKey: "Aahar_FCM_Token")
                        nsud.synchronize()
                        self.callAddDeviceRegistrationAPI(strToken: "\(token)")
                        print("FCM_Token----------------- : \(String(describing: token))")
                        print("DeviceID----------------- : \(UIDevice.current.identifierForVendor!.uuidString)")
                    }
                }
            case .denied:
                break
            case .notDetermined:
                break
            @unknown default:
                break
            }
        })
    }
    
    
    //MARK:
    //MARK: --------------IBAction
    @IBAction func actionOnBack(_ sender: UIButton) {
        sender.tag == 2 ? viewProcure.removeFromSuperview()  :  viewDonate.removeFromSuperview()
        strViewComeFrom = ""
    }
    @IBAction func actionOnOther(_ sender: UIButton) {
        let vc: OtherVC = self.storyboard!.instantiateViewController(withIdentifier: "OtherVC") as! OtherVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
  
    @IBAction func actionOnDonate_Buttons(_ sender: UIButton) {
  
            if((dict_Login_Data.value(forKey: "UserType")as! String == "Individual" &&  dict_Login_Data.value(forKey: "VerificationStatus")as! String == "Approved")) || ((dict_Login_Data.value(forKey: "UserType")as! String == "Organization" &&  dict_Login_Data.value(forKey: "VerificationStatus")as! String == "Approved")){
                if(sender.tag == 1){
                    let vc: DonateSurplusFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "DonateSurplusFoodVC") as! DonateSurplusFoodVC
                    vc.strTitle = (sender.titleLabel?.text!)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if (sender.tag == 2){
                    let vc: DonatedDiscountedFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "DonatedDiscountedFoodVC") as! DonatedDiscountedFoodVC
                    vc.strTitle = (sender.titleLabel?.text!)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let vc: DonateFreshFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "DonateFreshFoodVC") as! DonateFreshFoodVC
                    vc.strTitle = (sender.titleLabel?.text!)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertVerificationStatus, viewcontrol: self)
            }
     
    }
    @IBAction func actionOnProcure_Buttons(_ sender: UIButton) {
   
            if((dict_Login_Data.value(forKey: "UserType")as! String == "Individual" &&  dict_Login_Data.value(forKey: "VerificationStatus")as! String == "Approved")) || ((dict_Login_Data.value(forKey: "UserType")as! String == "Organization" &&  dict_Login_Data.value(forKey: "VerificationStatus")as! String == "Approved")){
                if(sender.tag == 1){
                    let vc: ProcureSurPlusFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureSurPlusFoodVC") as! ProcureSurPlusFoodVC
                    vc.strTitle = (sender.titleLabel?.text!)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }else if (sender.tag == 2){
                    let vc: ProcureDiscountedPackedItemVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureDiscountedPackedItemVC") as! ProcureDiscountedPackedItemVC
                    vc.strTitle = (sender.titleLabel?.text!)!
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertVerificationStatus, viewcontrol: self)
            }
   
    }
    @IBAction func actionOnButtons(_ sender: UIButton) {
        
        if (sender.tag == 3){ // MENU
            let vc: DrawerVC = self.storyboard!.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.handleDrawerView = self
            self.present(vc, animated: false, completion: {})
        }else{
            if(dict_Login_Data.value(forKey: "UserType")as! String == ""){
                let alert = UIAlertController(title: alertMessage, message: sender.tag == 1 ? alertUpdateProfile : alertUpdateProfileProcure, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                    print("Yay! You brought your towel!")
                }))
                alert.addAction(UIAlertAction(title: "UPDATE", style: .destructive, handler: { action in
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC")as! EditProfileVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }))
                self.present(alert, animated: true)
                
            }else{
                if(sender.tag == 1){// Donete
                    
//                    // remove donation button when testing user login
//                    if("\(dict_Login_Data.value(forKey: "DonorVId")!)" == "166"){
//                        btnDonateFreshHotMeal.isHidden = true
//                    }else{
//                        btnDonateFreshHotMeal.isHidden = false
//                    }
//
//
//                    self.viewDonate.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//                    UIView.animate(withDuration: 0.2) {
//                        self.viewDonate.transform = .identity
//                        self.viewDonate.frame = self.view.frame
//                        self.view.addSubview(self.viewDonate)
//                    }
                    
                    
                    let vc: DonateSurplusFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "DonateSurplusFoodVC") as! DonateSurplusFoodVC
                      vc.strTitle = "Donate Surplus Food"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else if(sender.tag == 2){// Procure
//                    self.viewProcure.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//                    UIView.animate(withDuration: 0.2) {
//                        self.viewProcure.transform = .identity
//                        self.viewProcure.frame = self.view.frame
//                        self.view.addSubview(self.viewProcure)
//                    }
                    
                    let vc: ProcureSurPlusFoodVC = self.storyboard!.instantiateViewController(withIdentifier: "ProcureSurPlusFoodVC") as! ProcureSurPlusFoodVC
                    vc.strTitle = "Procure Surplus Food"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callAddDeviceRegistrationAPI(strToken : String) {
        if !isInternetAvailable() {
            //    showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><DeviceRegistration xmlns='http://aahar.org.in/'><Did>0</Did><DeviceId>\(strToken)</DeviceId><imeiNo>\(UIDevice.current.identifierForVendor!.uuidString)</imeiNo><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId></DeviceRegistration></soap12:Body></soap12:Envelope>"
         //   dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "DeviceRegistrationResult", responcetype: "DeviceRegistrationResponse") { (responce, status) in
           //     remove_dotLoader(controller: self)
                if status == "Suceess"{
                    print(responce)
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){

                    }else{
                        //   self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                    //  self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
        
    }
    
    func callGetAdvertiseAPI() {
        if !isInternetAvailable() {
        //    showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAdvertisement xmlns='http://aahar.org.in/'></GetAdvertisement></soap12:Body></soap12:Envelope>"
          //  dotLoader(sender: UIButton() , controller : self, viewContain: self.view, onView: "MainView")
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetAdvertisementResult", responcetype: "GetAdvertisementResponse") { (responce, status) in
              //  remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if(dictTemp.value(forKey: "Result")as! String ==  "True"){
                        let aryTemp = dictTemp.value(forKey: "Detail")as! NSArray
                       aryAdvertise_Data = NSMutableArray()
                       aryAdvertise_Data = aryTemp.mutableCopy()as! NSMutableArray
                    }else{
                     //   self.showErrorWithImage(strTitle: alertDataNotFound, imgError: strDataNotFoundImage)
                    }
                }else{
                  //  self.showErrorWithImage(strTitle: alertSomeError, imgError: strDataNotFoundImage)
                }
            }
        }
        
    }
    
    
    
    func callUserDataByDonerIDAPI() {
         if !isInternetAvailable() {
         //    showErrorWithImage(strTitle: alertInternet, imgError: strInternetImage)
         }else{
             let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetProfile xmlns='http://aahar.org.in/'> <DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId></GetProfile></soap12:Body></soap12:Envelope>"
             WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "GetProfileResult", responcetype: "GetProfileResponse") { (responce, status) in
                 if status == "Suceess"{
                     let dictTemp = responce.value(forKey: "data")as! NSDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let aryTemp = dictTemp.value(forKey: "ProfileDetail")as! NSArray
                        let dictLoginData = aryTemp.object(at: 0)as! NSDictionary
                        nsud.setValue(dictLoginData, forKey: "Aahar_LoginData")
                        dict_Login_Data = NSMutableDictionary()
                        dict_Login_Data = (nsud.value(forKey: "Aahar_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    }else{
                       // showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "OTP invalid!", viewcontrol: self)
                     }
                 }else{
                 }
             }
         }
         
     }
    
    
}
// MARK: --------CLLocationManager Delegate Method----------

extension DashBoardVC: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        /* you can use these values*/
        strLat = "\( location.coordinate.latitude)"
        strLong =  "\(location.coordinate.longitude)"
        locationManager.stopUpdatingLocation()
    }
}
// MARK: --------UNNotification/Messaging Delegate Method----------

extension DashBoardVC : UNUserNotificationCenterDelegate{
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if let token = InstanceID.instanceID().token() {
            nsud.setValue("\(token)", forKey: "Aahar_FCM_Token")
            nsud.synchronize()
            print("FCM_Token----------------- : \(String(describing: token))")
            print("DeviceID----------------- : \(UIDevice.current.identifierForVendor!.uuidString)")

        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "Aahar_FCM_Token")
        nsud.synchronize()
    }
    
}

