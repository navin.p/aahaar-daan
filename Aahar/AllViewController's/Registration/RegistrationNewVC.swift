//
//  RegistrationNewVC.swift
//  Aahar
//
//  Created by Navin Patidar on 1/9/20.
//  Copyright © 2020 Saavan_patidar. All rights reserved.
//

import UIKit

class RegistrationNewVC: UIViewController {
    
    
    //MARK:-
    //MARK:- ---------IBOutlet
    
    
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnRegistration: UIButton!
    @IBOutlet weak var viewContain: UIView!

    
    //MARK:-
    //MARK:- ---------LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRegistration.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        btnRegistration.layer.cornerRadius = 8.0
      
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        if(textField == txtMobile){
            trimmedText = "\(trimmedText.replacingOccurrences(of: "-", with: ""))"
        }
        textField.text = trimmedText
    }
    
    //MARK:-
    //MARK:- ---------IBAction
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnRegistration(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            callRegistrationAPI(sender: sender)
        }
    }
    //MARK:-
    //MARK:- ---------validation
    func validation() -> Bool {
        
        if (txtUserName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_UserName, viewcontrol: self)
            return false
        } else if (txtMobile.text?.count == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
                    return false
                    
                }else if ((txtMobile.text?.count)! < 10){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
                    return false
                }
        if(txtEmail.text?.count != 0){
          if !(validateEmail(email: txtEmail.text!)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
                return false
            }
        }
        return true
    }
    //MARK:-
       //MARK:- ---------API Calling
    func callRegistrationAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            
        }else{
            
            let dict = NSMutableDictionary()
            dict.setValue(txtUserName.text!, forKey: "Name")
            dict.setValue(txtMobile.text!, forKey: "MobileNo")
            dict.setValue(txtEmail.text!, forKey: "EmailId")
            dict.setValue("1", forKey: "country_id")

                 var json = Data()
                           var jsonString = NSString()
                           if JSONSerialization.isValidJSONObject(dict) {
                               json = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                               jsonString = String(data: json, encoding: .utf8)! as NSString
                               print("UpdateLeadinfo JSON: \(jsonString)")
                           }
                           if(dict.count == 0){
                               jsonString = ""
                           }
                 let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><USUserRegistration xmlns='http://aahar.org.in/'><AddRegistrationMdl>\(jsonString)</AddRegistrationMdl></USUserRegistration></soap12:Body></soap12:Envelope>"

            

            dotLoader(sender: sender , controller : self, viewContain: self.viewContain, onView: "Button")
            
            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLUS, resultype: "USUserRegistrationResult", responcetype: "USUserRegistrationResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                           
                           alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            
                            self.navigationController?.popViewController(animated: true)
                           }))
                           self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
