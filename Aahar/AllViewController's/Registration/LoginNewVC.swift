//
//  LoginNewVC.swift
//  Aahar
//
//  Created by Navin Patidar on 1/9/20.
//  Copyright © 2020 Saavan_patidar. All rights reserved.
//

import UIKit

class LoginNewVC: UIViewController {
    //MARK:-
    //MARK:- ---------IBOutlet
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    @IBOutlet weak var btnRegistration: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRemember: UIButton!
    @IBOutlet weak var viewContain: UIView!
    //MARK:-
    //MARK:- ---------LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRegistration.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnRegistration.layer.cornerRadius = 8.0
        btnLogin.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnLogin.layer.cornerRadius = 8.0
        btnRemember.setImage(UIImage(named: "radio_2"), for: .normal)
        btnRemember.tag = 2
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        if(textField == txtMobile){
            trimmedText = "\(trimmedText.replacingOccurrences(of: "-", with: ""))"
        }
        textField.text = trimmedText
    }
    
    //MARK:-
    //MARK:- ---------IBAction
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actionOnRegistration(_ sender: UIButton) {
        let vc: RegistrationNewVC = self.storyboard!.instantiateViewController(withIdentifier: "RegistrationNewVC") as! RegistrationNewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func actionOnRemember(_ sender: UIButton) {
        if(sender.currentImage == UIImage(named: "radio_1")){
            sender.setImage(UIImage(named: "radio_2"), for: .normal)
            sender.tag = 2
        }else{
            sender.setImage(UIImage(named: "radio_1"), for: .normal)
            sender.tag = 1
        }
    }
    @IBAction func actionOnForgotPassword(_ sender: UIButton) {
        forgotPasswordAlert()
    }
    @IBAction func actionOnLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            
            
            callOnLoginAPI(sender: sender, strMobile: txtMobile.text!, strPassword: txtPassword.text!)
        }
    }
    
    
    func forgotPasswordAlert() {
        let alertController = UIAlertController(title: "", message: "Enter your registered mobile number", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Mobile number"
            textField.keyboardType = .numberPad
        }
        let saveAction = UIAlertAction(title: "Forgot", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    print("Text :: \(textField.text ?? "")")
                    if ((textField.text?.count)! < 10){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
                    }else{
                        self.callForgotPassword(sender: UIButton(), strMobile: textField.text!)
                    }
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        alertController.preferredAction = saveAction
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:-
    //MARK:- ---------validation
    func validation() -> Bool {
        
        if (txtMobile.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
            return false
            
        }else if ((txtMobile.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
            return false
        }else if (txtPassword.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Password, viewcontrol: self)
            return false
        }
        return true
        
    }
    //MARK:-
    //MARK:- ---------API Calling
    func callOnLoginAPI(sender : UIButton , strMobile : String , strPassword : String) {
        
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><Check_OTP xmlns='http://aahar.org.in/'><MobileNo>\(strMobile)</MobileNo><OTP>\(strPassword)</OTP></Check_OTP></soap12:Body></soap12:Envelope>"
        dotLoader(sender: sender , controller : self, viewContain: self.viewContain, onView: "Button")
        
        WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "Check_OTPResult", responcetype: "Check_OTPResponse") { (responce, status) in
            remove_dotLoader(controller: self)
            if status == "Suceess"{
                let dictTemp = responce.value(forKey: "data")as! NSDictionary
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    
                    if(self.btnRemember.tag == 1){
                        nsud.setValue("True", forKey: "Aahar_LoginStatus")
                        
                    }else{
                        nsud.setValue("False", forKey: "Aahar_LoginStatus")
                        
                    }
                    
                    
                    let aryTemp = dictTemp.value(forKey: "ProfileDetail")as! NSArray
                    let dictLoginData = aryTemp.object(at: 0)as! NSDictionary
                    nsud.setValue(dictLoginData, forKey: "Aahar_LoginData")
                    
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC")as! DynemicSplashVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Password invalid!", viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
    
    //MARK:-
    //MARK:- ---------API Calling
    func callForgotPassword(sender : UIButton , strMobile : String) {
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ForgetPassword xmlns='http://aahar.org.in/'><MobileNo>\(strMobile)</MobileNo></ForgetPassword></soap12:Body></soap12:Envelope>"
        dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "FullView")
        WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLUS, resultype: "ForgetPasswordResult", responcetype: "ForgetPasswordResponse") { (responce, status) in
            remove_dotLoader(controller: self)
            if status == "Suceess"{
                let dictTemp = responce.value(forKey: "data")as! NSDictionary
                if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                    let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
}
