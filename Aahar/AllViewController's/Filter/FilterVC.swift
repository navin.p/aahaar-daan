//
//  FilterVC.swift
//  Aahar
//
//  Created by Navin Patidar on 12/2/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------

protocol FilterDelegate : class{
    func GetDataFromFilterDelegate(dictData : NSDictionary ,tag : Int)
}





class FilterVC: UIViewController {
    // MARK: - --------------IBOutlet
       // MARK: -
       @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    weak var delegate: FilterDelegate?
    @IBOutlet weak var segmantDays: UISegmentedControl!
    @IBOutlet weak var segmantMiles: UISegmentedControl!

    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        if(DeviceType.IS_IPHONE_X){
                         heightHeader.constant = 94
                     }else{
                         heightHeader.constant = 74
                     }
        // Do any additional setup after loading the view.
    }
    
    
      // MARK: - ---------------IBAction
      // MARK: -
      @IBAction func actiononBack(_ sender: UIButton) {
          self.view.endEditing(true)
          
                  self.navigationController?.popViewController(animated: true)
          
      }

    @IBAction func actionOnSegmantMiles(_ sender: UISegmentedControl) {
        
        
    }
    @IBAction func actionOnFilter(_ sender: Any) {
        let dict = NSMutableDictionary()
        
        let titlesegmantDays = segmantDays.titleForSegment(at: segmantDays.selectedSegmentIndex)
        let titlesegmantMiles = segmantMiles.titleForSegment(at: segmantMiles.selectedSegmentIndex)
        dict.setValue("\(titlesegmantDays!)", forKey: "days")
        dict.setValue("\(titlesegmantMiles!)", forKey: "miles")
        delegate?.GetDataFromFilterDelegate(dictData: dict, tag: 0)
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnSegmantDays(_ sender: UISegmentedControl) {
        
        
       }
}
