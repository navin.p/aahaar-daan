//
//  CreatFeedBackVC.swift
//  Aahar
//
//  Created by Navin Patidar on 2/25/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class CreatFeedBackVC: UIViewController {
    @IBOutlet weak var viewHeader: CardView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var txtFeedBack: KMPlaceholderTextView!
    @IBOutlet weak var heightHeader: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
                        heightHeader.constant = 94
                    }else{
                        heightHeader.constant = 74
                    }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        btnSubmit.layer.cornerRadius = 15.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
        txtFeedBack.layer.borderColor = UIColor.darkGray.cgColor
        txtFeedBack.delegate = self
        lblCount.text =  "0/250"

        // Do any additional setup after loading the view.
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(txtFeedBack.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Feedback, viewcontrol: self)
        }else{
            callAddFeedbackSuggesionAPI(sender: sender)
        }
        
    }
   
    //MARK:- --------------- API CAlling
    //MARK:

    func callAddFeedbackSuggesionAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddFeedbackSuggesion xmlns='http://aahar.org.in/'><FeedbackId>0</FeedbackId><Feedback>\(txtFeedBack.text!)</Feedback><Suggesion>Nice..</Suggesion><DonorVId>\(dict_Login_Data.value(forKey: "DonorVId")!)</DonorVId><city_id>\(dict_Login_Data.value(forKey: "city_id")!)</city_id><state_id>\(dict_Login_Data.value(forKey: "state_id")!)</state_id></AddFeedbackSuggesion></soap12:Body></soap12:Envelope>"
            dotLoader(sender: sender , controller : self, viewContain: self.view, onView: "Button")

            WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "AddFeedbackSuggesionResult", responcetype: "AddFeedbackSuggesionResponse") { (responce, status) in
                remove_dotLoader(controller: self)
                if status == "Suceess"{
//                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
//                    let aryTemp = dictTemp.value(forKey: "FeedbackSuggesion")as! NSArray
                    self.navigationController?.popViewController(animated: true)
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
extension CreatFeedBackVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        lblCount.text =  "\(newText.count)/250"
        return newText.count < 250
    }

}
