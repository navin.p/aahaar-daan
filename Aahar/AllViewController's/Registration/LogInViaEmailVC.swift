//
//  LogInViaEmailVC.swift
//  Aahar
//
//  Created by Navin Patidar on 11/26/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit

class LogInViaEmailVC: UIViewController {
    //MARK:-
         //MARK:- ---------IBOutlet
         
       
       @IBOutlet weak var tblView: UITableView!
       @IBOutlet weak var txtEmail: ACFloatingTextfield!
       @IBOutlet weak var viewHeader: CardView!
       @IBOutlet weak var btnLogin: UIButton!
       @IBOutlet weak var viewContain: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
              tblView.tableFooterView = UIView()
              btnLogin.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
              viewHeader.backgroundColor = hexStringToUIColor(hex: primaryGreenColor)
          txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
                 let text = textField.text ?? ""
                 let trimmedText = text.trimmingCharacters(in: .whitespaces)
                 textField.text = trimmedText
        
     }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation()){
            callOnLoginAPI(sender: sender)
        }
    }
     // MARK: - --------------API Calling
       // MARK: -
       func callOnLoginAPI(sender : UIButton) {
              if !isInternetAvailable() {
                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                  
              }else{
                  
                  let strEmail = "\(txtEmail.text!.replacingOccurrences(of: " ", with: ""))"
                  let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UserLoginOTP xmlns='http://aahar.org.in/'>     <MobileNo></MobileNo><country_id></country_id><EmailId>\(strEmail)</EmailId><Password></Password></UserLoginOTP></soap12:Body></soap12:Envelope>"
               
                  dotLoader(sender: sender , controller : self, viewContain: self.viewContain, onView: "Button")
                  WebService.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "UserLoginOTPResult", responcetype: "UserLoginOTPResponse") { (responce, status) in
                      remove_dotLoader(controller: self)
                      if status == "Suceess"{
                          let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                          if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                              let testController = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC")as! OTPVC
                              var strMobile = "\(dictTemp.value(forKey: "Msg")!)"
                              strMobile = "\(strMobile.replacingOccurrences(of: "-", with: ""))"
                              dictTemp.setValue(strMobile.replacingOccurrences(of: " ", with: ""), forKey: "MobileNo")
                            dictTemp.setValue("LoginViaEmail", forKey: "LoginType")
                            dictTemp.setValue("\(strEmail)", forKey: "Email")
                             dictTemp.setValue("", forKey: "CountryCode")
                              testController.dictData = dictTemp
                              self.navigationController?.pushViewController(testController, animated: true)
                          }else{
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                          }
                      }else{
                          showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                      }
                  }
              }
          }
          
       
     // MARK: - --------------validation
        // MARK: -
        func validation() -> Bool {
         if (txtEmail.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Email, viewcontrol: self)
                return false
            }else if !(validateEmail(email: txtEmail.text!)){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
                return false
            }
            return true
        }
    }
